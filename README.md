# task manager extraEye #

## What is the project for ##
The project is for conducting personal affairs. It is multiaccess application. You can make task with its help , share tasks with other users, add reminders, as well as for actual tasks. Besides tasks and reminders can be united into groups. The project supports creation of planned tasks - future tasks, which will be created on certain dates.

## How to start ##
#### At first clone the project ####
```bash
$ git clone https://bitbucket.org/YuraGritsuk/extraeye.git
```
#### Then you should to install application on your computer ####
```bash
$ cd extraeye
$ python3 setup.py install
```
#### You are ready to start, enter *eye* ####
```bash
$ eye
```

## Running tests ##

````bash
$ [sudo] python3 setup.py test
````

## extraEye commands ##
* **task**   -    Contains a set of commands to work with tasks. 
* **reminder** -  Contains a set of commands to work with reminders.
* **scheduler** - Contains a set of commands to work with schedulers.
* **group**  -   Contains a set of commands to work with groups. 
* **user**   -    Contains a set of commands to work with tasks. 
#### Basic command syntax ####
Usage: eye COMMAND SUBCOMMAND [ARGS] [OPTIONS]
#### Example of use ####
```bash
$ eye user add Peter
$ eye user change -l Peter
$ eye task add -d "First Peter's task"
```

#### Further instructions you can get with using option *--help* ####




