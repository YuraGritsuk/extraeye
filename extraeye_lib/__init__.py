"""Implements specialized packages and modules-sets of functions for internal application work.

Packages and modules-sets of functions that providing adding, editing, transforming of data and
save it changes. Besides module includes application tests, logging library work, extraeye exceptions
and localization packages.

* entities         all special entities that are used in application.
* storage          represent modules-sets of functions that providing user/application interaction with storage.
* logger.py        sets of functions for working with extraeye logger.
* management       interlayer between console and storage.
* exceptions       modules with classes describing application failures.
* localization     modules translating application output into understandable for user languages.
* tests            tests for extraeye library.

Simple examples of using library:

>>>import extraeye_lib
>>>from extraeye_lib.management.tracker import Tracker

Add task:
    >>>Tracker().add_task('John', 'simple description')
    >>>Tracker().get_all_tasks('yura')
    [<extraeye_lib.entities.task.Task object at 0x7f70bee11b00>]

Add scheduler:
    >>>Tracker().add_scheduler('yura', {'description': 'simple description'})
    >>>Tracker().get_schedulers('yura')
    [<extraeye_lib.entities.scheduler.Scheduler object at 0x7f70bd706e80>]

Add group:
    >>>Tracker().add_group_to_task('yura', <task id>, 'group name')
    >>>Tracker().get_group('yura', 'sport')
    Group(name='sport', tasks=[<extraeye_lib.entities.task.Task object at 0x7ff71d5cbfd0>], reminders=[])

Add reminder:
    >>>Tracker().add_reminder('yura', 'description')
    >>>Tracker().get_all_reminders('yura')
    [<extraeye_lib.entities.reminder.Reminder object at 0x7f07217f84e0>]
"""
