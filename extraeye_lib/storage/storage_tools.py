"""Supporting functions for storage.py."""

import enum

from extraeye_lib.entities.user import User
from dateutil.relativedelta import relativedelta
from extraeye_lib.entities.scheduler import Scheduler
from datetime import (
    datetime,
    date
)
from extraeye_lib.entities.reminder import (
    Reminder,
    ReminderOwner,
    EntitiesType
)
from extraeye_lib.entities.task import (
    Task,
    Shared,
    Priority,
    Status,
    Permission,
    SharedType
)

# Task unique fields:
TASK_ID = 't_id'
PRIORITY = 'priority'
STATUS = 'status'
SUBTASKS = 'subtasks'
PARENT = 'parent'
TASK_REMINDERS = 'reminders'
DEADLINE = 'deadline'

# Reminder unique fields:
REMINDER_ID = 'r_id'
NOTIFY_DATE = 'notify_date'
IS_NOTIFIED = 'is_notified'
OWNER = 'owner'

# Scheduler unique fields:
SCHEDULER_ID = 's_id'
TASK_INFO = 'task_info'
START_DATE = 'start_date'
END_DATE = 'end_date'
TASKS_UPLOAD = 'tasks_upload'
DATE_DELTA = 'date_delta'
YEARS = 'years'
MONTHS = 'months'
DAYS = 'days'

# User unique fields:
LOGIN = 'login'
TASKS = 'tasks'
REMINDERS = 'reminders'
SCHEDULERS = 'schedulers'
SHARED_TASKS = 'shared_tasks'
HUMAN = 'human'

# datetime formats:
DEADLINE_FORMAT = '%Y-%m-%d %H:%M'
NOTIFY_DATE_FORMAT = '%Y-%m-%d'
RECEIVED_DATETIME_FORMAT = '%Y-%m-%d %H:%M:%S'


def convert_fields_to_json(obj):
    """Convert extraeye entities fields to json representable.

    Args:
        obj: field of converting extraeye entity.

    Returns:
        Json representable field value.
    """
    if isinstance(obj, set):
        return list(obj)
    elif isinstance(obj, datetime):
        return obj.isoformat(sep=' ')
    elif isinstance(obj, date):
        return obj.isoformat()
    elif isinstance(obj, relativedelta):
        return {YEARS: obj.years, MONTHS: obj.months, DAYS: obj.days}
    elif isinstance(obj, enum.Enum):
        return obj.value
    else:
        return obj.__dict__


# Fields that are repeating in different entities
USER_LOGIN = 'user_login'
DESCRIPTION = 'description'
TIME = 'time'
CREATION = 'creation'
EDITION = 'edition'
GROUPS = 'groups'
OTHER = 'other'


def convert_json_to_task(obj):
    """Convert json obj into task.

    Args:
        obj (dict): Dict with str task fields.

    Returns:
        'Task':
    """
    return Task(obj[USER_LOGIN], obj[DESCRIPTION], Priority(int(obj[PRIORITY])), Status(obj[STATUS]),
                obj[TASK_ID], convert_json_to_datetime(obj[DEADLINE]),
                convert_json_to_datetime(obj[TIME][CREATION]), convert_json_to_datetime(obj[TIME][EDITION]),
                obj[PARENT], obj[OTHER], obj[SUBTASKS], obj[TASK_REMINDERS], obj[GROUPS])


def convert_json_to_reminder(obj):
    """Convert json obj into reminder.

    Args:
        obj (dict): Dict with str reminder fields.

    Returns:
        'Reminder':
    """
    return Reminder(obj[USER_LOGIN], obj[DESCRIPTION], convert_json_to_date(obj[NOTIFY_DATE]), obj[REMINDER_ID],
                    convert_json_to_datetime(obj[TIME][CREATION]), convert_json_to_datetime(obj[TIME][EDITION]),
                    obj[IS_NOTIFIED], convert_json_to_reminder_owner(obj[OWNER]), obj[OTHER], obj[GROUPS])


def convert_json_to_user(obj):
    """Convert json obj into user.

    Args:
        obj (dict): Dict with str user fields.

    Returns:
        'User':
    """
    return User(obj[LOGIN], obj[TASKS], obj[REMINDERS], convert_json_to_shared_entities(obj[SHARED_TASKS]),
                obj[SCHEDULERS], obj[HUMAN])


def convert_json_to_scheduler(obj):
    """Convert json obj into scheduler.

    Args:
        obj (dict): Dict with str scheduler fields.

    Returns:
        'Scheduler':
    """
    return Scheduler(obj[USER_LOGIN], task_info=obj[TASK_INFO], start_date=convert_json_to_date(obj[START_DATE]),
                     end_date=convert_json_to_date(obj[END_DATE]), is_year=obj[DATE_DELTA][YEARS] != 0,
                     is_month=obj[DATE_DELTA][MONTHS] != 0, is_day=obj[DATE_DELTA][DAYS] != 0,
                     amount_differences=get_from_json_date_delta_amount_difference(obj[DATE_DELTA]),
                     tasks_upload=int(obj[TASKS_UPLOAD]), s_id=obj[SCHEDULER_ID],
                     creation_datetime=convert_json_to_datetime(obj[TIME][CREATION]),
                     edition_datetime=convert_json_to_datetime(obj[TIME][EDITION]))


def get_from_json_date_delta_amount_difference(obj):
    """Get from str date_delta field value of amount_difference field.

    Args:
        obj (dict): Date_delta dict.

    Returns:
        'int':
    """
    if obj[YEARS]:
        return obj[YEARS]
    elif obj[MONTHS]:
        return obj[MONTHS]
    else:
        return obj[DAYS]


def convert_json_to_datetime(datetime_str):
    """Convert str to datetime.

    Args:
        datetime_str (str):

    Returns:
        {
            'datetime': If datetime_str is not empty,
            'None': Otherwise.
        }
    """
    return datetime.strptime(datetime_str, RECEIVED_DATETIME_FORMAT) if datetime_str else None


def convert_json_to_date(date_str):
    """Convert str to date.

    Args:
        date_str (str):

    Returns:
        {
            'date': If date_str is not empty,
            'None': Otherwise.
        }
    """
    return datetime.strptime(date_str, NOTIFY_DATE_FORMAT).date() if date_str else None


def convert_json_to_reminder_owner(owner_str):
    """Convert json obj into ReminderOwner namedtuple.

    Args:
        owner_str (list): reminder_owner values-fields list.

    Returns
        {
            'ReminderOwner': If owner_str is not empty,
            'None': Otherwise.
        }
    """
    return ReminderOwner(EntitiesType(owner_str[0]), owner_str[1], owner_str[2]) if owner_str else None


def convert_json_to_shared_entities(shared_entities_str):
    """Convert json into Shared.

    Args:
        shared_entities_str (list): List of str shared_entity fields.

    Returns:
        'Shared': Shared entity
    """
    shared_entities = []
    for shared_entity_str in shared_entities_str:
        shared_entities.append(Shared(shared_entity_str[0], Permission(shared_entity_str[1]),
                                      SharedType(shared_entity_str[2]), shared_entity_str[3]))
    return shared_entities


def get_now_datetime():
    """Get current date and time without milliseconds.

    Returns:
        'datetime':
    """
    return datetime.strptime(datetime.now().strftime(DEADLINE_FORMAT), DEADLINE_FORMAT)


def archive_condition(is_archive, only_archive, is_all):
    """Determines whether the archive state matches the condition given by only_archive and is_all.

    Args:
        is_archive (bool): Archive condition of entity.
        only_archive (bool): Indicates that only archive entities are match.
        is_all (bool): Indicates all entities are match.

    Returns:
        {
            'True': If is_archive meet conditions given by only_archive and is_all,
            'False': Otherwise.
        }
    """
    if only_archive:
        return True if is_archive else False
    if is_all:
        return True
    return True if not is_archive else False
