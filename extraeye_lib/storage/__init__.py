"""Package with specialized modules-set of functions for working with files.

Implements functions for writing, reading and selecting information from files.

* path.py          represent class Path, which describe all need paths/dirs.
* storage.py       functions to write/select/read data.
* storage_tools.py supporting functions.
"""

