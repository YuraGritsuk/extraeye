"""A set of functions for read, write and select information with using files."""

import os
import json
import shutil
import extraeye_lib.storage.storage_tools as st_tools

from queue import Queue
from extraeye_lib.logger import write_log
from extraeye_lib.storage.path import Path
from extraeye_lib.entities.task import SharedType
from extraeye_lib.exceptions.name_exceptions import LoginNameError
from datetime import (
    datetime,
    date
)
from extraeye_lib.exceptions.id_exceptions import (
    TaskIdError,
    ReminderIdError,
    SchedulerIdError
)


@write_log
def write_tasks_to_json(user_login, tasks, update_edition_time=True, path=None):
    """Save schedulers to json.

    Convert scheduler fields in json representable and writes them to json file.

    Args:
        user_login (str): Indicates in whose storage schedulers will be written.
        tasks (list): List of tasks to be written.
        update_edition_time (:obj: `bool`, optional): Determines whether the edition time is updated.
            Defaults to True.
        path (:obj: `Path`, optional): Object providing functionality for working with storage
            paths. Defaults to None.
    """
    path = path if path else Path()

    path_to_tasks_dir = path.get_tasks_dir(user_login)
    if not os.path.exists(path_to_tasks_dir):
        os.makedirs(path_to_tasks_dir)
    for task in tasks:
        if update_edition_time:
            task.time.edition = datetime.strptime(datetime.now().strftime('%Y-%m-%d %H:%M'), '%Y-%m-%d %H:%M')

        path_to_task_file = path.create_path_to_task_file(user_login, task.t_id)
        with open(path_to_task_file, 'w', encoding='utf-8') as task_file:
            json.dump(task, task_file, default=st_tools.convert_fields_to_json)


@write_log
def write_reminders_to_json(user_login, reminders, update_edition_time=True, path=None):
    """Save reminders to json.

    Convert reminder fields in json representable and writes them to json file.

    Args:
        user_login (str): Indicates in whose storage reminders will be written.
        reminders (list): List of reminders to be written.
        update_edition_time (:obj: `bool`, optional): Determines whether the reminder edition time is updated.
            Defaults to True.
        path (:obj: `Path`, optional): Object providing functionality for working with storage
            paths. Defaults to None.
    """
    path = path if path else Path()

    path_to_reminders_dir = path.get_reminders_dir(user_login)
    if not os.path.exists(path_to_reminders_dir):
        os.makedirs(path_to_reminders_dir)
    for reminder in reminders:
        if update_edition_time:
            reminder.time.edition = datetime.strptime(datetime.now().strftime('%Y-%m-%d %H:%M'), '%Y-%m-%d %H:%M')

        path_to_reminder_file = path.create_path_to_reminder_file(user_login, reminder.r_id)
        with open(path_to_reminder_file, 'w', encoding='utf-8') as reminder_file:
            json.dump(reminder, reminder_file, default=st_tools.convert_fields_to_json)


@write_log
def write_schedulers_to_json(user_login, schedulers, update_edition_time=True, path=None):
    """Save schedulers to json.

    Convert scheduler fields in json representable and writes them to json file.

    Args:
        user_login (str): Indicates in whose storage schedulers will be written.
        schedulers (list): List of schedulers to be written.
        update_edition_time (:obj: `bool`, optional): Determines whether the scheduler edition time is updated.
            Defaults to True.
        path (:obj: `Path`, optional): Object providing functionality for working with storage
            paths. Defaults to None.
    """
    path = path if path else Path()

    path_to_schedulers_dir = path.get_schedulers_dir(user_login)
    if not os.path.exists(path_to_schedulers_dir):
        os.makedirs(path_to_schedulers_dir)
    for scheduler in schedulers:
        if update_edition_time:
            scheduler.time.edition = datetime.strptime(datetime.now().strftime('%Y-%m-%d %H:%M'), '%Y-%m-%d %H:%M')

        path_to_scheduler_file = path.create_path_to_scheduler_file(user_login, scheduler.s_id)
        with open(path_to_scheduler_file, 'w', encoding='utf-8') as scheduler_file:
            json.dump(scheduler, scheduler_file, default=st_tools.convert_fields_to_json)


@write_log
def write_user_to_json(user, path=None):
    """Save users to json.

    Convert user fields in json representable and writes them to json file.

    Args:
        user (User):
        path (:obj: `Path`, optional): Object providing functionality for working with storage
        paths. Defaults to None.
    """
    path = path if path else Path()

    path_to_user_dir = path.get_user_dir(user.login)
    if not os.path.exists(path_to_user_dir):
        os.makedirs(path_to_user_dir)

    path_to_user_file = path.create_path_to_user_file(user.login)
    with open(path_to_user_file, 'w', encoding='utf-8') as user_file:
        json.dump(user, user_file, default=st_tools.convert_fields_to_json)


@write_log
def read_tasks_from_json(user_login, only_archive=False, is_all=False, path=None):
    """Get tasks from json.

    Read tasks from json and convert received fields in dict from str to the desired format.

    Args:
        user_login (str): indicates whose tasks should be read.
        only_archive (:obj:`bool`, optional): indicates that only archive tasks will be taken. Defaults to False.
        is_all (:obj: `bool`, optional): indicates that all tasks will be taken. Defaults to False.
        path (:obj: `Path`, optional): Object providing functionality for working with storage
            paths. Defaults to None.

    Yields:
        Task: Task from user tasks that meet all conditions.
    """
    path = path if path else Path()

    path_to_tasks_dir = path.get_tasks_dir(user_login)
    if os.path.exists(path_to_tasks_dir):

        for task_file in os.listdir(path_to_tasks_dir):

            path_to_task_file = path.create_path_to_task_file(user_login, task_file)
            with open(path_to_task_file, 'r', encoding='utf-8') as json_task:
                task = st_tools.convert_json_to_task(json.load(json_task))
                if st_tools.archive_condition(task.is_archive(), only_archive, is_all):
                    yield task


@write_log
def read_reminders_from_json(user_login, only_archive=False, is_all=False, path=None):
    """Get reminders from json.

    Read reminders from json and convert received fields in dict from str to the desired format.

    Args:
        user_login (str): Indicates whose reminders should be read.
        only_archive (:obj:`bool`, optional): Indicates that only archive reminders will be taken. Defaults to False.
        is_all (:obj: `bool`, optional): Indicates that all reminders will be taken. Defaults to False.
        path (:obj: `Path`, optional): Object providing functionality for working with storage
            paths. Defaults to None.

    Yields:
        Reminder: Reminder from user reminders that meet all conditions.
    """
    path = path if path else Path()

    path_to_reminders_dir = path.get_reminders_dir(user_login)
    if os.path.exists(path_to_reminders_dir):

        for reminder_file in os.listdir(path_to_reminders_dir):

            path_to_reminder_file = path.create_path_to_reminder_file(user_login, reminder_file)
            with open(path_to_reminder_file, 'r', encoding='utf-8') as json_reminder:
                reminder = st_tools.convert_json_to_reminder(json.load(json_reminder))
                if st_tools.archive_condition(reminder.is_archive(), only_archive, is_all):
                    yield reminder


@write_log
def read_schedulers_from_json(user_login, path=None):
    """Get schedulers from json.

    Read schedulers from json and convert received fields in dict from str to the desired format.

    Args:
        user_login (str): Indicates whose reminders should be read.
        path (:obj: `Path`, optional): Object providing functionality for working with storage
            paths. Defaults to None.

    Yields:
        Scheduler: Scheduler from user schedulers.
    """
    path = path if path else Path()

    path_to_schedulers_dir = path.get_schedulers_dir(user_login)
    if os.path.exists(path_to_schedulers_dir):

        for scheduler_file in os.listdir(path_to_schedulers_dir):

            path_to_scheduler_file = path.create_path_to_scheduler_file(user_login, scheduler_file)
            with open(path_to_scheduler_file, 'r', encoding='utf-8') as json_scheduler:
                scheduler = st_tools.convert_json_to_scheduler(json.load(json_scheduler))
                yield scheduler


@write_log
def read_users_from_json(path=None):
    """Get users from json.

    Read users from json and convert received fields in dict from str to the desired format.

    Args:
        path (:obj: `Path`, optional): Object providing functionality for working with storage
            paths. Defaults to None.

    Yields:
        User: User from users.
    """
    path = path if path else Path()

    path_to_users_dir = path.get_users_dir()
    if os.path.exists(path_to_users_dir):

        for user_file in os.listdir(path_to_users_dir):
            path_to_user_file = path.create_path_to_user_file(user_file)
            with open(path_to_user_file, 'r', encoding='utf-8') as json_user:
                yield st_tools.convert_json_to_user(json.load(json_user))


@write_log
def get_all_tasks(user_login, path=None):
    """Get all user tasks.

    Get task from read_tasks_from_json function and append them to return list.

    Args:
        user_login (str): Indicates whose tasks should be taken.
        path (:obj: `Path`, optional): Object providing functionality for working with storage
            paths. Defaults to None.

    Returns:
        'list': List with all user tasks.
    """
    path = path if path else Path()

    tasks = []
    for task in read_tasks_from_json(user_login, is_all=True, path=path):
        tasks.append(task)
    return tasks


@write_log
def get_tasks_by_group_name(user_login, group_name, only_archive=False, is_all=False, path=None):
    """Get user group tasks.

    Args:
        user_login (str): Indicates whose tasks should be sorted out.
        group_name (str): Name of group whose tasks should be taken.
        only_archive (:obj:`bool`, optional): Indicates that only archive tasks will be sorted out. Defaults to False.
        is_all (:obj: `bool`, optional): Indicates that all tasks will be sorted out. Defaults to False.
        path (:obj: `Path`, optional): Object providing functionality for working with storage
            paths. Defaults to None.

    Returns:
        'list': Group tasks that meet all conditions.
    """
    path = path if path else Path()

    group_tasks = []
    for task in read_tasks_from_json(user_login, only_archive, is_all, path=path):
        if group_name in task.groups:
            group_tasks.append(task)

    return group_tasks


@write_log
def get_shared_tasks(user_login, only_archive=False, is_all=False, path=None):
    """Take the tasks that others shared with.

    Select tasks id that others shared with and get them using get_task_by_id function.

    Args:
        user_login (str): Identifies the user whose foreign tasks should be taken.
        only_archive (:obj:`bool`, optional): Indicates that only archive tasks will be sorted out.
            Defaults to False
        is_all (:obj: `bool`, optional): Indicates that all tasks will be sorted out. Defaults to False.
        path (:obj: `Path`, optional): Object providing functionality for working with storage
            paths. Defaults to None.

    Returns:
        'list': List of foreign tasks that meet all conditions.
    """
    path = path if path else Path()

    user = get_user_by_login(user_login, path=path)

    shared_tasks = []
    for temp_task in user.shared_tasks:
        if temp_task.shared_type == SharedType.RECEIVED:
            task = get_task_by_id(temp_task.user_login, temp_task.t_id, is_all=True, path=path)
            if st_tools.archive_condition(task.is_archive(), only_archive, is_all):
                shared_tasks.append(task)

    return shared_tasks


@write_log
def get_task_by_id(user_login, task_id, only_archive=False, is_all=False, path=None):
    """Get task by id.

    Sort out user tasks and compare each task id with task_id.

    Args:
        user_login (str): Indicates whose tasks will be sorted out.
        task_id (str): Id of the task being searched.
        only_archive (:obj:`bool`, optional): Indicates that only archive tasks will be sorted out.
            Defaults to False.
        is_all (:obj: `bool`, optional): Indicates that all tasks will be sorted out. Defaults to False.
        path (:obj: `Path`, optional): Object providing functionality for working with storage
            paths. Defaults to None.

    Returns:
        'Task': Task with id equal to task_id.

    Raises:
        TaskIdError: If user doesn't have task with id equal to task_id in the specified archive state.
    """
    path = path if path else Path()

    for task in read_tasks_from_json(user_login, only_archive, is_all, path=path):
        if task.t_id == task_id:
            return task
    raise TaskIdError()


@write_log
def get_scheduler_by_id(user_login, scheduler_id, path=None):
    """Get scheduler by id.

    Sort out user schedulers and compare each scheduler id with scheduler_id.

    Args:
        user_login (str): Indicates whose schedulers will be sorted out.
        scheduler_id (str): Id of the scheduler being searched.
        path (:obj: `Path`, optional): Object providing functionality for working with storage
            paths. Defaults to None.

    Returns:
        'Scheduler': Scheduler with id equal to scheduler_id.

    Raises:
        SchedulerIdError: If user doesn't have scheduler with id equal to scheduler_id.
        """
    path = path if path else Path()

    for scheduler in read_schedulers_from_json(user_login,  path=path):
        if scheduler.s_id == scheduler_id:
            return scheduler
    raise SchedulerIdError()


@write_log
def get_tasks_from_period(user_login, start_date, end_date, only_archive=False, is_all=False, path=None):
    """Get tasks within the period: from begin_date till end_date.

    Sort out user tasks and add checks whether they belong to the period.

    Args:
        user_login (str): Indicates whose tasks will be sorted out.
        start_date (date): First day of period.
        end_date (date): Last day of period.
        only_archive (:obj:`bool`, optional): Indicates that only archive tasks will be sorted out. Defaults to False.
        is_all (:obj: `bool`, optional): Indicates that all tasks will be sorted out. Defaults to False.
        path (:obj: `Path`, optional): Object providing functionality for working with storage
            paths. Defaults to None.

    Returns:
        'list': List of user tasks that satisfy the conditions.
    """
    path = path if path else Path()

    tasks = []
    for task in read_tasks_from_json(user_login, only_archive, is_all, path=path):
        if task.deadline:
            if start_date <= task.deadline.date() <= end_date:
                tasks.append(task)
        else:
            if start_date <= task.time.creation.date() <= end_date:
                tasks.append(task)
    return tasks


@write_log
def get_subtasks(user_login, parent_task, only_archive=False, is_all=False, path=None):
    """Get task subtasks.

    Sort out user tasks and select task with field parent equal to parent_task t_id.

    Args:
        user_login (str): Indicates whose tasks will be sorted out.
        parent_task (Task): Parent task of the subtasks that are being searched for.
        only_archive (:obj:`bool`, optional): Indicates that only archive tasks will be sorted out. Defaults to False.
        is_all (:obj: `bool`, optional): Indicates that all tasks will be sorted out. Defaults to False.
        path (:obj: `Path`, optional): Object providing functionality for working with storage
            paths. Defaults to None.

    Returns:
        'list': List of parent task subtasks.
    """
    path = path if path else Path()

    subtasks = []
    for task in read_tasks_from_json(user_login, only_archive, is_all, path=path):
        if task.t_id in parent_task.subtasks:
            subtasks.append(task)

    return subtasks


@write_log
def get_all_subtasks(user_login, parent_task, path=None):
    """ Get all task subtasks.

    Args:
        user_login (str):
        parent_task (Task):
        path (:obj: `Path`, optional): Object providing functionality for working with storage
            paths. Defaults to None.

    Returns:
        'list': List of parent tasks subtasks(including parent task, subtasks, subtasks
            of subtasks and etc.).
    """
    all_subtasks = []

    queue = Queue()
    queue.put(parent_task)
    while not queue.empty():
        temp_task = queue.get()
        for subtask in get_subtasks(user_login, temp_task, is_all=True, path=path):
            queue.put(subtask)
        all_subtasks.append(temp_task)

    return all_subtasks


@write_log
def get_task_reminders(user_login, task, only_archive=False, is_all=False, path=None):
    """Get task reminders.

    Sort out user reminders and append them returns list, if reminder id is in task reminders.

    Args:
        user_login (str): Indicates whose task and reminders will be sorted out.
        task (Task): Task reminders which are being searched for.
        only_archive (:obj:`bool`, optional): Indicates that only archive tasks will be sorted out. Defaults to False.
        is_all (:obj: `bool`, optional): Indicates that all tasks will be sorted out. Defaults to False.
        path (:obj: `Path`, optional): Object providing functionality for working with storage
            paths. Defaults to None.

    Returns:
        'list': List of parent task subtasks.
    """
    path = path if path else Path()

    task_reminders = []
    for reminder in read_reminders_from_json(user_login, only_archive, is_all, path=path):
        if reminder.r_id in task.reminders:
            task_reminders.append(reminder)
    return task_reminders


@write_log
def get_reminders_by_group_name(user_login, group_name, only_archive=False, is_all=False, path=None):
    """Get user reminders from the group.

    Args:
        user_login (str): Indicates whose reminders should be sorted out.
        group_name (str): Name of group whose reminders should be taken.
        only_archive (:obj:`bool`, optional): Indicates that only archive reminders will be sorted out.
            Defaults to False.
        is_all (:obj: `bool`, optional): Indicates that all reminders will be sorted out. Defaults to False.
        path (:obj: `Path`, optional): Object providing functionality for working with storage
            paths. Defaults to None.

    Returns:
        'list': Group reminders that meet all conditions.
    """
    path = path if path else Path()

    task_reminders = []
    for reminder in read_reminders_from_json(user_login, only_archive, is_all, path=path):
        if group_name in reminder.groups:
            task_reminders.append(reminder)
    return task_reminders


@write_log
def get_all_reminders(user_login, path=None):
    """Get all user reminders.

    Get reminder from read_tasks_from_json function and append them to return list.

    Args:
        user_login (str): Indicates whose reminders should be taken.
        path (:obj: `Path`, optional): Object providing functionality for working with storage
            paths. Defaults to None.

    Returns:
        'list': List with all user reminders.
        """
    path = path if path else Path()

    return [reminder for reminder in read_reminders_from_json(user_login, is_all=True, path=path)]


@write_log
def get_reminders_from_period(user_login, start_date, end_date, only_archive=False, is_all=False, path=None):
    """Get reminders within the period: from begin_date till end_date.

    Sort out user reminders and add checks whether they belong to the period.

    Args:
        user_login (str): Indicates whose reminders will be sorted out.
        start_date (date): First day of period.
        end_date (date): Last day of period.
        only_archive (:obj:`bool`, optional): Indicates that only archive reminders will be sorted out.
            Defaults to False.
        is_all (:obj: `bool`, optional): Indicates that all reminders will be sorted out. Defaults to False.
        path (:obj: `Path`, optional): Object providing functionality for working with storage
            paths. Defaults to None.

    Returns:
        'list': List of user reminders that satisfy the conditions.
    """
    path = path if path else Path()

    reminders = []
    for reminder in read_reminders_from_json(user_login, only_archive, is_all, path=path):
        if start_date <= reminder.notify_date <= end_date:
            reminders.append(reminder)
    return reminders


@write_log
def get_all_schedulers(user_login, path=None):
    """Get all user schedulers.

    Get user schedulers from read_scheduler_from_json and append them to return list.

    Args:
        user_login (str): Indicates whose schedulers will be sorted out.
        path (:obj: `Path`, optional): Object providing functionality for working with storage
            paths. Defaults to None.

    Returns:
        'list': List of all user schedulers.
    """
    path = path if path else Path()
    return [scheduler for scheduler in read_schedulers_from_json(user_login, path=path)]


@write_log
def get_non_empty_schedulers(user_login, path=None):
    """Get user schedulers which don't create all planned tasks.

    Args:
        user_login (str): Indicates whose schedulers will be sorted out.
        path (:obj: `Path`, optional): Object providing functionality for working with storage
            paths. Defaults to None.

    Returns:
        'list': List of non-empty user schedulers.
    """
    path = path if path else Path()

    non_empty_schedulers = []
    for scheduler in read_schedulers_from_json(user_login, path=path):
        if not scheduler.is_empty():
            non_empty_schedulers.append(scheduler)
    return non_empty_schedulers


@write_log
def get_uncreated_schedulers(user_login, path=None):
    """Get uncreated schedulers.

    Sort out user schedulers and if today is date of next scheduler task or later then it, add to scheduler return list.

    Args:
        user_login (str): Indicates whose schedulers will be sorted out.
        path (:obj: `Path`, optional): Object providing functionality for working with storage
            paths. Defaults to None.

    Returns:
        'list': List of schedulers that have task which should be created today.
    """
    path = path if path else Path()

    today_schedulers = []
    for scheduler in read_schedulers_from_json(user_login, path=path):
        if not scheduler.is_empty() and scheduler.get_next_date() <= date.today():
            today_schedulers.append(scheduler)
    return today_schedulers


@write_log
def get_reminder_by_id(user_login, reminder_id, only_archive=False, is_all=False, path=None):
    """Get reminder by id.

    Sort out user reminders and compare each reminder id with reminder_id.

    Args:
        user_login (str): Indicates whose reminders will be sorted out.
        reminder_id (str): id of the reminder being searched.
        only_archive (:obj:`bool`, optional): Indicates that only archive reminders will be sorted out.
            Defaults to False.
        is_all (:obj: `bool`, optional): Indicates that all reminders will be sorted out. Defaults to False.
        path (:obj: `Path`, optional): Object providing functionality for working with storage
            paths. Defaults to None.

    Returns:
        'Reminder': Reminder with id equal to reminder_id.

    Raises:
        ReminderIdError: If user doesn't have reminder with id equal to reminder_id in the specified archive state.
    """
    path = path if path else Path()

    for reminder in read_reminders_from_json(user_login, only_archive, is_all, path=path):
        if reminder.r_id == reminder_id:
            return reminder
    raise ReminderIdError


@write_log
def get_all_group_entities(user_login, group_name, only_archive=False, is_all=False, path=None):
    """Get all group entities.

    Args:
        user_login (str): Indicates whose entities should be sorted out.
        group_name (str): Name of group whose entities should be taken.
        only_archive (:obj:`bool`, optional): Indicates that only archive entities will be sorted out.
            Defaults to False.
        is_all (:obj: `bool`, optional): Indicates that all entities will be sorted out. Defaults to False.
        path (:obj: `Path`, optional): Object providing functionality for working with storage
            paths. Defaults to None.

    Returns:
        'tuple': Group tasks and group reminders that meet all conditions.
    """
    path = path if path else Path()

    groups_tasks = []
    for task in read_tasks_from_json(user_login, only_archive, is_all, path=path):
        if group_name in task.groups:
            groups_tasks.append(task)

    groups_reminders = []
    for reminder in read_reminders_from_json(user_login, only_archive, is_all, path=path):
        if group_name in reminder.groups:
            groups_reminders.append(reminder)

    return groups_tasks, groups_reminders


@write_log
def get_all_groups_entities(user_login, only_archive=False, is_all=False, path=None):
    """Get all groups entities.

    Args:
        user_login (str): Indicates whose entities should be sorted out.
        only_archive (:obj:`bool`, optional): Indicates that only archive entities will be sorted out.
            Defaults to False.
        is_all (:obj: `bool`, optional): Indicates that all entities will be sorted out. Defaults to False.
        path (:obj: `Path`, optional): Object providing functionality for working with storage
            paths. Defaults to None.

    Returns:
        'tuple': List of tasks and list of reminders, each task/reminder from lists has one or more groups and
            satisfies specified archive state.
        """
    path = path if path else Path()

    groups_tasks = []
    for task in read_tasks_from_json(user_login, only_archive, is_all, path=path):
        if task.groups:
            groups_tasks.append(task)

    groups_reminders = []
    for reminder in read_reminders_from_json(user_login, only_archive, is_all, path=path):
        if reminder.groups:
            groups_reminders.append(reminder)

    return groups_tasks, groups_reminders


@write_log
def get_user_by_login(user_login, path=None):
    """Get user by login.

    Sort out all user that are registered and select user with login equal to user_login.

    Args:
        user_login (str): Login of the searched user.
        path (:obj: `Path`, optional): Object providing functionality for working with storage
            paths. Defaults to None.

    Returns:
        'User': User with login equal to user_login.

    Raises:
        LoginNameError: If user with login equal to login_name not registered
    """
    path = path if path else Path()

    for user in read_users_from_json(path=path):
        if user.login == user_login:
            return user
    raise LoginNameError()


@write_log
def get_overdue_tasks(user_login, path=None):
    """Get tasks whose deadline has expired.

    Args:
        user_login (str):
        path (:obj: `Path`, optional): Object providing functionality for working with storage
            paths. Defaults to None.

    Returns:
        'list': Tasks list of user overdue tasks.
    """
    path = path if path else Path()

    overdue_tasks = []
    for task in read_tasks_from_json(user_login, path=path):
        if task.deadline and task.deadline < datetime.now():
            overdue_tasks.append(task)

    shared_tasks = get_shared_tasks(user_login, path=path)
    for task in shared_tasks:
        if task.deadline and task.deadline < datetime.now():
            overdue_tasks.append(task)

    return overdue_tasks


@write_log
def create_storage(path=None):
    """Create base directory for each user storage.

    Args:
        path (:obj: `Path`, optional): Object providing functionality for working with storage
            paths. Defaults to None.
    """
    path = path if path else Path()

    if not os.path.exists(path.base_dir):
        os.makedirs(path.base_dir)


@write_log
def create_user_storage(user, path=None):
    """Create separate storage for new user's data.

    Args:
        user (User):
        path (:obj: `Path`, optional): Object providing functionality for working with storage
            paths. Defaults to None.
    """
    path = path if path else Path()

    if os.path.exists(path.get_user_dir(user.login)):
        shutil.rmtree(path.get_user_dir(user.login))
    write_user_to_json(user, path)


@write_log
def get_missed_reminders(user_login, path=None):
    """Get reminders that should have notified but didn't.

    Args:
        user_login (str):
        path (:obj: `Path`, optional): Object providing functionality for working with storage
            paths. Defaults to None.

    Returns:
        'list':
    """
    path = path if path else Path()

    missed_reminders = []
    for reminder in read_reminders_from_json(user_login, path=path):
        if reminder.notify_date <= date.today():
            missed_reminders.append(reminder)

    return missed_reminders


@write_log
def delete_tasks_by_id(user_login, tasks_id, path=None):
    """Delete tasks by id.

    Get paths to files with info of the deleted tasks and removes them.

    Args:
        user_login (str): Indicates base dirs for deleted tasks.
        tasks_id (list): Indicates with base_dirs paths to files with info of the deleted tasks.
        path (:obj: `Path`, optional): Object providing functionality for working with storage
            paths. Defaults to None.

    Raises:
        TaskIdError: If there is no file with task info.
    """
    path = path if path else Path()

    for task_id in tasks_id:
        task_path = path.create_path_to_task_file(user_login, task_id)
        if not os.path.exists(task_path):
            raise TaskIdError()
        os.remove(task_path)


@write_log
def delete_reminders_by_id(user_login, reminders_id, path=None):
    """Delete reminders by id.

    Get paths to files with info of the deleted reminders and removes them.

    Args:
        user_login (str):
        reminders_id (list): Indicates with base_dirs paths to files with info of the deleted reminders.
        path (:obj: `Path`, optional): Object providing functionality for working with storage
        paths. Defaults to None.

    Raises:
        ReminderIdError: If there is no file with reminder info.
    """
    path = path if path else Path()

    for reminder_id in reminders_id:
        reminder_path = path.create_path_to_reminder_file(user_login, reminder_id)
        if not os.path.exists(reminder_path):
            raise ReminderIdError()
        os.remove(reminder_path)


@write_log
def delete_schedulers_by_id(user_login, schedulers_id, path=None):
    """Delete schedulers by id.

    Get paths to files with info of the deleted schedulers and removes them.

    Args:
        user_login (str):
        schedulers_id (list): Indicates with base_dirs paths to files with info of the deleted schedulers.
        path (:obj: `Path`, optional): Object providing functionality for working with storage
        paths. Defaults to None.

    Raises:
        SchedulerIdError: If there is no file with scheduler info.
    """
    path = path if path else Path()

    for scheduler_id in schedulers_id:
        scheduler_path = path.create_path_to_scheduler_file(user_login, scheduler_id)
        if not os.path.exists(scheduler_path):
            raise SchedulerIdError()
        os.remove(scheduler_path)
