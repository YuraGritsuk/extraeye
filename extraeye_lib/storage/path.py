"""Module represent class Path."""

import os


class Path:
    """Class for creating dirs and paths to files."""

    def __init__(
            self,
            base_dir=os.path.join(os.environ['HOME'], 'Extraeye_Storage'),

            users_dir_name='',
            user_info_file_name='User_Info',

            tasks_dir_name='Tasks',
            reminders_dir_name='Reminders',
            schedulers_dir_name='Schedulers'
            ):
        """Init path object.

        Args:
            base_dir (:obj: `str`, optional): Base dir for application storage. Defaults to
                os.path.join(os.environ['HOME'], 'Extraeye_Storage').
            users_dir_name (:obj: `str`, optional): Base dir name for user's data. Defaults to ''.
            user_info_file_name  (:obj: `str`, optional): Base file name for user's personal info.
                Defaults to 'User_Info'.
            tasks_dir_name (:obj: `str`, optional): Base dir name for user tasks storage.
                Defaults to 'Tasks'.
            reminders_dir_name (:obj: `str`, optional): Base dir name for user reminders storage.
                Defaults to 'Reminders'.
            schedulers_dir_name (:obj: `str`, optional): Base dir name for user schedulers storage.
                Defaults to 'Schedulers'.
        """

        self.base_dir = base_dir

        self.user_info_file_name = user_info_file_name
        self.users_dir_name = users_dir_name
        self.tasks_dir_name = tasks_dir_name
        self.reminders_dir_name = reminders_dir_name
        self.schedulers_dir_name = schedulers_dir_name

    def get_users_dir(self):
        """Get base users directory.

        Returns:
            'str':
        """
        return os.path.join(self.base_dir, self.users_dir_name)

    def get_user_dir(self, user_login):
        """Get dir for user storage.

        Returns:
            'str':
        """
        return os.path.join(self.base_dir, user_login)

    def create_path_to_user_file(self, user_login):
        """Build path to storage file with user info.

        Returns:
            'str': Path to file with user info.
        """
        return os.path.join(self.get_user_dir(user_login), self.user_info_file_name)

    def get_tasks_dir(self, user_login):
        """Get base user tasks directory.

        Returns:
            'str':
        """
        return os.path.join(self.base_dir, user_login, self.tasks_dir_name)

    def create_path_to_task_file(self, user_login, t_id):
        """Build path to storage file with task info.

        Args:
            user_login (str): Login of task owner.
            t_id (str): Id of a saved task.

        Returns:
            'str':
        """

        return os.path.join(self.get_tasks_dir(user_login), t_id)

    def get_reminders_dir(self, user_login):
        """Get base user reminders directory.

        Returns:
            'str':
        """
        return os.path.join(self.base_dir, user_login, self.reminders_dir_name)

    def create_path_to_reminder_file(self, user_login, r_id):
        """Build path to storage file with reminder info.

        Args:
            user_login (str): Login of reminder owner.
            r_id (str): Id of a saved reminder.

        Returns:
            'str':
        """
        return os.path.join(self.get_reminders_dir(user_login), r_id)

    def get_schedulers_dir(self, user_login):
        """Get base user schedulers directory.

        Returns:
            'str':
        """
        return os.path.join(self.base_dir, user_login, self.schedulers_dir_name)

    def create_path_to_scheduler_file(self, user_login, s_id):
        """Build path to storage file with user scheduler info.

        Args:
            user_login (str): Login of scheduler owner.
            s_id (str): Id of a saved scheduler.

        Returns:
            'str':
        """
        return os.path.join(self.get_schedulers_dir(user_login), s_id)
