import time
import unittest

from datetime import datetime, date
from extraeye_lib.entities.time_span import TimeSpan


class TimeSpanTests(unittest.TestCase):

    def test_initializing(self):
        initializing_info_list = [
            [None, None],
            [datetime(2019, 1, 1, 15, 30), datetime(2019, 1, 1, 15, 30)],
            [datetime(2019, 1, 1, 15, 30), datetime(2021, 2, 7, 17, 20)],
            [None, datetime(2019, 1, 1, 9, 0)]
        ]

        self._sleep_if_midnight()

        correct_answers = [
            [date.today(), date.today()],
            [datetime(2019, 1, 1, 15, 30), datetime(2019, 1, 1, 15, 30)],
            [datetime(2019, 1, 1, 15, 30), datetime(2021, 2, 7, 17, 20)],
            [date.today(), datetime(2019, 1, 1, 9, 0)]
        ]

        for init_datetimes, correct_answer in zip(initializing_info_list, correct_answers):
            time_span = TimeSpan(creation=init_datetimes[0], edition=init_datetimes[1])

            if init_datetimes[0] is None:
                self.assertEqual(time_span.creation.date(), correct_answer[0])
            else:
                self.assertEqual(time_span.creation, correct_answer[0])

            if init_datetimes[1] is None:
                self.assertEqual(time_span.edition.date(), correct_answer[1])
            else:
                self.assertEqual(time_span.edition, correct_answer[1])

    @staticmethod
    def _sleep_if_midnight():
        if datetime.now() > datetime(date.today().year, date.today().month, date.today().day, 23, 59, 59):
            time.sleep(1)


if __name__ == '__main__':
    unittest.main()
