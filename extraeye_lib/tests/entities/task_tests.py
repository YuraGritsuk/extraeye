import unittest

from datetime import date
from extraeye_lib.entities.task import (
    Task,
    Status,
    Priority,
    Permission
)


class TaskTests(unittest.TestCase):

    def test_initializing(self):
        initializing_info_list = [
            ['user_1', 'task_1', Priority.LOW, Status.ACTIVE, 1, date(2020, 1, 1), date(2019, 1, 1),
             date(2019, 1, 1), None, {'Stephen': Permission.READ}, [2, 3], [], ['sport']],

            ['user_2', 'task_2', Priority.HIGH, Status.FAILED, 2, date(2021, 3, 3), date(2015, 1, 1),
             date(2015, 3, 1), 1, {}, [1, 3], [], ['sport', 'study']],

            ['user_3', 'task_3', Priority.MEDIUM, Status.DONE, 3, date(2025, 1, 1), date(2018, 1, 1),
             date(2021, 1, 1), 2, {'Stephen': Permission.READ, 'Steven': Permission.READ}, [2], [], ['study']]
        ]

        for init_info in initializing_info_list:
            task = Task(
                       user_login=init_info[0],
                       description=init_info[1],
                       priority=init_info[2],
                       status=init_info[3],
                       t_id=init_info[4],
                       deadline=init_info[5],
                       creation_datetime=init_info[6],
                       edition_datetime=init_info[7],
                       parent=init_info[8],
                       other=init_info[9],
                       subtasks=init_info[10],
                       reminders=init_info[11],
                       groups=init_info[12]
                   )

            self.assertEqual(task.user_login, init_info[0])
            self.assertEqual(task.description, init_info[1])
            self.assertEqual(task.priority, init_info[2])
            self.assertEqual(task.status, init_info[3])
            self.assertEqual(task.t_id, init_info[4])
            self.assertEqual(task.deadline, init_info[5])
            self.assertEqual(task.time.creation, init_info[6])
            self.assertEqual(task.time.edition, init_info[7])
            self.assertEqual(task.parent, init_info[8])
            self.assertEqual(task.other, init_info[9])
            self.assertEqual(task.subtasks, init_info[10])
            self.assertEqual(task.reminders, init_info[11])
            self.assertEqual(task.groups, init_info[12])

    def test_is_archive_method(self):
        initializing_statuses = [Status.ACTIVE, Status.FAILED, Status.DONE]
        correct_answers = [False, True, True]

        for init_status, correct_answer in zip(initializing_statuses, correct_answers):
            task = Task('', '', status=init_status)
            self.assertEqual(task.is_archive(), correct_answer)

    def test_is_archive_Status_method(self):
        statuses = [Status.ACTIVE, Status.FAILED, Status.DONE]
        correct_answers = [False, True, True]

        for status, correct_answer in zip(statuses, correct_answers):
            self.assertEqual(Status.is_archive(status), correct_answer)


if __name__ == '__main__':
    unittest.main()
