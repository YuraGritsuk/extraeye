import unittest

from datetime import date
from extraeye_lib.entities.reminder import (
    Reminder,
    ReminderOwner,
    EntitiesType
)


class ReminderTests(unittest.TestCase):

    def test_initializing(self):
        initializing_info_list = [
            ['user_1', 'reminder_1', date(2020, 1, 1), 1, date(2019, 1, 1), date(2019, 1, 1), True,
             ReminderOwner(EntitiesType.TASK, 1, True), {}, ['sport']],

            ['user_1', 'reminder_1', date(2022, 1, 1), 2, date(2018, 1, 1), date(2019, 1, 1), False,
             ReminderOwner(EntitiesType.TASK, 2, False), {}, ['sport', 'study']],

            ['user_1', 'reminder_1', date(2025, 1, 1), 3, date(2018, 3, 1), date(2019, 1, 3), True,
             ReminderOwner(EntitiesType.TASK, 3, True), {}, ['sport']]
        ]

        for init_info in initializing_info_list:
            reminder = Reminder(
                           user_login=init_info[0],
                           description=init_info[1],
                           notify_date=init_info[2],
                           r_id=init_info[3],
                           creation_datetime=init_info[4],
                           edition_datetime=init_info[5],
                           is_notified=init_info[6],
                           owner=init_info[7],
                           other=init_info[8],
                           groups=init_info[9]
                       )

            self.assertEqual(reminder.user_login, init_info[0])
            self.assertEqual(reminder.description, init_info[1])
            self.assertEqual(reminder.notify_date, init_info[2])
            self.assertEqual(reminder.r_id, init_info[3])
            self.assertEqual(reminder.time.creation, init_info[4])
            self.assertEqual(reminder.time.edition, init_info[5])
            self.assertEqual(reminder.is_notified, init_info[6])
            self.assertEqual(reminder.owner, init_info[7])
            self.assertEqual(reminder.other, init_info[8])
            self.assertEqual(reminder.groups, init_info[9])

    def test_is_archive_method(self):
        initializing_statuses = [
            [False, False],
            [True, True],
            [False, True]
        ]
        correct_answers = [False, True, True]

        for init_status, correct_answer in zip(initializing_statuses, correct_answers):
            reminder = Reminder('', '', None, is_notified=init_status[0], owner=ReminderOwner(EntitiesType.TASK, '1',
                                                                                              init_status[1]))
            self.assertEqual(reminder.is_archive(), correct_answer)
