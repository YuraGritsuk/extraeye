import unittest

from datetime import date
from extraeye_lib.entities.scheduler import Scheduler


class SchedulerTests(unittest.TestCase):

    def test_initializing(self):
        initializing_info_list = [
            ['user_1', {'task_description: task_1'}, date(2018, 1, 1), date(2018, 1, 2), 0, 1, date.today(),
             date.today()],

            ['user_2', {'task_description: task_2'}, date(2019, 2, 1), date(2019, 3, 2), 0, 2, date(2020, 1, 1),
             date(2020, 1, 1)],

            ['user_3', {'task_description: task_3'}, date(2025, 9, 10), date(2026, 1, 9), 0, 2, date(2020, 1, 1),
             date(2020, 1, 1)]
        ]

        for init_info in initializing_info_list:
            scheduler = Scheduler(
                            user_login=init_info[0],
                            task_info=init_info[1],
                            start_date=init_info[2],
                            end_date=init_info[3],
                            tasks_upload=init_info[4],
                            s_id=init_info[5],
                            creation_datetime=init_info[6],
                            edition_datetime=init_info[7]
                        )

            self.assertEqual(scheduler.user_login, init_info[0])
            self.assertEqual(scheduler.task_info, init_info[1])
            self.assertEqual(scheduler.start_date, init_info[2])
            self.assertEqual(scheduler.end_date, init_info[3])
            self.assertEqual(scheduler.tasks_upload, init_info[4])
            self.assertEqual(scheduler.s_id, init_info[5])
            self.assertEqual(scheduler.time.creation, init_info[6])
            self.assertEqual(scheduler.time.edition, init_info[7])

    def test_scheduler_is_create_at_period_method(self):
        initializing_info_list = [
            [date(2020, 1, 1), date(2020, 1, 5), False, False, True, 1],
            [date(2020, 1, 1), date(2020, 1, 5), False, False, True, 1],
            [date(2020, 1, 1), date(2021, 1, 1), False, True, False, 2],
            [date(2020, 1, 1), date(2021, 1, 5), True, False, False, 1],
            [date(2020, 1, 2), date(2030, 1, 5), True, False, False, 2]
        ]

        entering_method_parameters = [
            [date(2020, 1, 4), date(2020, 1, 5)],
            [date(2020, 1, 6), date(2020, 1, 7)],
            [date(2020, 3, 1), date(2020, 3, 5)],
            [date(2022, 1, 1), date(2022, 1, 5)],
            [date(2026, 1, 1), date(2027, 1, 3)]
        ]

        correct_answers = [True, False, True, False, True]

        for init_info, method_parameters, correct_answer in zip(
                                                                initializing_info_list,
                                                                entering_method_parameters,
                                                                correct_answers
                                                                ):

            scheduler = (Scheduler(
                             user_login='',
                             task_info={},
                             start_date=init_info[0],
                             end_date=init_info[1],
                             is_year=init_info[2],
                             is_month=init_info[3],
                             is_day=init_info[4],
                             amount_differences=init_info[5]
                         ))

            self.assertEqual(scheduler.is_create_at_period(method_parameters[0], method_parameters[1]),
                             correct_answer)

    def test_get_next_date_method(self):
        initializing_info_list = [
            [date(2020, 1, 1), date(2020, 1, 5), False, False, True, 1, 0],
            [date(2020, 1, 1), date(2020, 1, 5), False, False, True, 1, 1],
            [date(2020, 1, 1), date(2021, 1, 1), False, True, False, 2, 3],
            [date(2020, 1, 1), date(2030, 1, 5), True, False, False, 1, 5],
            [date(2020, 1, 2), date(2030, 1, 5), True, False, False, 2, 2]
        ]

        correct_answers = [date(2020, 1, 1), date(2020, 1, 2), date(2020, 7, 1), date(2025, 1, 1), date(2024, 1, 2)]

        for init_info, correct_answer in zip(initializing_info_list, correct_answers):

            scheduler = (Scheduler(
                             user_login='',
                             task_info={},
                             start_date=init_info[0],
                             end_date=init_info[1],
                             is_year=init_info[2],
                             is_month=init_info[3],
                             is_day=init_info[4],
                             amount_differences=init_info[5],
                             tasks_upload=init_info[6]
                         ))

            self.assertEqual(scheduler.get_next_date(), correct_answer)

    def test_get_task_info_method(self):
        task_info = {'description': 'task_description'}
        scheduler = Scheduler('', task_info)

        self.assertEqual(scheduler.get_task_info(), task_info)

    def test_is_empty_method(self):
        initializing_info_list = [
            [date(2020, 1, 1), date(2020, 1, 5), False, False, True, 1, 0],
            [date(2020, 1, 1), date(2020, 1, 1), False, False, True, 1, 1],
            [date(2020, 1, 1), date(2020, 5, 1), False, True, False, 2, 3],
            [date(2020, 1, 1), date(2024, 1, 5), True, False, False, 1, 5],
            [date(2020, 1, 2), date(2030, 1, 5), True, False, False, 2, 2]
        ]

        correct_answers = [False, True, True, True, False]

        for init_info, correct_answer in zip(initializing_info_list, correct_answers):
            scheduler = (Scheduler(
                             user_login='',
                             task_info={},
                             start_date=init_info[0],
                             end_date=init_info[1],
                             is_year=init_info[2],
                             is_month=init_info[3],
                             is_day=init_info[4],
                             amount_differences=init_info[5],
                             tasks_upload=init_info[6]
                         ))

            self.assertEqual(scheduler.is_empty(), correct_answer)

    def test_pop_task_method(self):
        initializing_info_list = [
            [{'description': 'task_1'}, date(2020, 1, 1), date(2020, 1, 5), False, False, True, 1, 0],
            [{'description': 'task_2'}, date(2020, 1, 1), date(2020, 1, 5), False, False, True, 1, 1],
            [{'description': 'task_3'}, date(2020, 1, 1), date(2021, 1, 1), False, True, False, 2, 3],
            [{'description': 'task_4'}, date(2020, 1, 1), date(2030, 1, 5), True, False, False, 1, 5],
            [{'description': 'task_5'}, date(2020, 1, 2), date(2030, 1, 5), True, False, False, 2, 2]
        ]

        correct_answers = [
            ['task_1', 1],
            ['task_2', 2],
            ['task_3', 4],
            ['task_4', 6],
            ['task_5', 3],
        ]

        for init_info, correct_answer in zip(initializing_info_list, correct_answers):
            scheduler = (Scheduler(
                             user_login='',
                             task_info=init_info[0],
                             start_date=init_info[1],
                             end_date=init_info[2],
                             is_year=init_info[3],
                             is_month=init_info[4],
                             is_day=init_info[5],
                             amount_differences=init_info[6],
                             tasks_upload=init_info[7]
                         ))

            self.assertEqual(scheduler.pop_task().description, correct_answer[0])
            self.assertEqual(scheduler.tasks_upload, correct_answer[1])


if __name__ == '__main__':
    unittest.main()
