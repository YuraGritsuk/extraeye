import unittest

from extraeye_lib.entities.user import User
from extraeye_lib.entities.task import (
    Shared,
    SharedType,
    Permission
)


class UserTests(unittest.TestCase):

    def test_initializing(self):
        initializing_info_list = [
            ['user_1', [1, 2, 3], [4, 5, 6], [Shared('Peter', Permission.READ, SharedType.SEND, 100)],
             [7, 8, 9], None],
            ['user_2', [10, 20, 30], [40, 50, 60], [], [70, 80, 90], None],
        ]

        for init_info in initializing_info_list:
            user = User(
                       login=init_info[0],
                       tasks=init_info[1],
                       reminders=init_info[2],
                       shared_tasks=init_info[3],
                       schedulers=init_info[4],
                       human=init_info[5]
                   )

            self.assertEqual(user.login, init_info[0])
            self.assertEqual(user.tasks, init_info[1])
            self.assertEqual(user.reminders, init_info[2])
            self.assertEqual(user.shared_tasks, init_info[3])
            self.assertEqual(user.schedulers, init_info[4])
            self.assertEqual(user.human, init_info[5])


if __name__ == '__main__':
    unittest.main()
