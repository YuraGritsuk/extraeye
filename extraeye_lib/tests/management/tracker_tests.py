import os
import shutil
import unittest

from extraeye_lib.management.tracker import Tracker
from extraeye_lib.storage import storage
from extraeye_lib.entities.user import User
from dateutil.relativedelta import relativedelta
from extraeye_lib.entities.scheduler import Scheduler
from datetime import (
    datetime,
    date
)
from extraeye_lib.storage.path import Path
from extraeye_lib.entities.reminder import (
    Reminder,
    EntitiesType,
    ReminderOwner
)
from extraeye_lib.entities.task import (
    Task,
    Status
)
from extraeye_lib.exceptions.name_exceptions import GroupNameError

class TrackerTests(unittest.TestCase):
    TEST_STORAGE_PATH = os.path.join(os.environ['HOME'], 'Extraeye_Test_Storage')

    USER_LOGIN = 'John'
    DESCRIPTION = 'Big and difficult'
    ENTITY_ID = '1'

    def setUp(self):
        path = Path(base_dir=self.TEST_STORAGE_PATH)
        self.tracker = Tracker(path)
        storage.write_user_to_json(User(self.USER_LOGIN), path)

    def tearDown(self):
        shutil.rmtree(self.TEST_STORAGE_PATH)

    def test_add_task(self):
        self.tracker.add_task(self.USER_LOGIN, self.DESCRIPTION)
        user_tasks = storage.get_all_tasks(self.USER_LOGIN, path=self.tracker.path)

        self.assertEqual(1, len(user_tasks))
        self.assertEqual(self.DESCRIPTION, user_tasks[0].description)

    def test_add_subtask(self):
        self.tracker.add_task(self.USER_LOGIN, self.DESCRIPTION)

        parent_task = storage.get_all_tasks(self.USER_LOGIN, path=self.tracker.path)[0]
        parent_id = parent_task.t_id

        subtask_description = 'subtask_description'
        self.tracker.add_subtask(self.USER_LOGIN, parent_id, subtask_description)

        user_tasks = storage.get_all_tasks(self.USER_LOGIN, path=self.tracker.path)
        self.assertTrue(any(task.parent and task.parent == parent_id for task in user_tasks))
        self.assertEqual(parent_task.description, self.DESCRIPTION)

    def test_delete_task(self):
        self.tracker.add_task(self.USER_LOGIN, self.DESCRIPTION)

        deleted_task = storage.get_all_tasks(self.USER_LOGIN, path=self.tracker.path)[0]
        self.tracker.delete_task(self.USER_LOGIN, deleted_task.t_id)

        self.assertTrue(not storage.get_all_tasks(self.USER_LOGIN, path=self.tracker.path))

    def test_delete_task_with_subtasks(self):
        task_1 = Task(self.USER_LOGIN, 'task_1', t_id='1', subtasks=['2'])
        task_2 = Task(self.USER_LOGIN, 'task_2', t_id='2', parent='1', subtasks=['3'])
        task_3 = Task(self.USER_LOGIN, 'task_3', t_id='3', parent='2')

        tasks = [task_1, task_2, task_3]

        user = storage.get_user_by_login(self.USER_LOGIN, path=self.tracker.path)
        user.tasks.extend([task.t_id for task in tasks])

        storage.write_user_to_json(user, path=self.tracker.path)
        storage.write_tasks_to_json(self.USER_LOGIN, tasks, path=self.tracker.path)

        self.tracker._delete_task_with_subtasks(task_1)

        self.assertTrue(not storage.get_all_tasks(self.USER_LOGIN, path=self.tracker.path))
        for task in tasks:
            self.assertEqual(task.user_login, self.USER_LOGIN)

    def test_delete_task_from_users(self):
        self.tracker.add_task(self.USER_LOGIN, self.DESCRIPTION)
        self.tracker._delete_task_from_users(storage.get_all_tasks(self.USER_LOGIN, path=self.tracker.path)[0])

        user = storage.get_user_by_login(self.USER_LOGIN, path=self.tracker.path)
        self.assertTrue(not user.tasks)
        self.assertTrue(all(self.DESCRIPTION == task.description for task in
                            storage.get_all_tasks(self.USER_LOGIN, self.tracker.path)))

    def test_done_task(self):
        self.tracker.add_task(self.USER_LOGIN, self.DESCRIPTION)

        done_task = storage.get_all_tasks(self.USER_LOGIN, path=self.tracker.path)[0]
        self.tracker.done_task(self.USER_LOGIN, done_task.t_id)

        self.assertTrue(any(Status.DONE == task.status for task in storage.get_all_tasks(
                                                                       self.USER_LOGIN,
                                                                       path=self.tracker.path
                                                                   )))
        self.assertTrue(storage.get_all_tasks(self.USER_LOGIN, self.tracker.path)[0].description == self.DESCRIPTION)

    def test_recursive_done_task(self):
        task_1 = Task(self.USER_LOGIN, 'task_1', t_id='1', subtasks=['2'])
        task_2 = Task(self.USER_LOGIN, 'task_2', t_id='2', parent='1', subtasks=['3'])
        task_3 = Task(self.USER_LOGIN, 'task_3', t_id='3', parent='2')
        tasks = [task_1, task_2, task_3]

        storage.write_tasks_to_json(self.USER_LOGIN, tasks, path=self.tracker.path)

        self.tracker._recursive_done_task(task_2)

        self.assertTrue(all(Status.DONE == task.status for task in storage.get_all_tasks(
                                                                       self.USER_LOGIN,
                                                                       path=self.tracker.path
                                                                   )))
        self.assertTrue(all(self.USER_LOGIN == task.user_login for task in storage.get_all_tasks(
                                                                                self.USER_LOGIN,
                                                                                path=self.tracker.path
                                                                           )))

    def test_fail_task(self):
        self.tracker.add_task(self.USER_LOGIN, self.DESCRIPTION)

        failed_task = storage.get_all_tasks(self.USER_LOGIN, path=self.tracker.path)[0]
        self.tracker.fail_task(self.USER_LOGIN, failed_task.t_id)

        self.assertTrue(all(Status.FAILED == task.status for task in storage.get_all_tasks(
                                                                         self.USER_LOGIN,
                                                                         path=self.tracker.path
                                                                     )))
        self.assertTrue(all(self.DESCRIPTION == task.description for task in storage.get_all_tasks(
                                                                                 self.USER_LOGIN,
                                                                                 path=self.tracker.path
                                                                             )))

    def test_recursive_fail_task(self):
        task_1 = Task(self.USER_LOGIN, 'task_1', t_id='1', subtasks=['2'])
        task_2 = Task(self.USER_LOGIN, 'task_2', t_id='2', parent='1', subtasks=['3'])
        task_3 = Task(self.USER_LOGIN, 'task_3', t_id='3', parent='2')

        tasks = [task_1, task_2, task_3]
        storage.write_tasks_to_json(self.USER_LOGIN, tasks, path=self.tracker.path)

        self.tracker._recursive_fail_task(task_2)

        self.assertTrue(all(Status.FAILED == task.status for task in storage.get_all_tasks(
                                                                         self.USER_LOGIN,
                                                                         path=self.tracker.path
                                                                     )))

    def test_refresh_task_status(self):
        self.tracker.add_task(self.USER_LOGIN, self.DESCRIPTION)

        self.tracker._refresh_task_status(
            storage.get_all_tasks(self.USER_LOGIN, path=self.tracker.path)[0],
            Status.DONE,
        )

        self.assertTrue(Status.DONE == storage.get_all_tasks(self.USER_LOGIN, path=self.tracker.path)[0].status)
        self.assertTrue(storage.get_all_tasks(self.USER_LOGIN, self.tracker.path)[0].user_login == self.USER_LOGIN)

    def test_add_reminder(self):
        self.tracker.add_reminder(self.USER_LOGIN, self.DESCRIPTION, date(2020, 1, 1))

        self.assertEqual(1, len(storage.get_all_reminders(self.USER_LOGIN, path=self.tracker.path)))
        self.assertEqual(
            self.DESCRIPTION,
            storage.get_all_reminders(self.USER_LOGIN, path=self.tracker.path)[0].description
        )

    def test_delete_reminder(self):
        self.tracker.add_reminder(self.USER_LOGIN, self.DESCRIPTION, date(2020, 1, 1))

        deleted_reminder = storage.get_all_reminders(self.USER_LOGIN, path=self.tracker.path)[0]
        self.tracker.delete_reminder(self.USER_LOGIN, deleted_reminder.r_id)

        self.assertTrue(not storage.get_all_reminders(self.USER_LOGIN, path=self.tracker.path))

    def test_delete_reminder_from_user(self):
        self.tracker.add_reminder(self.USER_LOGIN, 'reminder_1', None)

        self.tracker._delete_reminder_from_user(
            storage.get_all_reminders(self.USER_LOGIN, path=self.tracker.path)[0],
        )

        user = storage.get_user_by_login(self.USER_LOGIN, path=self.tracker.path)
        self.assertTrue(not user.reminders)

    def test_refresh_task_reminders_status(self):
        task_reminder = Reminder(
            self.USER_LOGIN,
            self.DESCRIPTION,
            None,
            r_id='10',
            owner=ReminderOwner(EntitiesType.TASK, '1', True),
        )
        task = Task(self.USER_LOGIN, self.DESCRIPTION, t_id='1', reminders=['10'])

        storage.write_reminders_to_json(self.USER_LOGIN, [task_reminder], path=self.tracker.path)
        storage.write_tasks_to_json(self.USER_LOGIN, [task], path=self.tracker.path)

        self.tracker._refresh_task_reminders_status(task, True)

        self.assertTrue(storage.get_all_reminders(self.USER_LOGIN, self.tracker.path)[0].is_archive())
        self.assertTrue(storage.get_all_reminders(self.USER_LOGIN, self.tracker.path)[0].description == self.DESCRIPTION)

    def test_add_group_to_task(self):
        task = Task(self.USER_LOGIN, self.DESCRIPTION, t_id='1')
        storage.write_tasks_to_json(self.USER_LOGIN, [task], path=self.tracker.path)

        self.tracker.add_group_to_task(self.USER_LOGIN, task.t_id, 'sport')

        self.assertTrue(storage.get_all_tasks(self.USER_LOGIN, path=self.tracker.path)[0].groups)
        self.assertTrue(storage.get_all_tasks(self.USER_LOGIN, path=self.tracker.path)[0].description == self.DESCRIPTION)

    def test_add_group_to_reminder(self):
        reminder = Reminder(self.USER_LOGIN, self.DESCRIPTION, None)
        storage.write_reminders_to_json(self.USER_LOGIN, [reminder], path=self.tracker.path)

        self.tracker.add_group_to_reminder(self.USER_LOGIN, reminder.r_id, 'sport')

        self.assertTrue(storage.get_all_reminders(self.USER_LOGIN, path=self.tracker.path)[0].groups)

    def test_edit_task(self):
        testing_deadline = datetime(year=2018, month=5, day=12)

        task = Task(self.USER_LOGIN, self.DESCRIPTION, t_id='1', deadline=testing_deadline)
        storage.write_tasks_to_json(self.USER_LOGIN, [task], path=self.tracker.path)

        new_description = 'Very big and too difficult'
        self.tracker.edit_task(self.USER_LOGIN, task.t_id, new_description)

        self.assertEqual(
            new_description,
            storage.get_all_tasks(self.USER_LOGIN, path=self.tracker.path)[0].description
        )
        self.assertEqual(
            testing_deadline,
            storage.get_all_tasks(self.USER_LOGIN, path=self.tracker.path)[0].deadline
        )

    def test_edit_reminder(self):
        testing_notify_date = date(year=2018, month=5, day=12)

        reminder = Reminder(self.USER_LOGIN, self.DESCRIPTION, r_id='1', notify_date=testing_notify_date)
        storage.write_reminders_to_json(self.USER_LOGIN, [reminder], path=self.tracker.path)

        new_description = 'Very big and too difficult'
        self.tracker.edit_reminder(self.USER_LOGIN, reminder.r_id, new_description)

        self.assertEqual(
            new_description,
            storage.get_all_reminders(self.USER_LOGIN, path=self.tracker.path)[0].description
        )
        self.assertEqual(
            testing_notify_date,
            storage.get_all_reminders(self.USER_LOGIN, path=self.tracker.path)[0].notify_date
        )

    def test_get_group(self):
        group_name = 'sport'

        task_1 = Task(self.USER_LOGIN, self.DESCRIPTION, t_id='1', groups=[group_name])
        task_2 = Task(self.USER_LOGIN, self.DESCRIPTION, t_id='2')

        reminder_1 = Reminder(self.USER_LOGIN, self.DESCRIPTION, None, r_id='10', groups=[group_name])

        storage.write_tasks_to_json(self.USER_LOGIN, [task_1, task_2], path=self.tracker.path)
        storage.write_reminders_to_json(self.USER_LOGIN, [reminder_1], path=self.tracker.path)

        group = self.tracker.get_group(self.USER_LOGIN, group_name)

        self.assertTrue(all(task_1.t_id == group_task.t_id for group_task in group.tasks))
        self.assertTrue(all(reminder_1.r_id == group_reminder.r_id for group_reminder in group.reminders))

    def test_get_all_groups(self):
        group_1_name = 'sport'
        group_2_name = 'study'

        task_1 = Task(self.USER_LOGIN, self.DESCRIPTION, t_id='1', groups=[group_1_name])
        task_2 = Task(self.USER_LOGIN, self.DESCRIPTION, t_id='2', groups=[group_2_name])

        reminder_1 = Reminder(self.USER_LOGIN, self.DESCRIPTION, None, r_id='10', groups=[group_1_name])

        storage.write_tasks_to_json(self.USER_LOGIN, [task_1, task_2], path=self.tracker.path)
        storage.write_reminders_to_json(self.USER_LOGIN, [reminder_1], path=self.tracker.path)

        groups = self.tracker.get_all_groups(self.USER_LOGIN)

        self.assertTrue(any(group_1_name == group.name for group in groups))
        self.assertTrue(any(group_2_name == group.name for group in groups))

        for group in groups:
            if group.name == group_1_name:
                self.assertTrue(all(task_1.t_id == group_task.t_id for group_task in group.tasks))
                self.assertTrue(all(reminder_1.r_id == group_reminder.r_id for group_reminder in group.reminders))
            else:
                self.assertTrue(all(task_2.t_id == group_task.t_id for group_task in group.tasks))

    def test_get_all_tasks(self):
        for _ in range(5):
            self.tracker.add_task(self.USER_LOGIN, self.DESCRIPTION)

        self.assertEqual(5, len(self.tracker.get_all_tasks(self.USER_LOGIN)))

    def test_get_all_subtasks(self):
        task_1 = Task(self.USER_LOGIN, 'task_1', t_id='1', subtasks=['2'])
        task_2 = Task(self.USER_LOGIN, 'task_2', t_id='2', parent='1', subtasks=['3'])
        task_3 = Task(self.USER_LOGIN, 'task_3', t_id='3', parent='2')

        storage.write_tasks_to_json(self.USER_LOGIN, [task_1, task_2, task_3], path=Path(self.TEST_STORAGE_PATH))

        self.assertEqual(3, len(self.tracker.get_all_subtasks(self.USER_LOGIN, task_1.t_id)))

    def test_get_tasks_from_period(self):
        begin_date = date(2015, 1, 1)
        end_date = date(2015, 2, 1)

        task_1 = Task(
            self.USER_LOGIN,
            self.DESCRIPTION,
            t_id='1',
            deadline=datetime(year=2018, month=5, day=12)
        )
        task_2 = Task(
            self.USER_LOGIN,
            self.DESCRIPTION,
            t_id='2',
            deadline=datetime(year=2015, month=5, day=12)
        )
        task_3 = Task(
            self.USER_LOGIN,
            self.DESCRIPTION,
            t_id='3',
            deadline=datetime(year=2015, month=1, day=15)
        )
        storage.write_tasks_to_json(self.USER_LOGIN, [task_1, task_2, task_3], path=self.tracker.path)

        self.assertTrue(all(task_3.t_id == task_from_period.t_id for task_from_period in
                            self.tracker.get_tasks_from_period(
                                self.USER_LOGIN,
                                begin_date,
                                end_date,
                            )))

    def test_get_all_reminders(self):
        for _ in range(5):
            self.tracker.add_reminder(self.USER_LOGIN, self.DESCRIPTION, None)

        self.assertEqual(5, len(self.tracker.get_all_reminders(self.USER_LOGIN)))

    def test_get_reminders_from_period(self):
        begin_date = date(2015, 1, 1)
        end_date = date(2015, 2, 1)

        reminder_1 = Reminder(
            self.USER_LOGIN,
            self.DESCRIPTION,
            r_id='1',
            notify_date=date(year=2018, month=5, day=12)
        )
        reminder_2 = Reminder(
            self.USER_LOGIN,
            self.DESCRIPTION,
            r_id='2',
            notify_date=date(year=2015, month=3, day=19)
        )
        reminder_3 = Reminder(
            self.USER_LOGIN,
            self.DESCRIPTION,
            r_id='3',
            notify_date=date(year=2018, month=1, day=12)
        )
        storage.write_reminders_to_json(
            self.USER_LOGIN,
            [reminder_1, reminder_2, reminder_3],
            path=self.tracker.path
        )

        self.assertTrue(all(reminder_3.r_id == reminder_from_period.t_id for reminder_from_period in
                            self.tracker.get_reminders_from_period(
                                self.USER_LOGIN,
                                begin_date,
                                end_date,
                            )))

    def test_share_task(self):
        receiver_login = 'Receiver'
        receiver = User(receiver_login)
        storage.write_user_to_json(receiver, path=self.tracker.path)

        share_task = Task(self.USER_LOGIN, self.DESCRIPTION, t_id='1')
        storage.write_tasks_to_json(self.USER_LOGIN, [share_task], path=self.tracker.path)

        self.tracker.share_task(self.USER_LOGIN, receiver_login, share_task.t_id)

        receiver = storage.get_user_by_login(receiver_login, path=self.tracker.path)
        self.assertTrue(all(share_task.t_id == shared_task.t_id for shared_task in receiver.shared_tasks))

    def test_get_shared_tasks(self):
        receiver_login = 'Receiver'
        receiver = User(receiver_login)
        storage.write_user_to_json(receiver, path=self.tracker.path)

        share_task = Task(self.USER_LOGIN, self.DESCRIPTION, t_id='1')
        storage.write_tasks_to_json(self.USER_LOGIN, [share_task], path=self.tracker.path)

        self.tracker.share_task(self.USER_LOGIN, receiver_login, share_task.t_id)

        self.assertTrue(all(share_task.t_id == shared_task.t_id for shared_task in
                            self.tracker.get_shared_tasks(receiver_login)))

    def test_add_scheduler(self):
        for _ in range(5):
            self.tracker.add_scheduler(self.USER_LOGIN, {'description': self.DESCRIPTION})

        self.assertEqual(5, len(storage.get_all_schedulers(self.USER_LOGIN, path=self.tracker.path)))

    def test_check_scheduler(self):
        self.tracker.add_scheduler(
            self.USER_LOGIN,
            {'description': self.DESCRIPTION},
            end_date=date.today(),
        )

        self.tracker.check_scheduler(self.USER_LOGIN,)

        self.assertEqual(
            self.DESCRIPTION,
            storage.get_all_schedulers(self.USER_LOGIN, path=self.tracker.path)[0].task_info['description']
        )

    def test_get_schedulers_from_period(self):
        begin_date = date(2018, 3, 10)
        end_date = date(2018, 4, 10)

        scheduler_1 = Scheduler(
            self.USER_LOGIN,
            {'description': self.DESCRIPTION},
            s_id='1',
            start_date=date(2015, 3, 15),
            end_date=date(2020, 3, 15),
            is_year=True
        )
        scheduler_2 = Scheduler(
            self.USER_LOGIN,
            {'description': self.DESCRIPTION},
            s_id='2',
            start_date=date(2018, 1, 15),
            end_date=date(2018, 5, 15),
            is_month=True,
            amount_differences=2,
        )
        scheduler_3 = Scheduler(
            self.USER_LOGIN,
            {'description': self.DESCRIPTION},
            s_id='3',
            start_date=date(2018, 1, 1),
            end_date=date(2018, 1, 9),
        )

        storage.write_schedulers_to_json(
            self.USER_LOGIN,
            [scheduler_1, scheduler_2, scheduler_3],
            path=self.tracker.path
        )

        scheduler_from_period = self.tracker.get_schedulers_from_period(
            self.USER_LOGIN,
            begin_date,
            end_date,
        )

        for scheduler in scheduler_from_period:
            self.assertTrue(scheduler.s_id == scheduler_1.s_id or scheduler.s_id == scheduler_2.s_id)
            self.assertEqual(self.USER_LOGIN, scheduler.user_login)

    def test_mark_overdue_tasks(self):
        test_deadline = datetime(
                year=date.today().year,
                month=date.today().month,
                day=date.today().day
        )

        self.tracker.add_task(
            self.USER_LOGIN,
            self.DESCRIPTION,
            deadline=test_deadline - relativedelta(days=1),
        )

        self.tracker.mark_overdue_tasks(self.USER_LOGIN)

        self.assertTrue(all(Status.FAILED == task.status for task in storage.get_all_tasks(
                                                                         self.USER_LOGIN,
                                                                         path=self.tracker.path
                                                                     )))
        
    def test_mark_reminders_as_notified(self):
        for _ in range(5):
            self.tracker.add_reminder(self.USER_LOGIN, self.DESCRIPTION, notify_date=date.today())

        reminders = self.tracker.get_all_reminders(self.USER_LOGIN)
        self.tracker.mark_reminders_as_notified(self.USER_LOGIN, reminders)

        self.assertTrue(all(reminder.is_notified for reminder in self.tracker.get_all_reminders(self.USER_LOGIN)))

    def test_delete_scheduler(self):
        self.tracker.add_scheduler(self.USER_LOGIN, {'description': self.DESCRIPTION})

        deleted_scheduler = storage.get_all_schedulers(self.USER_LOGIN, path=self.tracker.path)[0]
        self.tracker.delete_scheduler(self.USER_LOGIN, deleted_scheduler.s_id)

        self.assertTrue(not storage.get_all_schedulers(self.USER_LOGIN, path=self.tracker.path))

    def test_delete_group_from_task(self):
        task = Task(self.USER_LOGIN, self.DESCRIPTION, t_id='1')
        group_name = 'test_group'
        incorrect_group_name = 'fake_group'

        storage.write_tasks_to_json(self.USER_LOGIN, [task], path=self.tracker.path)

        self.tracker.add_group_to_task(self.USER_LOGIN, task.t_id, group_name)

        with self.assertRaises(GroupNameError):
            self.tracker.delete_group_from_task(self.USER_LOGIN, task.t_id, incorrect_group_name)

        self.tracker.delete_group_from_task(self.USER_LOGIN, task.t_id, group_name)

        edited_task = storage.get_task_by_id(self.USER_LOGIN, task.t_id, path=self.tracker.path)
        self.assertFalse(group_name in edited_task.groups)
        self.assertEqual(self.DESCRIPTION, edited_task.description)

    def test_delete_group_from_reminder(self):
        reminder = Reminder(self.USER_LOGIN, self.DESCRIPTION, None, r_id='1')
        group_name = 'test_group'
        incorrect_group_name = 'fake_group'

        storage.write_reminders_to_json(self.USER_LOGIN, [reminder], path=self.tracker.path)

        self.tracker.add_group_to_reminder(self.USER_LOGIN, reminder.r_id, group_name)

        with self.assertRaises(GroupNameError):
            self.tracker.delete_group_from_reminder(self.USER_LOGIN, reminder.r_id, incorrect_group_name)

        self.tracker.delete_group_from_reminder(self.USER_LOGIN, reminder.r_id, group_name)

        edited_reminder = storage.get_reminder_by_id(self.USER_LOGIN, reminder.r_id, path=self.tracker.path)
        self.assertFalse(group_name in edited_reminder.groups)
        self.assertEqual(self.DESCRIPTION, edited_reminder.description)
