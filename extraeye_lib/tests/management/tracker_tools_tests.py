import time
import unittest

from collections import namedtuple
from extraeye_lib.management import tracker_tools
from extraeye_lib.entities.user import User
from dateutil.relativedelta import relativedelta
from extraeye_lib.entities.scheduler import Scheduler
from datetime import (
    date,
    datetime
)
from extraeye_lib.entities.task import (
    Task,
    Shared,
    Permission,
    SharedType
)


class TrackerToolsTests(unittest.TestCase):
    USER_LOGIN = 'Patrick'
    DESCRIPTION = 'Big and difficult'
    ENTITY_ID = '1'

    def test_create_subtask_relationships(self):
        parent_task = Task(self.USER_LOGIN, self.DESCRIPTION, t_id='1')
        subtask = Task(self.USER_LOGIN, self.DESCRIPTION, t_id='2')

        tracker_tools.create_subtask_relationships(parent_task, subtask)

        self.assertIn(subtask.t_id, parent_task.subtasks)
        self.assertEqual(parent_task.t_id, subtask.parent)

        with self.assertRaises(IndexError):
            tracker_tools.create_subtask_relationships(parent_task, subtask)

    def test_is_group_name_in_groups(self):
        Group = namedtuple('Group', 'name')

        testing_groups = [Group('sport'), Group('study'), Group('homework')]
        fake_groups = [Group('lie'), Group('ideal')]

        for group in testing_groups:
            self.assertTrue(group in testing_groups)

        for group in fake_groups:
            self.assertFalse(group in testing_groups)

    def test_find_group_index_in_groups(self):
        Group = namedtuple('Group', 'name')

        testing_groups = [Group('sport'), Group('study'), Group('homework')]

        for group in testing_groups:
            self.assertEqual(testing_groups.index(group),
                             tracker_tools.find_group_index_in_groups(testing_groups, group.name))

    def test_check_group_name_unique(self):
        testing_groups = ['sport', 'study', 'homework']
        unique_groups = ['lie', 'ideal']

        task = Task(self.USER_LOGIN, self.DESCRIPTION, groups=testing_groups)

        for not_unique_group in testing_groups:
            with self.assertRaises(NameError):
                tracker_tools.check_group_name_unique_in_entity(task, not_unique_group)

        for unique_group in unique_groups:
            tracker_tools.check_group_name_unique_in_entity(task, unique_group)

    def test_create_receiver_to_task_relationships(self):
        receiver = User(self.USER_LOGIN)
        receiver_rights = Permission.READ

        task = Task(self.USER_LOGIN, self.DESCRIPTION)

        tracker_tools.create_receiver_to_task_relationships(receiver, task, receiver_rights)

        self.assertEqual(receiver_rights, task.other[receiver.login])
        self.assertEqual(self.USER_LOGIN, task.user_login)
        self.assertEqual(self.DESCRIPTION, task.description)

    def test_take_tasks_from_schedulers(self):
        self.sleep_until_next_day_if_midnight()

        scheduler = Scheduler(self.USER_LOGIN,
                              {'description': self.USER_LOGIN},
                              start_date=date.today(),
                              end_date=date.today() + relativedelta(days=1),
                              amount_differences=1,
                              tasks_upload=0
                              )
        first_creation_task_date = scheduler.get_next_date()
        tasks_upload = scheduler.tasks_upload

        tracker_tools.take_tasks_from_schedulers([scheduler])

        self.assertFalse(first_creation_task_date == scheduler.get_next_date())
        self.assertTrue(tasks_upload + 1 == scheduler.tasks_upload)

    def test_add_tasks_to_user(self):
        user = User(self.USER_LOGIN)
        task = Task(self.USER_LOGIN, self.DESCRIPTION)

        tracker_tools.add_tasks_to_user(user, [task])

        self.assertIn(task.t_id, user.tasks)

    def test_transform_show_dates(self):
        self.sleep_until_next_day_if_midnight()

        date_1 = date.today() + relativedelta(weeks=1)
        date_2 = date.today() + relativedelta(months=1)

        self.assertEqual((date_1, date_2), tracker_tools.transform_show_dates(date_1, date_2))

        self.assertEqual((date.today(), date_2), tracker_tools.transform_show_dates(None, date_2))

        self.assertEqual((date_1, date_1 + relativedelta(weeks=1)), tracker_tools.transform_show_dates(date_1, None))

        self.assertEqual((date.today(), date.today() + relativedelta(weeks=1)),
                         tracker_tools.transform_show_dates(None, None))

    def test_find_shared_task_index(self):
        fake_index = '5'

        tasks_id = [str(_) for _ in range(1, 5)]

        shared_task_1 = Shared(self.USER_LOGIN, Permission.READ, SharedType.SEND, tasks_id[0])
        shared_task_2 = Shared(self.USER_LOGIN, Permission.READ, SharedType.SEND, tasks_id[1])
        shared_task_3 = Shared(self.USER_LOGIN, Permission.READ, SharedType.SEND, tasks_id[2])
        shared_task_4 = Shared(self.USER_LOGIN, Permission.READ, SharedType.SEND, tasks_id[3])

        shared_tasks = [shared_task_1, shared_task_2, shared_task_3, shared_task_4]

        for shared_task in shared_tasks:
            self.assertEqual(shared_tasks.index(shared_task),
                             tracker_tools.find_shared_task_index(shared_tasks, shared_task.t_id))

        with self.assertRaises(IndexError):
            tracker_tools.find_shared_task_index(shared_tasks, fake_index)

    @staticmethod
    def sleep_until_next_day_if_midnight():
        if datetime.now() > datetime(date.today().year, date.today().month, date.today().day, 23, 59, 59):
            time.sleep(1)
