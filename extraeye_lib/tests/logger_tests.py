import os
import logging
import unittest

from extraeye_lib.logger import (
    write_log,
    setup_extraeye_logger,
    get_extraeye_logger
)


class LoggerTests(unittest.TestCase):
    TESTING_LOGGER_PATH = os.path.join(os.environ['HOME'], 'testing_logger')
    TESTING_LOGGER_LEVEL = logging.DEBUG
    TESTING_LOGGER_FORMAT = '%(message)s'
    TESTING_LOGGER_IS_ENABLED = True

    def setUp(self):
        setup_extraeye_logger(
            logger_level=self.TESTING_LOGGER_LEVEL,
            logger_formatter=self.TESTING_LOGGER_FORMAT,
            logger_path=self.TESTING_LOGGER_PATH,
            enabled=self.TESTING_LOGGER_IS_ENABLED
        )

    def tearDown(self):
        os.remove(self.TESTING_LOGGER_PATH)

    def test_setup_extraeye_logger(self):
        testing_logger = get_extraeye_logger()

        self.assertEqual(self.TESTING_LOGGER_LEVEL, testing_logger.level)
        self.assertEqual(not self.TESTING_LOGGER_IS_ENABLED, testing_logger.disabled)

    def test_write_log(self):
        return_value = 5

        self.assertEqual(return_value, write_log(lambda: return_value)())
        self.assertTrue(os.stat(self.TESTING_LOGGER_PATH).st_size)
