import os
import unittest

from extraeye_lib.storage.path import Path


class PathTests(unittest.TestCase):

    USER_LOGIN = 'MAX'
    ENTITY_ID = '1'
    TEMP_STORAGE = 'TESTS'

    def test_initializing(self):
        path = Path(self.TEMP_STORAGE)
        self.assertEqual(path.base_dir, self.TEMP_STORAGE)
        
        self.assertEqual(
            path.create_path_to_task_file(self.USER_LOGIN, self.ENTITY_ID),
            os.path.join(path.base_dir, self.USER_LOGIN, path.tasks_dir_name, self.ENTITY_ID)
        )

        self.assertEqual(
            path.create_path_to_reminder_file(self.USER_LOGIN, self.ENTITY_ID),
            os.path.join(path.base_dir, self.USER_LOGIN, path.reminders_dir_name, self.ENTITY_ID)
        )

        self.assertEqual(
            path.create_path_to_scheduler_file(self.USER_LOGIN, self.ENTITY_ID),
            os.path.join(path.base_dir, self.USER_LOGIN, path.schedulers_dir_name, self.ENTITY_ID)
        )

        self.assertEqual(
            path.create_path_to_user_file(self.USER_LOGIN),
            os.path.join(path.get_user_dir(self.USER_LOGIN), path.user_info_file_name)
        )

