import os
import time
import json
import shutil
import unittest
import extraeye_lib.management.tracker

from datetime import (
    date,
    datetime
)
from extraeye_lib.storage import storage
from extraeye_lib.entities.user import User
from dateutil.relativedelta import relativedelta
from extraeye_lib.entities.scheduler import Scheduler
from extraeye_lib.entities.reminder import (
    Reminder,
    ReminderOwner,
    EntitiesType
)
from extraeye_lib.storage.path import Path
from extraeye_lib.entities.task import Task


class StorageTests(unittest.TestCase):
    TEST_STORAGE_PATH = os.path.join(os.environ['HOME'], 'Extraeye_Test_Storage')

    USER_LOGIN = 'John'
    DESCRIPTION = 'Big and difficult'
    ENTITY_ID = '1'

    def setUp(self):
        self.path = Path(self.TEST_STORAGE_PATH)
        self.tracker = extraeye_lib.management.tracker.Tracker(self.path)
        
    def tearDown(self):
        shutil.rmtree(self.TEST_STORAGE_PATH)

    def test_write_tasks_to_json(self):
        task_path = self.path.create_path_to_task_file(self.USER_LOGIN, self.ENTITY_ID)
        task = Task(self.USER_LOGIN, self.DESCRIPTION, t_id=self.ENTITY_ID)
        storage.write_tasks_to_json(self.USER_LOGIN, [task], path=self.path)

        self.assertTrue(os.path.exists(task_path))

        with open(task_path, 'r', encoding='utf-8') as task_file:
            json_task = json.load(task_file)
            self.assertEqual(json_task['description'], self.DESCRIPTION)

    def test_write_reminders_to_json(self):
        reminder_path = self.path.create_path_to_reminder_file(self.USER_LOGIN, self.ENTITY_ID)
        reminder = Reminder(self.USER_LOGIN, self.DESCRIPTION, None, r_id=self.ENTITY_ID)
        storage.write_reminders_to_json(self.USER_LOGIN, [reminder], path=self.path)

        self.assertEqual(os.path.exists(reminder_path), True)

        with open(reminder_path, 'r', encoding='utf-8') as reminder_file:
            json_reminder = json.load(reminder_file)
            self.assertEqual(json_reminder['description'], self.DESCRIPTION)

    def test_write_schedulers_to_json(self):
        scheduler_path = self.path.create_path_to_scheduler_file(self.USER_LOGIN, self.ENTITY_ID)
        scheduler = Scheduler(self.USER_LOGIN, {'description': self.DESCRIPTION}, s_id=self.ENTITY_ID)
        storage.write_schedulers_to_json(self.USER_LOGIN, [scheduler], path=self.path)

        self.assertEqual(os.path.exists(scheduler_path), True)

        with open(scheduler_path, 'r', encoding='utf-8') as scheduler_file:
            json_scheduler = json.load(scheduler_file)
            self.assertEqual(json_scheduler['task_info']['description'], self.DESCRIPTION)

    def test_write_users_to_json(self):
        user = User(self.USER_LOGIN)
        storage.write_user_to_json(user, path=self.path)

        self.assertEqual(os.path.exists(self.path.get_user_dir(self.USER_LOGIN)), True)

        with open(self.path.create_path_to_user_file(self.USER_LOGIN), 'r',
                  encoding='utf-8') as user_file:
            json_user = json.load(user_file)
            self.assertEqual(json_user['login'], self.USER_LOGIN)

    def test_read_tasks_from_json(self):
        task = Task(self.USER_LOGIN, self.DESCRIPTION)
        storage.write_tasks_to_json(self.USER_LOGIN, [task], path=self.path)

        for task_from_storage in storage.read_tasks_from_json(self.USER_LOGIN, path=self.path):
            self.assertEqual(task.t_id, task_from_storage.t_id)
            self.assertEqual(task.description, task_from_storage.description)

    def test_read_reminders_from_json(self):
        reminder = Reminder(self.USER_LOGIN, self.DESCRIPTION, None)
        storage.write_reminders_to_json(self.USER_LOGIN, [reminder], path=self.path)

        for reminder_from_storage in storage.read_reminders_from_json(self.USER_LOGIN, path=self.path):
            self.assertEqual(reminder.r_id, reminder_from_storage.r_id)
            self.assertEqual(reminder.description, reminder_from_storage.description)

    def test_read_schedulers_from_json(self):
        scheduler = Scheduler(self.USER_LOGIN, {'description': self.USER_LOGIN})
        storage.write_schedulers_to_json(self.USER_LOGIN, [scheduler], path=self.path)

        for scheduler_from_storage in storage.read_schedulers_from_json(self.USER_LOGIN, path=self.path):
            self.assertEqual(scheduler.s_id, scheduler_from_storage.s_id)
            self.assertEqual(scheduler.task_info, scheduler_from_storage.task_info)

    def test_read_users_from_json(self):
        user = User(self.USER_LOGIN)
        storage.write_user_to_json(user, path=self.path)

        for user_from_storage in storage.read_users_from_json(path=self.path):
            self.assertEqual(user.login, user_from_storage.login)

    def test_get_all_tasks(self):
        task_1 = Task(self.USER_LOGIN, 'task_1')
        task_2 = Task(self.USER_LOGIN, 'task_2')
        task_3 = Task(self.USER_LOGIN, 'task_3')

        tasks = [task_1, task_2, task_3]
        storage.write_tasks_to_json(self.USER_LOGIN, tasks, path=self.path)

        for task_from_storage in storage.get_all_tasks(self.USER_LOGIN, path=self.path):
            self.assertTrue(any(task_from_storage.t_id == task.t_id for task in tasks))

    def test_get_tasks_by_group_name(self):
        group_name = 'sport'

        task_1 = Task(self.USER_LOGIN, 'task_1', groups=[group_name])
        task_2 = Task(self.USER_LOGIN, 'task_2')
        task_3 = Task(self.USER_LOGIN, 'task_3', groups=[group_name])

        storage.write_tasks_to_json(self.USER_LOGIN, [task_1, task_2, task_3], path=self.path)

        group_tasks_from_storage = storage.get_tasks_by_group_name(
                                       self.USER_LOGIN,
                                       group_name,
                                       path=self.path
                                   )
        self.assertTrue(any(task_1.t_id == group_task.t_id for group_task in group_tasks_from_storage))
        self.assertTrue(any(task_3.t_id == group_task.t_id for group_task in group_tasks_from_storage))

    def test_get_shared_tasks(self):
        sender = User(self.USER_LOGIN)
        receiver = User('Paul')
        task = Task(self.USER_LOGIN, self.DESCRIPTION)

        storage.write_user_to_json(sender, self.path)
        storage.write_user_to_json(receiver, self.path)
        storage.write_tasks_to_json(self.USER_LOGIN, [task], path=self.path)

        self.tracker.share_task(sender.login, receiver.login, task.t_id)
        for shared_task_from_json in storage.get_shared_tasks(receiver.login, path=self.path):
            self.assertEqual(task.t_id, shared_task_from_json.t_id)

    def test_get_task_by_id(self):
        task_1 = Task(self.USER_LOGIN, 'task_1')
        task_2 = Task(self.USER_LOGIN, 'task_2')
        task_3 = Task(self.USER_LOGIN, 'task_3')

        tasks = [task_1, task_2, task_3]
        storage.write_tasks_to_json(self.USER_LOGIN, tasks, path=self.path)

        for task in tasks:
            self.assertEqual(
                task.description,
                storage.get_task_by_id(self.USER_LOGIN, task.t_id, path=self.path).description
            )

    def test_get_tasks_from_period(self):
        date_1 = datetime(2020, 1, 1).date()
        date_2 = datetime(2020, 6, 25).date()
        date_3 = datetime(2025, 11, 19).date()

        task_1 = Task(self.USER_LOGIN, 'task_1', deadline=datetime(2020, 1, 1))
        task_2 = Task(self.USER_LOGIN, 'task_2', deadline=datetime(2020, 6, 25))
        task_3 = Task(self.USER_LOGIN, 'task_3', deadline=datetime(2025, 11, 19))

        storage.write_tasks_to_json(self.USER_LOGIN, [task_1, task_2, task_3], path=self.path)

        self.assertFalse(any(task_3.t_id == date_task.t_id for date_task in
                             storage.get_tasks_from_period(self.USER_LOGIN, date_1, date_2, path=self.path)))
        self.assertFalse(any(task_2.t_id == date_task.t_id for date_task in
                             storage.get_tasks_from_period(self.USER_LOGIN, date_1, date_1, path=self.path)))
        self.assertTrue(any(task_1.t_id == date_task.t_id for date_task in
                            storage.get_tasks_from_period(self.USER_LOGIN, date_1, date_1, path=self.path)))
        self.assertTrue(any(task_2.t_id == date_task.t_id for date_task in
                            storage.get_tasks_from_period(self.USER_LOGIN, date_1, date_2, path=self.path)))
        self.assertTrue(any(task_3.t_id == date_task.t_id for date_task in
                            storage.get_tasks_from_period(self.USER_LOGIN, date_1, date_3, path=self.path)))

    def test_get_subtasks(self):
        task_1 = Task(self.USER_LOGIN, 'task_1', parent='3', t_id='1')
        task_2 = Task(self.USER_LOGIN, 'task_2', parent='3', t_id='2')
        task_3 = Task(self.USER_LOGIN, 'task_3', t_id='3', subtasks=['1', '2'])

        storage.write_tasks_to_json(self.USER_LOGIN, [task_1, task_2, task_3], path=self.path)

        self.assertTrue(task_1.t_id == subtask_from_storage.t_id for subtask_from_storage
                        in storage.get_subtasks(self.USER_LOGIN, task_3, path=self.path))
        self.assertTrue(task_2.t_id == subtask_from_storage.t_id for subtask_from_storage
                        in storage.get_subtasks(self.USER_LOGIN, task_3, path=self.path))

    def test_get_all_subtasks(self):
        task_1 = Task(self.USER_LOGIN, 'task_1', t_id='1', subtasks=['2'])
        task_2 = Task(self.USER_LOGIN, 'task_2', t_id='2', parent='1', subtasks=['3'])
        task_3 = Task(self.USER_LOGIN, 'task_3', t_id='3', parent='2')

        storage.write_tasks_to_json(self.USER_LOGIN, [task_1, task_2, task_3], path=self.path)

        self.assertEqual(3, len(storage.get_all_subtasks(self.USER_LOGIN, task_1, path=self.path)))

    def test_get_task_reminders(self):
        task = Task(self.USER_LOGIN, self.DESCRIPTION, t_id=self.ENTITY_ID, reminders=['2', '3'])

        reminder_1 = Reminder(
                         self.USER_LOGIN,
                         'reminder_1',
                         None,
                         r_id='2',
                         owner=ReminderOwner(EntitiesType.TASK, task.t_id, False)
                     )

        reminder_2 = Reminder(
                         self.USER_LOGIN,
                         'reminder_2',
                         None,
                         r_id='3',
                         owner=ReminderOwner(EntitiesType.TASK, task.t_id, False)
                     )

        storage.write_reminders_to_json(self.USER_LOGIN, [reminder_1, reminder_2], path=self.path)
        storage.write_tasks_to_json(self.USER_LOGIN, [task], path=self.path)

        task_reminders_from_json = storage.get_task_reminders(self.USER_LOGIN, task, path=self.path)

        self.assertTrue(any(reminder_1.r_id == task_reminder.r_id for task_reminder in task_reminders_from_json))
        self.assertTrue(any(reminder_2.r_id == task_reminder.r_id for task_reminder in task_reminders_from_json))

    def test_get_reminders_by_group_name(self):
        group_name = 'sport'

        reminder_1 = Reminder(self.USER_LOGIN, 'task_1', None, groups=[group_name])
        reminder_2 = Reminder(self.USER_LOGIN, 'task_2', None)
        reminder_3 = Reminder(self.USER_LOGIN, 'task_3', None, groups=[group_name])

        storage.write_reminders_to_json(
            self.USER_LOGIN,
            [reminder_1, reminder_2, reminder_3],
            path=self.path
        )

        group_reminders_from_storage = storage.get_reminders_by_group_name(
                                           self.USER_LOGIN,
                                           group_name,
                                           path=self.path
                                       )
        self.assertTrue(any(reminder_1.r_id == group_reminder.r_id for group_reminder in group_reminders_from_storage))
        self.assertTrue(any(reminder_3.r_id == group_reminder.r_id for group_reminder in group_reminders_from_storage))

    def test_get_all_reminders(self):
        reminder_1 = Reminder(self.USER_LOGIN, 'task_1', None)
        reminder_2 = Reminder(self.USER_LOGIN, 'task_2', None)
        reminder_3 = Reminder(self.USER_LOGIN, 'task_3', None)

        reminders = [reminder_1, reminder_2, reminder_3]
        storage.write_reminders_to_json(self.USER_LOGIN, reminders, path=self.path)

        for reminder_from_storage in storage.get_all_reminders(self.USER_LOGIN, path=self.path):
            self.assertTrue(any(reminder_from_storage.r_id == reminder.r_id for reminder in reminders))

    def test_get_reminders_from_period(self):
        date_1 = datetime(2020, 1, 1).date()
        date_2 = datetime(2020, 6, 25).date()
        date_3 = datetime(2025, 11, 19).date()

        reminder_1 = Reminder(self.USER_LOGIN, 'reminder_1', date_1)
        reminder_2 = Reminder(self.USER_LOGIN, 'reminder_2', date_2)
        reminder_3 = Reminder(self.USER_LOGIN, 'reminder_3', date_3)

        storage.write_reminders_to_json(
            self.USER_LOGIN,
            [reminder_1, reminder_2, reminder_3],
            path=self.path
        )

        self.assertFalse(any(reminder_3.r_id == storage_reminder.r_id for storage_reminder in
                             storage.get_reminders_from_period(
                                 self.USER_LOGIN,
                                 date_1,
                                 date_2,
                                 path=self.path
                             )))
        self.assertFalse(any(reminder_2.r_id == storage_reminder.r_id for storage_reminder in
                             storage.get_reminders_from_period(
                                 self.USER_LOGIN,
                                 date_1,
                                 date_1,
                                 path=self.path
                             )))
        self.assertTrue(any(reminder_1.r_id == storage_reminder.r_id for storage_reminder in
                            storage.get_reminders_from_period(
                                self.USER_LOGIN,
                                date_1,
                                date_1,
                                path=self.path
                            )))
        self.assertTrue(any(reminder_2.r_id == storage_reminder.r_id for storage_reminder in
                            storage.get_reminders_from_period(
                                self.USER_LOGIN,
                                date_1,
                                date_2,
                                path=self.path
                            )))
        self.assertTrue(any(reminder_3.r_id == storage_reminder.r_id for storage_reminder in
                            storage.get_reminders_from_period(
                                self.USER_LOGIN,
                                date_1,
                                date_3,
                                path=self.path
                            )))

    def test_get_reminder_by_id(self):
        reminder_1 = Reminder(self.USER_LOGIN, 'reminder_1', None)
        reminder_2 = Reminder(self.USER_LOGIN, 'reminder_2', None)
        reminder_3 = Reminder(self.USER_LOGIN, 'reminder_3', None)

        reminders = [reminder_1, reminder_2, reminder_3]
        storage.write_reminders_to_json(self.USER_LOGIN, reminders, path=self.path)

        for reminder in reminders:
            self.assertEqual(
                reminder.description,
                storage.get_reminder_by_id(self.USER_LOGIN, reminder.r_id, path=self.path).description)

    def test_get_all_schedulers(self):
        scheduler_1 = Scheduler(self.USER_LOGIN, {})
        scheduler_2 = Scheduler(self.USER_LOGIN, {})
        scheduler_3 = Scheduler(self.USER_LOGIN, {})

        schedulers = [scheduler_1, scheduler_2, scheduler_3]
        storage.write_schedulers_to_json(self.USER_LOGIN, schedulers, path=self.path)

        for scheduler_from_storage in storage.get_all_schedulers(self.USER_LOGIN, path=self.path):
            self.assertTrue(any(scheduler_from_storage.s_id == scheduler.s_id for scheduler in schedulers))

    def test_get_uncreated_schedulers(self):
        scheduler_1 = Scheduler(
            self.USER_LOGIN, {},
            start_date=date.today() - relativedelta(weeks=1),
            end_date=date.today(),
            tasks_upload=1
        )
        scheduler_2 = Scheduler(
            self.USER_LOGIN, {},
            start_date=date.today() + relativedelta(weeks=1),
            end_date=date.today() + relativedelta(months=1),
            tasks_upload=0
        )

        storage.write_schedulers_to_json(self.USER_LOGIN, [scheduler_1, scheduler_2], path=self.path)

        self.assertEqual(1, len(storage.get_uncreated_schedulers(self.USER_LOGIN, path=self.path)))
        self.assertEqual(
            scheduler_1.s_id,
            storage.get_uncreated_schedulers(self.USER_LOGIN, path=self.path)[0].s_id)

    def test_get_all_group_entities(self):
        group_1_name = 'sport'
        group_2_name = 'study'

        task_1 = Task(self.USER_LOGIN, 'task_1', parent='3', t_id='1', groups=[group_1_name])
        task_2 = Task(self.USER_LOGIN, 'task_2', parent='3', t_id='2', groups=[group_2_name])

        reminder_1 = Reminder(self.USER_LOGIN, 'reminder_1', None, groups=[group_1_name])
        reminder_2 = Reminder(self.USER_LOGIN, 'reminder_2', None, groups=[group_2_name])

        storage.write_tasks_to_json(self.USER_LOGIN, [task_1, task_2], path=self.path)
        storage.write_reminders_to_json(self.USER_LOGIN, [reminder_1, reminder_2], path=self.path)

        group_tasks_from_storage, group_reminders_from_storage = storage.get_all_group_entities(
                                                                     self.USER_LOGIN,
                                                                     group_1_name,
                                                                     path=self.path
                                                                 )
        self.assertEqual(1, len(group_tasks_from_storage))
        self.assertEqual(task_1.t_id, group_tasks_from_storage[0].t_id)
        self.assertEqual(1, len(group_reminders_from_storage))
        self.assertEqual(reminder_1.r_id, group_reminders_from_storage[0].r_id)

        group_tasks_from_storage, group_reminders_from_storage = storage.get_all_group_entities(
                                                                     self.USER_LOGIN,
                                                                     group_2_name,
                                                                     path=self.path
                                                                 )

        self.assertEqual(1, len(group_tasks_from_storage))
        self.assertEqual(task_2.t_id, group_tasks_from_storage[0].t_id)
        self.assertEqual(1, len(group_reminders_from_storage))
        self.assertEqual(reminder_2.r_id, group_reminders_from_storage[0].r_id)

    def test_get_all_groups_entities(self):
        group_1_name = 'sport'
        group_2_name = 'study'

        task_1 = Task(self.USER_LOGIN, 'task_1', parent='3', t_id='1', groups=[group_1_name])
        task_2 = Task(self.USER_LOGIN, 'task_2', parent='3', t_id='2', groups=[group_2_name])

        reminder_1 = Reminder(self.USER_LOGIN, 'reminder_1', None, groups=[group_1_name])
        reminder_2 = Reminder(self.USER_LOGIN, 'reminder_2', None, groups=[group_2_name])

        storage.write_tasks_to_json(self.USER_LOGIN, [task_1, task_2], path=self.path)
        storage.write_reminders_to_json(self.USER_LOGIN, [reminder_1, reminder_2], path=self.path)

        groups_tasks_from_storage, groups_reminders_from_storage = storage.get_all_groups_entities(
                                                                       self.USER_LOGIN,
                                                                       path=self.path
                                                                   )

        self.assertEqual(2, len(groups_tasks_from_storage))
        self.assertTrue(any(task_1.t_id == group_task.t_id for group_task in groups_tasks_from_storage))
        self.assertTrue(any(task_2.t_id == group_task.t_id for group_task in groups_tasks_from_storage))
        self.assertEqual(2, len(groups_reminders_from_storage))
        self.assertTrue(any(reminder_1.r_id == group_reminder.r_id for group_reminder in groups_reminders_from_storage))
        self.assertTrue(any(reminder_2.r_id == group_reminder.r_id for group_reminder in groups_reminders_from_storage))

    def test_get_user_by_login(self):
        user_1 = User(self.USER_LOGIN, tasks=['1'])

        storage.write_user_to_json(user_1, self.path)

        user_1_from_storage = storage.get_user_by_login(user_1.login, path=self.path)

        self.assertEqual(user_1.login, user_1_from_storage.login)
        self.assertEqual(user_1.tasks, user_1_from_storage.tasks)

    def test_create_storage(self):
        storage.create_storage(path=self.path)
        self.assertTrue(os.path.exists(self.TEST_STORAGE_PATH))

        one_more_temp_base_dir = os.path.join(os.environ['HOME'], 'one_more_dir')
        another_path = Path(one_more_temp_base_dir)
        storage.create_storage(path=another_path)
        self.assertTrue(os.path.exists(one_more_temp_base_dir))

    def test_delete_tasks_by_id(self):
        task_1 = Task(self.USER_LOGIN, 'task_1')
        task_2 = Task(self.USER_LOGIN, 'task_2')
        storage.write_tasks_to_json(self.USER_LOGIN, [task_1, task_2], path=self.path)

        storage.delete_tasks_by_id(self.USER_LOGIN, [task_1.t_id], path=self.path)
        storage.delete_tasks_by_id(self.USER_LOGIN, [task_2.t_id], path=self.path)

        self.assertEqual(0, len(storage.get_all_tasks(self.USER_LOGIN, path=self.path)))

    def test_delete_reminders_by_id(self):
        reminder_1 = Reminder(self.USER_LOGIN, 'reminder_1', None)
        reminder_2 = Reminder(self.USER_LOGIN, 'reminder_2', None)
        storage.write_reminders_to_json(self.USER_LOGIN, [reminder_1, reminder_2], path=self.path)

        storage.delete_reminders_by_id(self.USER_LOGIN, [reminder_1.r_id], path=self.path)
        storage.delete_reminders_by_id(self.USER_LOGIN, [reminder_2.r_id], path=self.path)

        self.assertEqual(0, len(storage.get_all_reminders(self.USER_LOGIN, path=self.path)))

    def test_delete_schedulers_by_id(self):
        scheduler_1 = Scheduler(self.USER_LOGIN, {'description': 'scheduler_1'})
        scheduler_2 = Scheduler(self.USER_LOGIN, {'description': 'scheduler_2'})
        storage.write_schedulers_to_json(self.USER_LOGIN, [scheduler_1, scheduler_2], path=self.path)

        storage.delete_schedulers_by_id(self.USER_LOGIN, [scheduler_1.s_id], path=self.path)
        storage.delete_schedulers_by_id(self.USER_LOGIN, [scheduler_2.s_id], path=self.path)

        self.assertEqual(0, len(storage.get_all_schedulers(self.USER_LOGIN, path=self.path)))

    def test_get_missed_reminders(self):

        if datetime.now() > datetime(date.today().year, date.today().month, date.today().day, 23, 59, 59):
            time.sleep(1)

        reminder_1 = Reminder(
            self.USER_LOGIN,
            'reminder_1',
            notify_date=date.today() - relativedelta(days=1)
        )
        reminder_2 = Reminder(
            self.USER_LOGIN,
            'reminder_2',
            notify_date=date.today() - relativedelta(months=1)
        )
        reminder_3 = Reminder(
            self.USER_LOGIN,
            'reminder_3',
            notify_date=date.today() + relativedelta(days=1)
        )

        storage.write_reminders_to_json(
            self.USER_LOGIN,
            [reminder_1, reminder_2, reminder_3],
            path=self.path
        )

        missed_reminder_from_storage = storage.get_missed_reminders(self.USER_LOGIN, path=self.path)
        self.assertEqual(2, len(missed_reminder_from_storage))
        self.assertFalse(any(reminder_3.r_id == missed_reminder.r_id for missed_reminder
                             in missed_reminder_from_storage))

    def test_get_overdue_tasks(self):
        if datetime.now() > datetime(date.today().year, date.today().month, date.today().day, 23, 59, 59):
            time.sleep(1)

        user = User(self.USER_LOGIN)

        task_1 = Task(self.USER_LOGIN, 'task_1', deadline=datetime(year=date.today().year, month=date.today().month,
                                                                   day=date.today().day) - relativedelta(days=1))
        task_2 = Task(self.USER_LOGIN, 'task_2', deadline=datetime(year=date.today().year, month=date.today().month,
                                                                   day=date.today().day) + relativedelta(days=1))
        task_3 = Task(self.USER_LOGIN, 'task_3', deadline=datetime(year=date.today().year, month=date.today().month,
                                                                   day=date.today().day) - relativedelta(years=2))

        user.tasks.extend([task_1, task_2, task_3])
        storage.write_user_to_json(user, path=self.path)
        storage.write_tasks_to_json(self.USER_LOGIN, [task_1, task_2, task_3], path=self.path)

        storage_overdue_tasks = storage.get_overdue_tasks(self.USER_LOGIN, path=self.path)
        self.assertEqual(2, len(storage_overdue_tasks))
