import os
import json
import time
import shutil
import unittest

from extraeye_lib.entities.user import User
from dateutil.relativedelta import relativedelta
from extraeye_lib.entities.scheduler import Scheduler
from extraeye_lib.entities.reminder import (
    Reminder,
    EntitiesType,
    ReminderOwner
)
from extraeye_lib.entities.task import (
    Task,
    Status,
    Shared,
    Permission,
    SharedType
)
from extraeye_lib.storage import (
    storage,
    storage_tools
)
from extraeye_lib.storage.path import Path
from datetime import (
    date,
    datetime
)


class StorageToolsTests(unittest.TestCase):
    TEST_STORAGE_PATH = os.path.join(os.environ['HOME'], 'Extraeye_Test_Storage')

    USER_LOGIN = 'John'
    DESCRIPTION = 'Big and difficult'
    ENTITY_ID = '1'

    def setUp(self):
        self.path = Path(self.TEST_STORAGE_PATH)

        os.makedirs(self.path.base_dir)
        os.makedirs(self.path.get_user_dir(self.USER_LOGIN))
        os.makedirs(self.path.get_tasks_dir(self.USER_LOGIN))
        os.makedirs(self.path.get_reminders_dir(self.USER_LOGIN))
        os.makedirs(self.path.get_schedulers_dir(self.USER_LOGIN))

    def tearDown(self):
        shutil.rmtree(self.path.base_dir)

    def test_convert_fields_to_json(self):
        self.assertEqual([], storage_tools.convert_fields_to_json(set()))

        self.assertEqual(
            datetime(year=2018, month=6, day=10).isoformat(sep=' '),
            storage_tools.convert_fields_to_json(datetime(year=2018, month=6, day=10))
        )

        self.assertEqual(date.today().isoformat(), storage_tools.convert_fields_to_json(date.today()))

        self.assertEqual({'years': 0, 'months': 0, 'days': 11},
                         storage_tools.convert_fields_to_json(relativedelta(days=11)))

        self.assertEqual(Status.FAILED.value, storage_tools.convert_fields_to_json(Status.FAILED))

        self.assertEqual(Status.__dict__, storage_tools.convert_fields_to_json(Status))

    def test_convert_json_to_task(self):
        task = Task(self.USER_LOGIN, self.DESCRIPTION)
        storage.write_tasks_to_json(self.USER_LOGIN, [task], path=self.path)

        task_path = self.path.create_path_to_task_file(self.USER_LOGIN, task.t_id)
        with open(task_path, 'r', encoding='utf-8') as json_task_file:
            json_task = storage_tools.convert_json_to_task(json.load(json_task_file))

        self.assertEqual(task.description, json_task.description)

    def test_convert_json_to_reminder(self):
        reminder = Reminder(self.USER_LOGIN, self.DESCRIPTION, None)
        storage.write_reminders_to_json(self.USER_LOGIN, [reminder], path=self.path)

        reminder_path = self.path.create_path_to_reminder_file(self.USER_LOGIN, reminder.r_id)
        with open(reminder_path, 'r', encoding='utf-8') as json_reminder_file:
            json_reminder = storage_tools.convert_json_to_reminder(json.load(json_reminder_file))

        self.assertEqual(reminder.description, json_reminder.description)

    def test_convert_json_to_scheduler(self):
        scheduler = Scheduler(self.USER_LOGIN, self.DESCRIPTION)
        storage.write_schedulers_to_json(self.USER_LOGIN, [scheduler], path=self.path)

        scheduler_path = self.path.create_path_to_scheduler_file(self.USER_LOGIN, scheduler.s_id)
        with open(scheduler_path, 'r', encoding='utf-8') as json_scheduler_file:
            json_scheduler = storage_tools.convert_json_to_scheduler(json.load(json_scheduler_file))

        self.assertEqual(scheduler.task_info, json_scheduler.task_info)

    def test_convert_json_to_user(self):
        user = User(self.USER_LOGIN)
        storage.write_user_to_json(user, path=self.path)

        user_path = self.path.create_path_to_user_file(self.USER_LOGIN)
        with open(user_path, 'r', encoding='utf-8') as json_user_file:
            json_user = storage_tools.convert_json_to_user(json.load(json_user_file))

        self.assertEqual(user.login, json_user.login)

    def test_get_from_json_date_delta_amount_difference(self):
        amount_differences = 50

        scheduler = Scheduler(self.USER_LOGIN, self.DESCRIPTION, amount_differences=amount_differences)
        storage.write_schedulers_to_json(self.USER_LOGIN, [scheduler], path=self.path)

        scheduler_path = self.path.create_path_to_scheduler_file(self.USER_LOGIN, scheduler.s_id)
        with open(scheduler_path, 'r', encoding='utf-8') as json_scheduler_file:
            json_scheduler_dict = json.load(json_scheduler_file)

        json_date_delta = json_scheduler_dict['date_delta']
        self.assertEqual(amount_differences, storage_tools.get_from_json_date_delta_amount_difference(json_date_delta))

    def test_convert_json_to_datetime(self):
        testing_datetime = datetime(year=2018, month=6, day=10)

        task = Task(self.USER_LOGIN, self.DESCRIPTION, deadline=testing_datetime)
        storage.write_tasks_to_json(self.USER_LOGIN, [task], path=self.path)

        task_path = self.path.create_path_to_task_file(self.USER_LOGIN, task.t_id)
        with open(task_path, 'r', encoding='utf-8') as json_task_file:
            json_task_dict = json.load(json_task_file)

        json_datetime = json_task_dict['deadline']
        self.assertEqual(testing_datetime, storage_tools.convert_json_to_datetime(json_datetime))

    def test_convert_json_to_date(self):
        testing_date = date.today()

        reminder = Reminder(self.USER_LOGIN, self.DESCRIPTION, notify_date=testing_date)
        storage.write_reminders_to_json(self.USER_LOGIN, [reminder], path=self.path)

        reminder_path = self.path.create_path_to_reminder_file(self.USER_LOGIN, reminder.r_id)
        with open(reminder_path, 'r', encoding='utf-8') as json_reminder_file:
            json_reminder_dict = json.load(json_reminder_file)

        json_date = json_reminder_dict['notify_date']
        self.assertEqual(testing_date, storage_tools.convert_json_to_date(json_date))

    def test_convert_json_to_reminder_owner(self):
        testing_owner = ReminderOwner(EntitiesType.TASK, self.ENTITY_ID, True)

        reminder = Reminder(self.USER_LOGIN, self.DESCRIPTION, None, owner=testing_owner)
        storage.write_reminders_to_json(self.USER_LOGIN, [reminder], path=self.path)

        reminder_path = self.path.create_path_to_reminder_file(self.USER_LOGIN, reminder.r_id)

        with open(reminder_path, 'r', encoding='utf-8') as json_reminder_file:
            json_reminder_dict = json.load(json_reminder_file)

        json_owner = json_reminder_dict['owner']
        self.assertEqual(testing_owner, storage_tools.convert_json_to_reminder_owner(json_owner))

    def test_convert_json_to_shared_entities(self):
        testing_shared_entity = Shared(self.USER_LOGIN, Permission.READ, SharedType.SEND, self.ENTITY_ID)

        user = User(self.USER_LOGIN, shared_tasks=[testing_shared_entity])
        storage.write_user_to_json(user, self.path)

        user_path = self.path.create_path_to_user_file(self.USER_LOGIN)
        with open(user_path, 'r', encoding='utf-8') as json_user_file:
            json_user_dict = json.load(json_user_file)

        json_shared_entity = json_user_dict['shared_tasks']
        self.assertEqual(
            testing_shared_entity,
            storage_tools.convert_json_to_shared_entities(json_shared_entity)[0]
        )

    def test_get_now_datetime(self):
        if datetime.now() > datetime(date.today().year, date.today().month, date.today().day, 23, 59, 59):
            time.sleep(1)

        storage_tools_now_datetime = storage_tools.get_now_datetime()
        current_date = date.today()

        self.assertEqual(current_date, storage_tools_now_datetime.date())
        self.assertEqual(datetime.now().hour, storage_tools_now_datetime.hour)
        self.assertEqual(datetime.now().minute, storage_tools_now_datetime.minute)

    def test_archive_condition(self):
        entering_parameters = [
            [True, True, False],
            [False, True, False],
            [False, False, True],
            [False, False, False]
        ]
        correct_answers = [True, False, True, True]

        for parameters, correct_answer in zip(entering_parameters, correct_answers):
            self.assertEqual(
                correct_answer,
                storage_tools.archive_condition(parameters[0], parameters[1], parameters[2])
            )
