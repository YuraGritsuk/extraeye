import unittest


def run_tests():
    test_modules = [
        'extraeye_lib.tests.entities.reminder_tests',
        'extraeye_lib.tests.entities.scheduler_tests',
        'extraeye_lib.tests.entities.task_tests',
        'extraeye_lib.tests.entities.time_span_tests',
        'extraeye_lib.tests.entities.user_tests',
        'extraeye_lib.tests.storage.path_tests',
        'extraeye_lib.tests.storage.storage_tests',
        'extraeye_lib.tests.storage.storage_tools_tests',
        'extraeye_lib.tests.logger_tests',
        'extraeye_lib.tests.management.tracker_tests',
        'extraeye_lib.tests.management.tracker_tools_tests'
    ]

    suite = unittest.TestSuite()

    for test_module in test_modules:
        suite.addTests(unittest.defaultTestLoader.loadTestsFromName(test_module))

    unittest.TextTestRunner().run(suite)


if __name__ == 'extraeye_lib.tests.run_tests':
    run_tests()
