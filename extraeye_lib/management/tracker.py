"""Class that represents uniting console with storage functions."""

from queue import Queue
from collections import namedtuple
from extraeye_lib.management import tracker_tools
from extraeye_lib.storage import storage
from extraeye_lib.logger import write_log
from extraeye_lib.entities.user import User
from extraeye_lib.entities.scheduler import Scheduler
from extraeye_lib.entities.reminder import (
    Reminder,
    EntitiesType
)
from extraeye_lib.exceptions.name_exceptions import (
    GroupNameError,
    LoginNameError
)
from extraeye_lib.entities.task import (
    Task,
    Shared,
    Status,
    Permission,
    SharedType
)


class Tracker:
    """Class represent functions for perform user request that are coming from the console.

    Carry out all user request with storage functions help.
    """

    def __init__(self, customized_path=None):
        """Init Tracker object.

        Args:
            customized_path (:obj: `str`, optional): Path to the storage with user data.
                Defaults to None.
        """
        self.path = customized_path

    @write_log
    def add_task(self, user_login, description, priority=None, deadline=None, groups=None):
        """Add task.

        Create task, add it to user tasks and save changes.

        Args:
            user_login (str):
            description (str):
            priority (:obj: `Priority`, optional): Defaults to None.
            deadline (:obj: `datetime`, optional): Defaults to None.
            groups (:obj: `str`, optional): Default to None.
        """
        task = Task(user_login, description, priority=priority, deadline=deadline, groups=groups)
        storage.write_tasks_to_json(user_login, [task], update_edition_time=False, path=self.path)

        user = storage.get_user_by_login(user_login, path=self.path)
        user.tasks.append(task.t_id)
        storage.write_user_to_json(user, path=self.path)

    @write_log
    def add_subtask(self, user_login, parent_id, description, priority=None, deadline=None, groups=None):
        """Add subtask.

        Create task, add it to user tasks, create task to subtask relationships and save changes.

        Args:
            user_login (str):
            parent_id (str): parent task's t_id.
            description (str): subtask's description
            priority (:obj: `Priority`, optional): Defaults to None.
            deadline (:obj: `datetime`, optional): Defaults to None.
            groups (:obj: `str`, optional): Default to None.
        """
        parent_task = storage.get_task_by_id(user_login, parent_id, path=self.path)
        subtask = Task(user_login, description, priority=priority, deadline=deadline, groups=groups)
        tracker_tools.create_subtask_relationships(parent_task, subtask)
        storage.write_tasks_to_json(user_login, [subtask, parent_task], path=self.path)

        user = storage.get_user_by_login(user_login, path=self.path)
        user.tasks.append(subtask.t_id)
        storage.write_user_to_json(user, path=self.path)

    @write_log
    def delete_task(self, user_login, task_id):
        """Delete task.

        Get task with help of function get_task_by_id, notifies the parent of deleted task, if it exists then,
        delete task with all it subtasks and reminders.

        Args:
            user_login (str):
            task_id (str):
            
        """
        task = storage.get_task_by_id(user_login, task_id, is_all=True, path=self.path)
        if task.parent:
            parent_task = storage.get_task_by_id(
                task.user_login,
                task.parent,
                only_archive=task.is_archive(),
                path=self.path
            )
            parent_task.subtasks.remove(task_id)
            storage.write_tasks_to_json(parent_task.user_login, [parent_task], path=self.path)

        self._delete_task_with_subtasks(task)

    @write_log
    def _delete_task_with_subtasks(self, task):
        queue = Queue()
        queue.put(task)

        while not queue.empty():
            temp_task = queue.get()
            for subtask in storage.get_subtasks(
                               temp_task.user_login,
                               temp_task,
                               is_all=True,
                               path=self.path
                           ):
                queue.put(subtask)

            for reminder_id in temp_task.reminders:
                self.delete_reminder(temp_task.user_login, reminder_id)

            self._delete_task_from_users(temp_task)
            storage.delete_tasks_by_id(temp_task.user_login, [temp_task.t_id], path=self.path)

    @write_log
    def _delete_task_from_users(self, task):
        user = storage.get_user_by_login(task.user_login, path=self.path)

        if task.other:
            for other_user_login in task.other:
                shared_user = storage.get_user_by_login(other_user_login, path=self.path)
                shared_user_task_pos = tracker_tools.find_shared_task_index(shared_user.shared_tasks, task.t_id)
                del shared_user.shared_tasks[shared_user_task_pos]
                storage.write_user_to_json(shared_user, path=self.path)

                current_user_task_pos = tracker_tools.find_shared_task_index(user.shared_tasks, task.t_id)
                del user.shared_tasks[current_user_task_pos]

        user.tasks.remove(task.t_id)
        storage.write_user_to_json(user, path=self.path)

    @write_log
    def delete_group_from_task(self, user_login, task_id, group_name):
        """Delete task from group by removing group name fom task groups.

        Args:
            user_login (str):
            task_id (str):
            group_name (str):

        Raises:
            GroupNameError: If group name is not in task groups.
        """
        task = storage.get_task_by_id(user_login, task_id, path=self.path)

        if group_name not in task.groups:
            raise GroupNameError()

        task.groups.remove(group_name)
        storage.write_tasks_to_json(user_login, [task], path=self.path)

    @write_log
    def delete_reminder(self, user_login, reminder_id):
        """Delete reminder.

        Get reminder with help of function get_reminder_by_id, notifies the owner task, if it exists,
        then delete reminder.

        Args:
            user_login (str):
            reminder_id (str):
        """
        reminder = storage.get_reminder_by_id(user_login, reminder_id, is_all=True, path=self.path)

        if reminder.owner:
            if reminder.owner.type == EntitiesType.TASK:
                reminder_task = storage.get_task_by_id(
                                    user_login,
                                    reminder.owner.r_id,
                                    only_archive=reminder.owner.is_archive,
                                    path=self.path
                                )
                reminder_task.reminders.remove(reminder.r_id)
                storage.write_tasks_to_json(reminder_task.user_login, [reminder_task], path=self.path)

        self._delete_reminder_from_user(reminder)
        storage.delete_reminders_by_id(reminder.user_login, [reminder_id], path=self.path)

    @write_log
    def delete_group_from_reminder(self, user_login, reminder_id, group_name):
        """Delete reminder from group by removing group name fom reminder groups.

        Args:
            user_login (str):
            reminder_id (str):
            group_name (str):
            
        """
        reminder = storage.get_reminder_by_id(user_login, reminder_id, path=self.path)

        if group_name not in reminder.groups:
            raise GroupNameError(not_in_entity=True)

        reminder.groups.remove(group_name)
        storage.write_reminders_to_json(user_login, [reminder], path=self.path)

    @write_log
    def _delete_reminder_from_user(self, reminder):
        reminder = storage.get_reminder_by_id(
                       reminder.user_login,
                       reminder.r_id,
                       only_archive=reminder.is_archive(),
                       path=self.path
                   )
        user = storage.get_user_by_login(reminder.user_login, path=self.path)
        user.reminders.remove(reminder.r_id)
        storage.write_user_to_json(user, path=self.path)

    @write_log
    def done_task(self, user_login, task_id):
        """Done task.

        Get task with using get_task_by_id, recursively put it status and all related tasks
        into Status.DONE, notify tasks reminders and save changes.

        Args:
            user_login (str):
            task_id (str):
            
        """
        task = storage.get_task_by_id(
                   user_login,
                   task_id,
                   only_archive=False,
                   is_all=False,
                   path=self.path
               )
        self._recursive_done_task(task)

    @write_log
    def _recursive_done_task(self, task):
        for subtask in storage.get_subtasks(task.user_login, task, is_all=True, path=self.path):
            if subtask.status == Status.ACTIVE:
                self._recursive_done_task(subtask)
        self._refresh_task_status(task, Status.DONE)

        if task.parent:
            parent_task = storage.get_task_by_id(
                              task.user_login,
                              task.parent,
                              is_all=True,
                              path=self.path
                          )
        else:
            parent_task = None
        while parent_task and all(subtask.status == Status.DONE for subtask in
                                  storage.get_subtasks(
                                      parent_task.user_login,
                                      parent_task,
                                      is_all=True,
                                      path=self.path
                                  )):
            self._refresh_task_status(parent_task, Status.DONE)
            if parent_task.parent:
                parent_task = storage.get_task_by_id(parent_task.user_login, parent_task.parent, path=self.path)
            else:
                parent_task = None

    @write_log
    def fail_task(self, user_login, task_id):
        """Fail task.

        Get task with using get_task_by_id, recursively put it status and all related tasks into
        Status.FAILED, notify tasks reminders and save changes.

        Args:
            user_login (str):
            task_id (str):
            
        """
        task = storage.get_task_by_id(user_login, task_id, path=self.path)
        self._recursive_fail_task(task)

    @write_log
    def _recursive_fail_task(self, task):
        if task.status != Status.FAILED:
            queue = Queue()
            queue.put(task)

            while not queue.empty():
                temp_task = queue.get()
                self._refresh_task_status(temp_task, Status.FAILED)

                for subtask in storage.get_subtasks(
                                   temp_task.user_login,
                                   temp_task,
                                   is_all=True,
                                   path=self.path
                               ):
                    if subtask.status != Status.FAILED:
                        queue.put(subtask)

            if task.parent:
                parent_task = storage.get_task_by_id(task.user_login, task.parent, path=self.path)
                self._recursive_fail_task(parent_task)

    @write_log
    def _refresh_task_status(self, task, new_status):
        task.status = new_status
        is_archive = Status.is_archive(new_status)
        self._refresh_task_reminders_status(task, is_archive=is_archive)
        storage.write_tasks_to_json(task.user_login, [task], path=self.path)

    @write_log
    def _refresh_task_reminders_status(self, task, is_archive):
        reminders = storage.get_task_reminders(task.user_login, task, path=self.path)
        for reminder in reminders:
            reminder.owner = reminder.owner._replace(is_archive=is_archive)
        storage.write_reminders_to_json(task.user_login, reminders, path=self.path)

    @write_log
    def add_group_to_task(self, user_login, task_id, group_name):
        """Adding group to task.

        Get task with using get_task_by_id, checks if the group is already in task and, if not, add group to
        task and save changes.

        Args:
            user_login (str):
            task_id (str):
            group_name (str):
        """
        task = storage.get_task_by_id(user_login, task_id, path=self.path)
        tracker_tools.check_group_name_unique_in_entity(task, group_name)
        task.groups.append(group_name)
        storage.write_tasks_to_json(user_login, [task], path=self.path)

    @write_log
    def add_group_to_reminder(self, user_login, reminder_id, group_name):
        """Adding group to reminder.

        Get reminder with using get_reminder_by_id, checks if the group is already in reminder and, if not,
        add group to reminder and save changes.

        Args:
            user_login (str):
            reminder_id (str):
            group_name (str):
        """
        reminder = storage.get_reminder_by_id(user_login, reminder_id, path=self.path)
        tracker_tools.check_group_name_unique_in_entity(reminder, group_name)
        reminder.groups.append(group_name)
        storage.write_reminders_to_json(user_login, [reminder], path=self.path)

    @write_log
    def add_reminder(self, user_login, description, notify_date=None, owner=None, groups=None):
        """Add reminder.

        Create reminder, if reminder has owner, notify him, add reminder to user reminders and save changes.

        Args:
            user_login (str):
            description (str):
            notify_date (:obj: `date`, optional): Default to None.
            owner (:obj: `ReminderOwner`, optional): Defaults to None.
            groups (:obj: `str`, optional): Default to None.
        """
        user = storage.get_user_by_login(user_login, path=self.path)
        reminder = Reminder(user_login, description, notify_date, owner=owner, groups=groups)

        if owner:
            if owner.type == EntitiesType.TASK:
                task = storage.get_task_by_id(user_login, owner.r_id, path=self.path)
                reminder.description = reminder.description if reminder.description else task.description
                reminder.owner = reminder.owner._replace(is_archive=task.is_archive())
                task.reminders.append(reminder.r_id)
                storage.write_tasks_to_json(user_login, [task], path=self.path)

        storage.write_reminders_to_json(
            user_login,
            [reminder],
            update_edition_time=False,
            path=self.path
        )
        user.reminders.append(reminder.r_id)
        storage.write_user_to_json(user, path=self.path)

    @write_log
    def edit_task(self, user_login, task_id, description=None, deadline=None, priority=None):
        """Edit task

        Get task with using get_task_by_id function, if any from keyword arguments is not None,
        refresh corresponding field and save changes.

        Args:
            user_login (str):
            task_id (str):
            description (:obj: `str`, optional): Defaults to None.
            deadline (:obj: `datetime`, optional): Defaults to None.
            priority (:obj: `Priority`, optional): Defaults to None.
        """
        task = storage.get_task_by_id(user_login, task_id, path=self.path)
        if not (description or deadline or priority):
            return
        if description:
            task.description = description
        if deadline:
            task.deadline = deadline
        if priority:
            task.priority = priority
        if description or deadline or priority:
            storage.write_tasks_to_json(user_login, [task], path=self.path)

    @write_log
    def edit_reminder(self, user_login, r_id, description=None, notify_date=None):
        """Edit reminder.

        Get reminder with using get_reminder_by_id function, if any from keyword arguments is not None,
        refresh corresponding fields and save changes.

        Args:
            user_login (str):
            r_id (str):
            description (:obj: `str`, optional): Defaults to None.
            notify_date (:obj: `datetime`, optional): Defaults to None.
            
        """
        reminder = storage.get_reminder_by_id(user_login, r_id, path=self.path)
        if description:
            reminder.description = description
        if notify_date:
            reminder.notify_date = notify_date
        if description or notify_date:
            storage.write_reminders_to_json(user_login, [reminder], path=self.path)

    @write_log
    def get_group(self, user_login, group_name):
        """Get group.

        Get group tasks and group reminders with using get_all_group_entities, init group object.

        Args:
            user_login (str):
            group_name (str):
            

        Returns:
            'namedtuple': Namedtuple with group name and lists of tasks and reminders.
        """
        Group = namedtuple('Group', 'name, tasks, reminders')
        group_tasks, group_reminders = storage.get_all_group_entities(
                                           user_login,
                                           group_name,
                                           path=self.path
                                       )
        group = Group(group_name, group_tasks, group_reminders)
        return group

    @write_log
    def get_all_groups(self, user_login):
        """Get all groups.

        Get all tasks and reminders that have one or more groups, breaks received from storage
        tasks and reminders lists into groups.

        Args:
            user_login (str):

        Returns:
            'list': List of namedtuple objects with group name and lists of tasks and reminders.
        """
        Group = namedtuple('Group', 'name, tasks, reminders')
        group_tasks, group_reminders = storage.get_all_groups_entities(user_login, path=self.path)
        groups = []

        for obj in group_tasks + group_reminders:
            for group_name in obj.groups:
                if not tracker_tools.is_group_name_in_groups(groups, group_name):
                    groups.append(Group(group_name, [], []))
                group_index = tracker_tools.find_group_index_in_groups(groups, group_name)

                if isinstance(obj, Task):
                    groups[group_index].tasks.append(obj)

                elif isinstance(obj, Reminder):
                    groups[group_index].reminders.append(obj)
        return groups

    def get_all_tasks(self, user_login):
        """Get all tasks.

        Get all user tasks with using get_all_tasks.

        Args:
            user_login (str):

        Returns:
            'list': List of all user tasks.
        """
        all_user_tasks = storage.get_all_tasks(user_login, path=self.path)
        shared_tasks = storage.get_shared_tasks(user_login, is_all=True, path=self.path)
        all_user_tasks.extend(shared_tasks)
        return all_user_tasks

    @write_log
    def get_tasks_from_period(self, user_login, start_date=None, end_date=None, only_archive=False):
        """Get archive or not archive tasks from date.

        Check period dates, if any of dates is not specified, puts to it a default value,
        then get tasks from period with using storage.get_tasks_from_period.

        Args:
            user_login (str):
            start_date (:obj: `date`, optional): Defaults to None.
            end_date (:obj: `date`, optional): Defaults to None.
            only_archive (:obj: `bool`, optional): Indicates that only archive tasks will be sorted out.
                Defaults to False:.

        Returns:
            'list': List of tasks from specified period that meet archive conditions.
        """
        if not start_date or not end_date:
            start_date, end_date = tracker_tools.transform_show_dates(start_date, end_date)
        return storage.get_tasks_from_period(
                   user_login,
                   start_date,
                   end_date,
                   only_archive=only_archive,
                   path=self.path
               )

    @write_log
    def get_all_subtasks(self, user_login, parent_task_id):
        """ Get all task subtasks.

        Args:
            user_login (str):
            parent_task_id (str):

        Returns:
            'list': List of parent tasks subtasks(including parent task, subtasks, subtasks
                of subtasks and etc.).
        """
        parent_task = storage.get_task_by_id(user_login, parent_task_id, path=self.path)
        return storage.get_all_subtasks(user_login, parent_task, path=self.path)

    @write_log
    def get_all_reminders(self, user_login):
        """Get all reminders.

            Get all user reminders with using get_all_tasks.

            Args:
                user_login (str):

            Returns:
                'list': List of all user reminders.
            """
        return storage.get_all_reminders(user_login, path=self.path)

    @write_log
    def get_reminders_from_period(self, user_login, start_date, end_date, only_archive=False):
        """Get archive or not archive reminders from date.

            Check period dates, if any of dates is not specified, puts to it a default value,
            then get reminders from period with using storage.get_reminders_from_period.

            Args:
                user_login (str):
                start_date (:obj: `date`, optional): Defaults to None.
                end_date (:obj: `date`, optional): Defaults to None.
                only_archive (:obj: `bool`, optional): Indicates that only archive reminders will be sorted out.
                    Defaults to False:.

            Returns:
                'list': List of reminders from specified period that meet archive conditions.
            """
        if not start_date or not end_date:
            start_date, end_date = tracker_tools.transform_show_dates(start_date, end_date)
        return storage.get_reminders_from_period(
                   user_login,
                   start_date,
                   end_date,
                   only_archive=only_archive,
                   path=self.path
               )

    @write_log
    def share_task(self, user_login, receiver_login, task_id, rights=Permission.READ):
        """Share task with other users.

        Get sender and receiver user objects with using get_user_by_login, append to them relevant information
        and save changes.

        Args:
            user_login (str): Login of user-sender.
            receiver_login (str):
            task_id (str): Task's t_id, that will be shared.
            rights (Permission): Receiver user rights on changing task.

        Raises:
            LoginNameError: If sender and receiver logins are equal.
        """
        if user_login == receiver_login:
            raise LoginNameError(sender_receiver_is_equal=True)
        sender = storage.get_user_by_login(user_login, path=self.path)
        receiver = storage.get_user_by_login(receiver_login, path=self.path)

        shared_task = storage.get_task_by_id(sender.login, task_id, path=self.path)
        tracker_tools.create_receiver_to_task_relationships(receiver, shared_task, rights)
        sender.shared_tasks.append(Shared(receiver.login, rights, SharedType.SEND, shared_task.t_id))
        receiver.shared_tasks.append(Shared(sender.login, rights, SharedType.RECEIVED, shared_task.t_id))

        storage.write_tasks_to_json(sender.login, [shared_task], path=self.path)
        storage.write_user_to_json(sender, path=self.path)
        storage.write_user_to_json(receiver, path=self.path)

    @write_log
    def get_shared_tasks(self, user_login, only_archive=False, is_all=False):
        """Get received tasks from other users.

        Args:
            user_login (str):
            only_archive (:obj: `bool`, optional): Defaults to False.
            is_all (:obj: `bool`, optional): Indicates that all tasks will be taken . Defaults to False.

        Returns:
            'list': List of received from other users tasks that satisfy archive condition.
        """
        return storage.get_shared_tasks(user_login, only_archive=only_archive, is_all=is_all, path=self.path)

    @write_log
    def add_scheduler(self, user_login, task_info, start_date=None, end_date=None, is_year=False, is_month=False,
                      is_day=True, amount_differences=None):
        """Add scheduler and save it.

        Args:
            user_login (str):
            start_date (:obj: `date`, optional): At that date will be created first task . Defaults to None.
            end_date (:obj: `date`, optional):  After this date, tasks will not be created. Defaults to None.
            is_year (:obj: `bool`, optional): Indicates that date difference between created tasks
                will count in years.
            is_month (:obj: `bool`, optional): Indicates that date difference between created tasks
                will count in months.
            is_day (:obj: `bool`, optional): Indicates that date difference between created tasks
                will count in days.
            amount_differences (:obj: `bool`, optional): Amount of years/months/days will be between
                created tasks.
            task_info (dict): dict with keys - task fields names, values - values of relevant fields.
        """
        scheduler = Scheduler(
                        user_login,
                        task_info,
                        start_date,
                        end_date,
                        is_year,
                        is_month,
                        is_day,
                        amount_differences
                    )

        user = storage.get_user_by_login(user_login, path=self.path)
        user.schedulers.append(scheduler.s_id)

        storage.write_schedulers_to_json(user_login, [scheduler], path=self.path)
        storage.write_user_to_json(user, path=self.path)

    @write_log
    def check_scheduler(self, user_login):
        """Checks schedulers for uncreated tasks.

        If schedulers have uncreated tasks, create this tasks, append them to user tasks, then save all changes.

        Args:
            user_login (str):
            
        """
        user = storage.get_user_by_login(user_login, path=self.path)

        schedulers = storage.get_uncreated_schedulers(user_login, path=self.path)
        scheduler_tasks = tracker_tools.take_tasks_from_schedulers(schedulers)
        tracker_tools.add_tasks_to_user(user, scheduler_tasks)

        storage.write_tasks_to_json(
            user_login,
            scheduler_tasks,
            update_edition_time=False,
            path=self.path)
        storage.write_user_to_json(user, path=self.path)
        if schedulers:
            storage.write_schedulers_to_json(user_login, schedulers, path=self.path)

    @write_log
    def get_schedulers(self, user_login, is_all=False):
        """Get user schedulers.

        Args:
            user_login (str):
            is_all (:obj: `bool`, optional): Indicates that empty schedulers will be taken too.

        Returns:
            'list': List of all user schedulers.
        """

        if is_all:
            return storage.get_all_schedulers(user_login, path=self.path)
        return storage.get_non_empty_schedulers(user_login, path=self.path)

    @write_log
    def create_user_storage(self, user_login):
        """Init new user and create his personal data storage.

            Args:
                user_login (str):
                
            """
        new_user = User(user_login)
        storage.create_user_storage(new_user, path=self.path)

    def get_schedulers_from_period(self, user_login, start_date, end_date):
        """Get schedulers from date.

        Check period dates, if any of dates is not specified, puts to it a default value,
        then get schedulers from period with using storage.get_schedulers_from_period.

        Args:
            user_login (str):
            start_date (:obj: `date`, optional): Defaults to None.
            end_date (:obj: `date`, optional): Defaults to None.

        Returns:
            'list': List of schedulers from specified period.
        """
        if not start_date or not end_date:
            start_date, end_date = tracker_tools.transform_show_dates(start_date, end_date)
        user_schedulers = self.get_schedulers(user_login)
        return [scheduler for scheduler in user_schedulers if scheduler.is_create_at_period(start_date, end_date)]

    @write_log
    def delete_scheduler(self, user_login, scheduler_id):
        """Delete scheduler.

        Get scheduler with help of function get_scheduler_by_id, then delete scheduler.

        Args:
            user_login (str):
            scheduler_id (str):
            
        """
        deleted_scheduler = storage.get_scheduler_by_id(user_login, scheduler_id, path=self.path)
        storage.delete_schedulers_by_id(user_login, [scheduler_id], path=self.path)

        user = storage.get_user_by_login(user_login, path=self.path)
        user.schedulers.remove(deleted_scheduler.s_id)
        storage.write_user_to_json(user, self.path)

    @write_log
    def mark_overdue_tasks(self, user_login):
        """Fail tasks whose deadline has expired.

        Args:
            user_login (str):
            
        """
        overdue_tasks = storage.get_overdue_tasks(user_login, path=self.path)
        for overdue_task in overdue_tasks:
            self._recursive_fail_task(overdue_task)
        storage.write_tasks_to_json(user_login, overdue_tasks, path=self.path)

    @write_log
    def get_missed_reminders(self, user_login):
        """Get reminders that should have notified but didn't.

        Args:
            user_login (str):
            

        Returns:
            'list':
        """
        return storage.get_missed_reminders(user_login, path=self.path)

    @write_log
    def mark_reminders_as_notified(self, user_login, reminders):
        """Mark missed reminders as notified.

        Args:
            user_login (str):
            reminders (list):
            
        """
        for reminder in reminders:
            reminder.is_notified = True
        storage.write_reminders_to_json(user_login, reminders, path=self.path)
