"""Implements modules for ensuring the right interaction between console and storage.

Package modules that gets request or data from cli, transforms getting information
and performs the necessary action with storage functions helping.

* tracker.py        directly the point of collecting, transforming and delivering of information.
* tracker_tools.py  sets of supporting for functions.
"""