"""Supporting functions for module tracker.py"""

from datetime import date
from dateutil.relativedelta import relativedelta
from extraeye_lib.exceptions.name_exceptions import (
    GroupNameError,
    LoginNameError
)
from extraeye_lib.exceptions.id_exceptions import (
    TaskIdError,
)


def create_subtask_relationships(parent_task, subtask):
    """Create task to subtask relationships.

    Append subtask id to parent_task subtasks and parent_task t_id to subtask parent.

    Args:
        parent_task (Task):
        subtask (Task):

    Raises:
        TaskIdError: If parent task already has subtask id in subtasks
    """
    if subtask.t_id in parent_task.subtasks:
        raise TaskIdError(task_subtask_relationships_already_exists=True)
    parent_task.subtasks.append(subtask.t_id)
    subtask.parent = parent_task.t_id


def is_group_name_in_groups(groups, group_name):
    """Checks if there is a group named group_name in groups.

    Args:
        groups (list):
        group_name (str):

    Returns:
        {
            'True': If group with this name is in groups,
            'False': Otherwise.
        }
    """
    for group in groups:
        if group.name == group_name:
            return True
    return False


def find_group_index_in_groups(groups, group_name):
    """Find position of group.

    Find position of group with name equal to group_name.

    Args:
        groups (list):
        group_name (str):

    Returns:
        'int': Position of group with name equal to group_name in groups.

    Raises:
        GroupNameError: If group with name equal to group_name not found.
    """
    for index, group in enumerate(groups):
        if group.name == group_name:
            return index
    raise GroupNameError()


def check_group_name_unique_in_entity(entity, group_name):
    """Checking group name uniqueness.

    Checks if there is a group with that name in the entity's groups.

    Args:
        entity (Task or Reminder):
        group_name (str):

    Raises:
        GroupNameError: If such group already exists in entity's groups.
    """
    if group_name in entity.groups:
        raise GroupNameError(already_have_entity=True)


def create_receiver_to_task_relationships(receiver, shared_task, rights):
    """Create task to receiver relationships.

    Append to sender-user shared tasks note about receiver-user and his rights.

    Args:
        receiver (User):
        shared_task (Shared):

    Raises:
        LoginNameError: If receiver already has this task.
    """
    if receiver.login in shared_task.other and shared_task.other[receiver.login] == rights.value:
        raise LoginNameError(receiver_already_have_task=True)
    else:
        shared_task.other.update({receiver.login: rights})


def take_tasks_from_schedulers(schedulers):
    """Pop scheduler tasks from schedulers.

    Get task from separate scheduler, after this scheduler next creation task date shifts or
    scheduler becomes empty.

    Args:
        schedulers (list):

    Returns:
        'list': List of popped scheduler tasks.
    """
    scheduler_tasks = []
    for scheduler in schedulers:
        while not scheduler.is_empty() and scheduler.get_next_date() <= date.today():
            scheduler_tasks.append(scheduler.pop_task())
    return scheduler_tasks


def add_tasks_to_user(user, tasks):
    """Add task id to user tasks.

    Args:
        user (User):
        'list': Tasks list that will be added to user tasks
    """
    for task in tasks:
        user.tasks.append(task.t_id)


def transform_show_dates(start_date=None, end_date=None):
    """Set dates with value None in default values.

    Set dates with value None in default values. If start_date is None, start_date will set in today date,
    if end_date is None, end_date will set in start_date + 1 week.

    Args:
        start_date (:obj: `date`, optional): Defaults to None.
        end_date (:obj: `date`, optional): Defaults to None.

    Returns:
        {
            'start_date': transformed start_date,
            'end_date': transformed end_date.
        }
    """
    if not start_date and not end_date:
        start_date = date.today()

    if not start_date and end_date:
        if end_date < date.today():
            start_date = end_date - relativedelta(weeks=1)
        else:
            start_date = date.today()

    if start_date and not end_date:
        end_date = start_date + relativedelta(weeks=1)

    return start_date, end_date


def find_shared_task_index(shared_tasks, task_id):
    """Get position of shared_task with t_id equal to task's task_id from shared_tasks list.

    Args:
        shared_tasks (list): entities list of type Shared
        task_id (str):

    Returns:
        'int': Position of shared task with t_id equal to task's task_id from shared tasks list.

    Raises:
        TaskIdError: If scheduler task not found
    """
    for index, shared_task in enumerate(shared_tasks):
        if shared_task.t_id == task_id:
            return index
    raise TaskIdError()
