"""Module consist of functions for working with extraeye logger."""

import os
import logging

from functools import wraps


EXTRAEYE_LOGGER_NAME = 'extraeye logger'


def setup_extraeye_logger(
        logger_level=logging.INFO,
        logger_formatter='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
        logger_path=os.path.join(os.environ['HOME'], 'extraeye_log.log'),
        enabled=True
):
    """Setup extraeye logger.

    Args:
     logger_level (:obj: `logging.Level`, optional): Defaults to logging.INFO.
     logger_formatter (:obj: `str`, optional): Defaults to '%(asctime)s - %(name)s - %(levelname)s - %(message)s'.
     logger_path (:obj: `os.path`, optional): Defaults to os.path.join(os.environ['HOME'], 'extraeye_log.log').
     enabled (:obj: `bool`, optional): Default to True.
    """
    file_handler = logging.FileHandler(logger_path)
    formatter = logging.Formatter(logger_formatter)
    file_handler.setFormatter(formatter)

    logger = get_extraeye_logger()
    logger.setLevel(logger_level)
    logger.addHandler(file_handler)

    logger.disabled = False if enabled else True


def get_extraeye_logger():
    return logging.getLogger(EXTRAEYE_LOGGER_NAME)


def write_log(func):
    """Function-decorator for logger.

    The decorator that wraps functions and writes it on entering and exiting function. Moreover
    writes information about exceptions that occurred during the function code execution.

    Args:
        func (function): Function thar will be decorated.

    Returns:
        'function': Wrapped function.

    Raises:
        Different exception types: If exception appears in wrapped function.
    """
    logger = get_extraeye_logger()

    @wraps(func)
    def wrapped(*args, **kwargs):
        try:
            logger.info('get in %s.%s' % (func.__module__, func.__name__))
            result = func(*args, **kwargs)
            logger.info('get out %s.%s' % (func.__module__, func.__name__))
            return result
        except Exception as error:
            logger.error('error appears in %s.%s' % (func.__module__, func.__name__), exc_info=True)
            raise error

    return wrapped
