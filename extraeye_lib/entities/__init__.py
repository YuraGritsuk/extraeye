"""Modules describing specialized instances for application work.

* reminder.py  represent class Reminder, enums and types for work with reminders.
* scheduler.py represent class Scheduler.
* task.py      represent class Task, enums and types to work with tasks.
* time_span.py represent class TimeSpan.
* user.py      represent class User.
"""
