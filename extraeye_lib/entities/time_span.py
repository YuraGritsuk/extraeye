"""Module consist of TimeSpan class, which provides basic work with creation and edition time."""

from datetime import datetime


class TimeSpan:
    """Class for represent creation and edition time of entity."""

    def __init__(self, creation=None, edition=None):
        """Init TimeSpan object."""

        if creation is None:
            creation = datetime.strptime(datetime.now().strftime('%Y-%m-%d %H:%M'), '%Y-%m-%d %H:%M')
        if edition is None:
            edition = datetime.strptime(datetime.now().strftime('%Y-%m-%d %H:%M'), '%Y-%m-%d %H:%M')

        self.creation = creation
        self.edition = edition
