"""Module contains class Scheduler."""

import uuid

from extraeye_lib.entities.task import Task
from dateutil.relativedelta import relativedelta
from extraeye_lib.entities.time_span import TimeSpan
from extraeye_lib.exceptions.other_exceptions import SchedulerAlreadyCreatedAllTasksError
from datetime import (
    date,
    datetime
)


class Scheduler:
    """Class for represent scheduler."""

    def __init__(
            self,
            user_login,
            task_info,
            start_date=None,
            end_date=None,
            is_year=False,
            is_month=False,
            is_day=False,
            amount_differences=None,
            tasks_upload=0,
            s_id=None,
            creation_datetime=None,
            edition_datetime=None
    ):
        """Init new scheduler object."""

        if s_id is None:
            s_id = uuid.uuid1().__str__()
        if amount_differences is None:
            amount_differences = 1
        if start_date is None:
            start_date = date.today() + relativedelta(days=1)
        if end_date is None:
            end_date = start_date
        if not (is_year and is_month and is_day):
            is_day = True

        self.s_id = s_id
        self.user_login = user_login

        self.task_info = task_info
        self.tasks_upload = tasks_upload

        if is_year:
            self.date_delta = relativedelta(years=amount_differences)
        elif is_month:
            self.date_delta = relativedelta(months=amount_differences)
        elif is_day:
            self.date_delta = relativedelta(days=amount_differences)

        self.start_date = start_date
        self.end_date = end_date
        self.time = TimeSpan(creation_datetime, edition_datetime)

    def _is_create_at_date(self, supposed_date):
        temp_tasks_upload = self.tasks_upload
        temp_date = self.start_date + temp_tasks_upload * self.date_delta

        while temp_date < supposed_date and temp_date <= self.end_date:
            temp_tasks_upload += 1
            temp_date = self.start_date + temp_tasks_upload * self.date_delta

        if temp_date == supposed_date and temp_date <= self.end_date:
            return True
        else:
            return False

    def is_create_at_period(self, start_date, end_date):
        """Checking does the scheduler need to create a task on this period.

        Args:
            start_date (date):
            end_date (date):

        Returns:
            {
                'True': If scheduler need to create a task on this period,
                'False': Otherwise.
            }
        """
        if end_date <= date.today():
            return False

        temp_date = start_date if start_date > date.today() else date.today() + relativedelta(days=1)
        while temp_date <= end_date:
            if self._is_create_at_date(temp_date):
                return True
            temp_date += relativedelta(days=1)
        return False

    def pop_task(self):
        """Get scheduler task and increment tasks_upload field.

        Returns:
            'Task': Scheduler task.
        """
        if self.start_date + self.tasks_upload * self.date_delta > self.end_date:
            raise SchedulerAlreadyCreatedAllTasksError()
        new_task = self._init_task()
        self.tasks_upload += 1
        return new_task

    def get_task_info(self):
        """Get scheduler task fields by dict.

        Returns:
            'dict': Task_info.
        """
        return self.task_info

    def _init_task(self):
        """Initialize scheduler task and get it.

        Returns:
            'Task': Scheduler task.
        """
        return Task(self.user_login, self.task_info['description'],
                    creation_datetime=datetime.combine(self.get_next_date(), datetime.min.time()),
                    edition_datetime=datetime.combine(self.get_next_date(), datetime.min.time())
                    )

    def get_next_date(self):
        """Get the nearest date of creating scheduler task.

        Returns:
            'date': Nearest date of creating scheduler task/

        Raises:
            ValueError: If scheduler is empty.
        """
        if not self.is_empty():
            return self.start_date + self.tasks_upload * self.date_delta
        raise SchedulerAlreadyCreatedAllTasksError()

    def is_empty(self):
        """Check if all scheduler tasks have been created.

        Returns:
            {
                'True': If next_date <= end_date,
                'False': Otherwise.
            }
        """
        next_date = self.start_date + self.tasks_upload * self.date_delta
        if next_date <= self.end_date:
            return False
        return True
