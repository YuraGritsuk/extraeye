"""Module represent class for users."""


class User:
    """Class for represent user."""

    def __init__(
            self,
            login,
            tasks=None,
            reminders=None,
            shared_tasks=None,
            schedulers=None,
            human=None
    ):
        """Init new user object."""

        if tasks is None:
            tasks = []
        if reminders is None:
            reminders = []
        if shared_tasks is None:
            shared_tasks = []
        if schedulers is None:
            schedulers = []

        self.login = login
        self.human = human

        self.tasks = tasks
        self.reminders = reminders
        self.shared_tasks = shared_tasks
        self.schedulers = schedulers
