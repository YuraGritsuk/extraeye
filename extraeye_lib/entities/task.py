"""Module consists of class for represent tasks and describing tasks enums."""

import uuid

from enum import Enum
from collections import namedtuple
from extraeye_lib.entities.time_span import TimeSpan


class Priority(Enum):
    """Enum that define task importance."""
    LOW = 1
    MEDIUM = 2
    HIGH = 3


class Status(Enum):
    """Enum that define task condition."""
    DONE = 1
    ACTIVE = 2
    FAILED = 3

    @staticmethod
    def is_archive(status):
        """Check whether status is archive.

    Args:
        status (Status):

    Returns:
        {
            'True': If status is not equal to Status.ACTIVE,
            'False': otherwise.
        }
        """
        if status != Status.ACTIVE:
            return True
        return False


class Task:
    """Class that represent extraeye task."""
    def __init__(
            self,
            user_login,
            description,
            priority=Priority.MEDIUM,
            status=Status.ACTIVE,
            t_id=None,
            deadline=None,
            creation_datetime=None,
            edition_datetime=None,
            parent=None,
            other=None,
            subtasks=None,
            reminders=None,
            groups=None,
    ):
        """Initializing new task fields."""

        if t_id is None:
            t_id = uuid.uuid1().__str__()
        if priority is None:
            priority = Priority.MEDIUM
        if status is None:
            status = Status.active
        if other is None:
            other = {}
        if subtasks is None:
            subtasks = []
        if reminders is None:
            reminders = []
        if groups is None:
            groups = []

        self.t_id = t_id
        self.user_login = user_login

        self.status = status
        self.priority = priority
        self.parent = parent
        self.description = description

        self.deadline = deadline
        self.time = TimeSpan(creation_datetime, edition_datetime)

        self.subtasks = subtasks
        self.reminders = reminders
        self.groups = groups
        self.other = other

    def is_archive(self):
        """Check whether the task is archive.

        Returns:
            {
                'False': If task status is equal to Status.ACTIVE,
                'True': Otherwise.
            }
        """
        if self.status == Status.ACTIVE:
            return False
        return True


Shared = namedtuple('Shared', 'user_login, permission, shared_type, t_id')


class SharedType(Enum):
    """Enum that define task share status."""
    SEND = 'send'
    RECEIVED = 'received'


class Permission(Enum):
    """Enum that define opportunities to interaction with task."""
    WRITE = 'w'
    READ = 'r'
