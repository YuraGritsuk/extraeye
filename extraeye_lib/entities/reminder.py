"""Module represent classes for work with reminders in application."""

import uuid

from enum import Enum
from datetime import date
from collections import namedtuple
from dateutil.relativedelta import relativedelta
from extraeye_lib.entities.time_span import TimeSpan


class Reminder:
    """Class for represent extraeye reminder."""

    def __init__(
            self,
            user_login,
            description,
            notify_date=None,
            r_id=None,
            creation_datetime=None,
            edition_datetime=None,
            is_notified=False,
            owner=None,
            other=None,
            groups=None
    ):
        """Init new reminder object."""
        if r_id is None:
            r_id = uuid.uuid1().__str__()
        if notify_date is None:
            notify_date = date.today() + relativedelta(days=1)
        if other is None:
            other = {}
        if groups is None:
            groups = []

        self.r_id = r_id
        self.user_login = user_login

        self.description = description
        self.notify_date = notify_date
        self.is_notified = is_notified
        self.owner = owner
        self.groups = groups
        self.other = other

        self.time = TimeSpan(creation_datetime, edition_datetime)

    def is_archive(self):
        """Check whether the reminder is archive.

        Returns:
            {
                'True': If reminder already notified or reminder owner is archive entity.
                'False': Otherwise.
            }
        """
        if self.is_notified or (self.owner and self.owner.is_archive):
            return True
        return False


ReminderOwner = namedtuple('OwnerTask', 'type, r_id, is_archive')


class EntitiesType(Enum):
    """Enum for represent reminder owner types."""
    TASK = 'task'
    REMINDER = 'reminder'
