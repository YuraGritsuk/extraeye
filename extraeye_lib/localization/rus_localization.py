"""Functions that translate application language into russian language.

* convert_priority_to_str
* convert_status_to_str
* get_task_id_error_to_str
* get_reminder_id_error_to_str
* get_scheduler_id_error_to_str
* get_group_name_error_str
* get_login_name_error_str
* get_scheduler_already_created_all_tasks_error_str
* convert_date_delta_to_rus_str
"""


def convert_priority_to_str(priority_obj):
    """ Convert priority enum field to str in russian language.

    Args:
        priority_obj (Priority):

    Returns:
        {
            'Низкий': if priority_obj.value is equal to 1
            'Средний': if priority_obj.value is equal to 2
            'Высокий': if priority_obj.value is equal to 3
        }
    """
    if priority_obj.value == 1:
        return 'Низкий'
    if priority_obj.value == 2:
        return 'Средний'
    if priority_obj.value == 3:
        return 'Высокий'


def convert_status_to_str(status_obj):
    """ Convert status enum field to str in russian language.

    Args:
        status_obj (Status):

    Returns:
        {
            'Сделана': if priority_obj.value is equal to 1
            'Активная': if priority_obj.value is equal to 2
            'Провалена': if priority_obj.value is equal to 3
        }
    """
    if status_obj.value == 1:
        return 'Сделана'
    elif status_obj.value == 2:
        return 'Активная'
    elif status_obj.value == 3:
        return 'Провалена'


def get_task_id_error_rus_str(error):
    """ Convert TaskIdError to str in russian language.

    Args:
        error (TaskIdError):

    Returns:
        {
            'Отношение задача подзадача уже задано': If task_subtask_relationships_already_exists is True,
            'Задача с указанным id не найдена': Otherwise.
        }
    """
    if error.task_subtask_relationships_already_exists:
        return 'Отношение задача подзадача уже задано'
    else:
        return 'Задача с указанным id не найдена'


def get_reminder_id_error_rus_str():
    """Get ReminderIdError's message in russian language.

    Returns:
        'str':
    """
    return 'Напоминание с указанным id не найдено'


def get_scheduler_id_error_rus_str():
    """ Convert SchedulerIdError to str in russian language.

    Returns:
        'str':
    """
    return 'Планировщик с указанным id не найден'


def get_group_name_error_rus_str(error):
    """ Convert GroupNameError to str in russian language.

    Args:
        error (GroupNameError):

    Returns:
        {
            'Сущность уже есть в группе': If not_in_entity is True,
            'В группе уже есть указанная задача': If already_have_task is True,
            'В группе уже есть указанное напоминание': If already_have_reminder is True,
            'Неправильно указано имя группы': Otherwise.
        }
    """
    if error.not_in_entity:
        return 'Сущность уже есть в группе'
    elif error.already_have_entity:
        return 'В группе уже есть эта сущность'
    else:
        return 'Неправильно указано имя группы'


def get_login_name_error_rus_str(error):
    """ Convert LoginNameError to str in russian language.

    Args:
        error (GroupNameError):

    Returns:
        {
            'Логины отправителя и получателя совпадают': If sender_receiver_is_equal is True,
            'У получателя уже есть данная задача': If receiver_already_have_task is True,
            'В группе уже есть указанное напоминание': If already_have_reminder is True,
            'Неправильно указано логин': Otherwise.
        }
    """
    if error.sender_receiver_is_equal:
        return 'Логины отправителя и получателя совпадают'
    elif error.receiver_already_have_task:
        return 'У получателя уже есть данная задача'
    elif error.already_have_reminder:
        return 'В группе уже есть указанное напоминание'
    else:
        return 'Неправильно указано логин'


def get_scheduler_already_created_all_tasks_error_rus_str():
    """Get SchedulerAlreadyCreatedAllTasksError's message in russian language.

    Returns:
        'str': 'Планировщик уже создал все задачи'
    """
    return 'Планировщик уже создал все задачи'


def convert_date_delta_to_rus_str(date_delta):
    """Convert field scheduler field date_delta to str in russian language.

    Args:
        date_delta (relativedelta):

    Returns:
        {
            '<int> Год/Года/Лет': If is_years is True,
            '<int> Месяц/Месяца/Месяцев': If is_months is True,
            '<int> День/Дня/Дней': If is_days is True
        }
    """
    if date_delta.years:
        return '{} Год/Года/Лет'.format(date_delta.years)
    elif date_delta.months:
        return '{} Месяц/Месяца/Месяцев'.format(date_delta.months)
    else:
        return '{} День/Дня/Дней'.format(date_delta.days)
