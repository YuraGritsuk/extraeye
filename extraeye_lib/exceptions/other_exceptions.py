"""Describe failures that are related with special incorrect actions that cannot be unit in groups.

* SchedulerAlreadyCreatedAllTasksError
"""


class SchedulerAlreadyCreatedAllTasksError(Exception):
    """Class for describing error that appears while trying to pop task from empty scheduler."""

    def __init__(self):
        """Init SchedulerAlreadyCreatedAllTasksError obj."""
        super().__init__('Scheduler has already created all planned tasks')
