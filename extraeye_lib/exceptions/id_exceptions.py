"""Describe failures that are related with using incorrect id's.

* TaskIdError
* ReminderIdError
* SchedulerIdError
"""


class TaskIdError(IndexError):
    """Class for representing errors associated with incorrect tasks id."""
    
    def __init__(self, task_subtask_relationships_already_exists=False):
        """Init TaskIdError object."""
        self.task_subtask_relationships_already_exists = task_subtask_relationships_already_exists

        if task_subtask_relationships_already_exists:
            super().__init__('Task subtask relationships already exists')
        else:
            super().__init__()


class ReminderIdError(IndexError):
    """Class for representing errors associated with incorrect reminders id."""

    def __init__(self):
        """Init ReminderIdError object."""
        super().__init__('Incorrect reminder id')


class SchedulerIdError(IndexError):
    """Class for representing errors associated with incorrect schedulers id."""

    def __init__(self):
        """Init SchedulerIdError object."""
        super().__init__('Incorrect scheduler id')
