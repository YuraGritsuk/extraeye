"""Describe failures that are related with using incorrect names.

* LoginNameError
* GroupNameError
"""


class LoginNameError(NameError):
    """Class for describing errors that are appears while using incorrect login names."""

    def __init__(
            self,
            sender_receiver_is_equal=False,
            receiver_already_have_task=False,

    ):
        """Init LoginNameError obj."""
        self.sender_receiver_is_equal = sender_receiver_is_equal
        self.receiver_already_have_task = receiver_already_have_task

        if sender_receiver_is_equal:
            super().__init__('Sender and receiver logins are equal')
        elif receiver_already_have_task:
            super().__init__('Receiver already have this task')
        else:
            super().__init__('Incorrect user login')


class GroupNameError(NameError):
    """Class for describing errors that are appears while using incorrect group names."""

    def __init__(
            self,
            not_in_entity=False,
            already_have_entity=False,
    ):
        """Init GroupNameError obj."""
        self.not_in_entity = not_in_entity
        self.already_have_entity = already_have_entity

        if not_in_entity:
            super().__init__('There is not group in this entity')
        elif already_have_entity:
            super().__init__('Group already has this entity')
        else:
            super().__init__('Incorrect group name')
