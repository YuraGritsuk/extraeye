"""Present special types for describe extraeye failures.

* id_exceptions     failures that are related with using incorrect id's
* name_exceptions   failures that are related with using incorrect names
* other_exceptions  present classes for all other failures
"""