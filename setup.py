from setuptools import setup, find_packages

setup(
    name='extraeye',
    version='1.1',
    author='Yura Gritsuk',
    packages=find_packages(),
    install_requires=['click', 'python-dateutil', 'django'],
    test_suite='extraeye_lib.tests.run_tests',
    entry_points='''
        [console_scripts]
        eye=console_api.extraeye_entry:extraeye_entry
        '''
    )
