"""Application entry point.

Functions that are called directly after entering commands by user.
"""

import sys
import click
import extraeye_lib.management.tracker
import console_api.console as console
import console_api.extraeye_entry_tools as entry_tools

from console_api import user_authorization

CONTEXT_SETTINGS = {'help_option_names': ['-h', '--help']}


@click.group(context_settings=CONTEXT_SETTINGS, help='Команда запускающая приложение')
@click.pass_context
def cli(cli_context):
    """Entry command.

    Here appears parsing subcommands and invoking them. Besides in function code occurs configuring logger,
    storage paths. If user is going to work with his data, it's presetting to work.

    Args:
        cli_context (click.core.Context): dict with main command settings.
    """
    _define_displaying_group_help_menu(cli_context.invoked_subcommand)
    try:
        _application_presetting(cli_context)
        if cli_context.invoked_subcommand != 'user':
            _date_presetting(cli_context)
    except NameError as error:
        console.print_error(error)
        sys.exit(-1)
    except IndexError as error:
        console.print_error(error)
        sys.exit(-1)
    except FileExistsError as error:
        console.print_error(error)
        sys.exit(-1)


def _application_presetting(cli_context):
    entry_tools.setup_extraeye_logger()

    storage_path = entry_tools.setup_storage_paths()
    config_path, user_register_info_dir = entry_tools.setup_authorization_paths()

    extraeye_lib_tracker = extraeye_lib.management.tracker.Tracker(customized_path=storage_path)

    cli_context.obj = {
        'extraeye_lib_tracker': extraeye_lib_tracker,
        'config_path': config_path,
        'user_register_info_dir': user_register_info_dir
    }


def _date_presetting(environment):

    if not user_authorization.is_current_user_registered(
            environment.obj['config_path'],
            environment.obj['user_register_info_dir']
    ):
        console.print_error('Зарегистрируйтесь или войдите в аккаунт')
        sys.exit(-1)
    user_login = user_authorization.get_current_user_login(environment.obj['config_path'])
    environment.obj['user_login'] = user_login

    extraeye_lib_tracker = environment.obj['extraeye_lib_tracker']

    extraeye_lib_tracker.check_scheduler(environment.obj['user_login'])
    extraeye_lib_tracker.mark_overdue_tasks(environment.obj['user_login'])

    missed_reminders = extraeye_lib_tracker.get_missed_reminders(environment.obj['user_login'])
    if missed_reminders:
        choice = console.suggest_show_reminders()
        if choice is not None:
            if choice:
                console.show_notify_reminders(missed_reminders)
            extraeye_lib_tracker.mark_reminders_as_notified(
                user_login=environment.obj['user_login'],
                reminders=missed_reminders,
            )


def _define_displaying_group_help_menu(invoked_command):
    if invoked_command == 'task':
        entry_tools.modify_usage_error(task)
    elif invoked_command == 'reminder':
        entry_tools.modify_usage_error(reminder)
    elif invoked_command == 'scheduler':
        entry_tools.modify_usage_error(scheduler)
    elif invoked_command == 'group':
        entry_tools.modify_usage_error(group)
    elif invoked_command == 'user':
        entry_tools.modify_usage_error(user)


@cli.group(help='Команды для работы с задачами')
def task():
    """Task commands group."""
    pass


@task.command(help='Добавить задачу')
@click.option('--description', '-d', callback=entry_tools.validate_task_description,
              help='Задать описание(обязательная)')
@click.option('--priority', '-pr', type=int, callback=entry_tools.validate_priority,
              help='Указать приоритет(1-низкий, 2-средний(по умолчанию), 3-высокий)')
@click.option('--deadline', '-dl', callback=entry_tools.validate_deadline_datetime, help='Задать крайний срок'
                                                                                         '(ДД.ММ.ГГГГ[_ЧЧ:ММ])')
@click.option('--group', '-g', callback=entry_tools.validate_group, multiple=True, help='Добавить задачу в группу')
@click.option('--parent', '-p', help='Указать id задачи-родителя')
@click.pass_obj
def add(environment, description, priority, deadline, group, parent):
    """Call add_task or add_subtask functions from extraeye_lib.manager.

    Args:
        environment (dict): Dict with application settings.
        description (str):
        priority (:obj: `Priority`, optional): Defaults to None.
        deadline (:obj: `datetime`, optional): Defaults to None.
        group (:obj: `list`, optional): List of groups to which the task belongs. Defaults to None.
        parent (:obj: `str`, optional): Defaults to None.
    """
    try:
        extraeye_lib_tracker = environment['extraeye_lib_tracker']

        if parent is None:
            extraeye_lib_tracker.add_task(
                environment['user_login'],
                description,
                priority,
                deadline,
                group,
            )
        else:
            extraeye_lib_tracker.add_subtask(
                environment['user_login'],
                parent,
                description,
                priority,
                deadline,
                group,
            )
    except NameError as error:
        console.print_error(error)
        sys.exit(-1)
    except IndexError as error:
        console.print_error(error)
        sys.exit(-1)


@task.command(help='Удалить задачу')
@click.option('--task-id', '-i', callback=entry_tools.validate_entity_id, help='Указать id задачи(обязательная)')
@click.pass_obj
def delete(environment, task_id):
    """Call delete_task function from extraeye_lib.manager.

    Args:
        environment (dict): Dict with application settings.
        task_id (str):
    """
    try:
        extraeye_lib_tracker = environment['extraeye_lib_tracker']

        extraeye_lib_tracker.delete_task(
            environment['user_login'],
            task_id,
        )
    except NameError as error:
        console.print_error(error)
        sys.exit(-1)
    except IndexError as error:
        console.print_error(error)
        sys.exit(-1)


@task.command(help='Пометить задачу выполненной')
@click.option('--task-id', '-i', callback=entry_tools.validate_entity_id, help='Указать id задачи(обязательная)')
@click.pass_obj
def done(environment, task_id):
    """Call done_task function from extraeye_lib.manager.

    Args:
        environment (dict): Dict with application settings.
        task_id (str):
    """
    try:
        extraeye_lib_tracker = environment['extraeye_lib_tracker']

        extraeye_lib_tracker.done_task(environment['user_login'], task_id)
    except NameError as error:
        console.print_error(error)
        sys.exit(-1)
    except IndexError as error:
        console.print_error(error)
        sys.exit(-1)


@task.command(help='Пометить задачу проваленной')
@click.option('--task-id', '-i', callback=entry_tools.validate_entity_id, help='Указать id задачи(обязательная)')
@click.pass_obj
def fail(environment, task_id):
    """Call fail_task function from extraeye_lib.manager.

    Args:
        environment (dict): Dict with application settings.
        task_id (str):
    """
    try:
        extraeye_lib_tracker = environment['extraeye_lib_tracker']

        extraeye_lib_tracker.fail_task(environment['user_login'], task_id, )
    except NameError as error:
        console.print_error(error)
        sys.exit(-1)
    except IndexError as error:
        console.print_error(error)
        sys.exit(-1)


@task.command(help='Поделиться задачей с другими пользователями')
@click.option('--receiver-login', '-rl', callback=entry_tools.validate_receiver_login,
              help='Указать логин пользователя, с которым хотите поделиться(обязательная)')
@click.option('--task-id', '-i', callback=entry_tools.validate_entity_id, help='Указать id задачи(обязательная)')
@click.pass_obj
def share(environment, receiver_login, task_id):
    """Call share_task function from extraeye_lib.manager.

    Args:
        environment (dict): Dict with application settings.
        receiver_login (str):
        task_id (str):
    """
    try:
        extraeye_lib_tracker = environment['extraeye_lib_tracker']

        extraeye_lib_tracker.share_task(
            environment['user_login'],
            receiver_login,
            task_id,
        )
    except NameError as error:
        console.print_error(error)
        sys.exit(-1)
    except IndexError as error:
        console.print_error(error)
        sys.exit(-1)


@task.command(help='Редактировать задачу')
@click.option('--task-id', '-i', callback=entry_tools.validate_entity_id, help='Указать id редактируемой '
                                                                               'задачи(обязательная)')
@click.option('--description', '-d', help='Задать новое описание')
@click.option('--priority', '-pr', type=int, callback=entry_tools.validate_priority, help='Задать новый приоритет(1-низ'
                                                                                          'кий, 2-средний, 3-высокий)')
@click.option('--deadline', '-dl', callback=entry_tools.validate_deadline_datetime, help='Изменить крайний срок'
                                                                                         '(ДД.ММ.ГГГГ[_ЧЧ:ММ])')
@click.pass_obj
def edit(environment, task_id, description, priority, deadline):
    """Call edit_task function from extraeye_lib.manager.

    Args:
        environment (dict): Dict with application settings.
        task_id (str):
        description (:obj: `str`, optional): Defaults to None.
        priority (:obj: `Priority`, optional): Defaults to None.
        deadline (:obj: `datetime`, optional): Defaults to None.
    """
    try:
        extraeye_lib_tracker = environment['extraeye_lib_tracker']

        extraeye_lib_tracker.edit_task(
            environment['user_login'],
            task_id,
            description=description,
            deadline=deadline,
            priority=priority,
        )
    except NameError as error:
        console.print_error(error)
        sys.exit(-1)
    except IndexError as error:
        console.print_error(error)
        sys.exit(-1)


@task.command(help='Просмотреть задачи')
@click.option('--full-info', '-f', is_flag=True, help='Подробный вывод')
@click.option('--is-all', '-a', is_flag=True, help='Показать все задачи пользователя')
@click.option('--only-archive', '-ar', is_flag=True, help='Показать архивные(используется в паре с другими '
                                                          'опциями)')
@click.option('--all-subtasks', '-as', help='Показать все подзадачи(подзадачи их подзадач и т.д.) задачи')
@click.option('--is-shared', '-sh', is_flag=True, help='Показать задачи, которыми с вами поделились')
@click.option('--start-date', '-sd', callback=entry_tools.validate_start_date_in_show,
              help='Указать первый день отображаемого периода(по умолчанию текущий день)')
@click.option('--end-date', '-ed', callback=entry_tools.validate_end_date_in_show,
              help='Указать последний день отображаемого периода(по умолчанию неделя с первого дня')
@click.pass_obj
def show(environment, full_info, is_all, only_archive, all_subtasks, is_shared, start_date, end_date):
    """Show user tasks.

    Get tasks that meet all conditions with using get_tasks functions from extraeye_lib.manager. Displaying them
    by console functions. Moreover display task that scheduler should to create at specified period.

    Args:
        environment (dict): Dict with application settings.
        full_info (:obj: `bool`, optional): Indicates displaying format. Defaults to False.
        is_all (:obj: `bool`, optional): Indicates that all user tasks will be received. Defaults to False.
        only_archive (:obj: `bool`, optional): Indicates that only archive tasks will be received. Defaults to False.
        all_subtasks (:obj: `str`, optional): Indicates parent task id which all subtasks will be shown.
            Defaults to None.
        is_shared (:obj: `bool`, optional): Indicates that only other user tasks will be received. Defaults to False.
        start_date (:obj: `date`, optional): Define first date of displaying period. Defaults to None.
        end_date (:obj: `date`, optional): Define last date of displaying period. Defaults to None.
    """
    try:
        extraeye_lib_tracker = environment['extraeye_lib_tracker']

        if is_all:
            tasks = extraeye_lib_tracker.get_all_tasks(
                environment['user_login'],
            )
        elif is_shared:
            tasks = extraeye_lib_tracker.get_shared_tasks(
                environment['user_login'],
                only_archive=only_archive,
            )
        elif all_subtasks:
            tasks = extraeye_lib_tracker.get_all_subtasks(
                environment['user_login'],
                all_subtasks,
            )
        else:
            tasks = extraeye_lib_tracker.get_tasks_from_period(
                environment['user_login'],
                start_date=start_date,
                end_date=end_date,
                only_archive=only_archive,
            )
        console.show_tasks(tasks, full_info)

        if not is_shared and not only_archive:
            if is_all:
                schedulers = extraeye_lib_tracker.get_schedulers(
                    environment['user_login'],
                )
            else:
                schedulers = extraeye_lib_tracker.get_schedulers_from_period(
                    environment['user_login'],
                    start_date, end_date,
                )
            console.show_planned_task(schedulers)

    except ValueError as error:
        console.print_error(error)
        sys.exit(-1)
    except NameError as error:
        console.print_error(error)
        sys.exit(-1)
    except IndexError as error:
        console.print_error(error)
        sys.exit(-1)


@cli.group(help='Команда для работы с напоминаниями')
def reminder():
    """Reminder commands group."""
    pass


@reminder.command(help='Добавить напоминание')
@click.option('--description', '-d', help='Задать описание(обязательная)')
@click.option('--notify-date', '-nd', callback=entry_tools.validate_date,
              help='Установить время напоминания(ДД.ММ.ГГГГ)')
@click.option('--owner', '-o', nargs=2, callback=entry_tools.validate_reminder_owner,
              help='Привязать напоминание по id, сначала указывается тип владельца(task), далее через пробел его id')
@click.option('--group', '-g', callback=entry_tools.validate_group, multiple=True, help='Добавить задачу в группу')
@click.pass_obj
def add(environment, description, notify_date, owner, group):
    """Call add_reminder function from extraeye_lib.manager.

   Args:
       environment (dict): Dict with application settings.
       description (str):
       notify_date (:obj: `date`, optional):
       owner (:obj: `ReminderOwner`, optional): Defaults to None.
       group (:obj: `list`, optional): List of groups to which the reminder belongs. Defaults to None.
   """
    try:
        extraeye_lib_tracker = environment['extraeye_lib_tracker']

        entry_tools.check_parameters_for_reminder(reminder, description, owner)
        extraeye_lib_tracker.add_reminder(
            environment['user_login'],
            description,
            notify_date,
            owner,
            group
        )
    except NameError as error:
        console.print_error(error)
        sys.exit(-1)
    except IndexError as error:
        console.print_error(error)
        sys.exit(-1)


@reminder.command(help='Удалить напоминание')
@click.option('--reminder-id', '-i', callback=entry_tools.validate_entity_id, help='Указать id напоминания'
                                                                                   '(обязательная)')
@click.pass_obj
def delete(environment, reminder_id):
    """Call delete_reminder function from extraeye_lib.manager.

    Args:
        environment (dict): Dict with application settings.
        reminder_id (str):
    """
    try:
        extraeye_lib_tracker = environment['extraeye_lib_tracker']

        extraeye_lib_tracker.delete_reminder(
            environment['user_login'],
            reminder_id,
        )
    except NameError as error:
        console.print_error(error)
        sys.exit(-1)
    except IndexError as error:
        console.print_error(error)
        sys.exit(-1)


@reminder.command(help='Редактировать напоминание')
@click.option('--reminder-id', '-i', callback=entry_tools.validate_entity_id, help='Указать id редактируемого '
                                                                                   'напоминания(обязательная)')
@click.option('--description', '-d', help='Задать новое описание')
@click.option('--notify-date', '-nd', callback=entry_tools.validate_date, help='Задать новую дату напоминания'
                                                                               '(ДД.ММ.ГГГГ)')
@click.pass_obj
def edit(environment, reminder_id, description, notify_date):
    """Call edit_task function from extraeye_lib.manager.

    Args:
        environment (dict): Dict with application settings.
        reminder_id (str):
        description (:obj: `str`, optional): Defaults to None.
        notify_date (:obj: `date`, optional): Defaults to None.
    """
    try:
        extraeye_lib_tracker = environment['extraeye_lib_tracker']

        extraeye_lib_tracker.edit_reminder(
            environment['user_login'],
            reminder_id,
            description,
            notify_date,

        )
    except NameError as error:
        console.print_error(error)
        sys.exit(-1)
    except IndexError as error:
        console.print_error(error)
        sys.exit(-1)


@reminder.command(help='Просмотреть напоминания')
@click.option('--full-info', '-f', is_flag=True, help='Подробность вывода')
@click.option('--is-all', '-a', is_flag=True, help='Показать все напоминания')
@click.option('--only-archive', '-ar', is_flag=True, help='Показать архивные(используется в паре с другими опциями')
@click.option('--start-date', '-sd', callback=entry_tools.validate_start_date_in_show,
              help='Указать первый день отображаемого периода(по умолчанию текущий день)')
@click.option('--end-date', '-ed', callback=entry_tools.validate_end_date_in_show,
              help='Указать последний день отображаемого периода(по умолчанию неделя с первого дня')
@click.pass_obj
def show(environment, is_all, only_archive, start_date, end_date, full_info):
    """Show user reminders.

    Get reminders that meet all conditions with using get_reminders functions from extraeye_lib.manager.
    Displaying them by console functions.

    Args:
        environment (dict): Dict with application settings.
        is_all (:obj: `bool`, optional): Indicates that all user tasks will be received.
            Defaults to False.
        only_archive (:obj: `bool`, optional): Indicates that only archive tasks will
            be received. Defaults to False.
        start_date (:obj: `date`, optional): Define first date of displaying period. Defaults to None.
        end_date (:obj: `date`, optional): Define last date of displaying period. Defaults to None.
        full_info (:obj: `bool`, optional): Indicates displaying format. Defaults to False.
    """
    try:
        extraeye_lib_tracker = environment['extraeye_lib_tracker']

        if is_all:
            reminders = extraeye_lib_tracker.get_all_reminders(
                environment['user_login']
            )
        else:
            reminders = extraeye_lib_tracker.get_reminders_from_period(
                environment['user_login'], start_date, end_date,
                only_archive=only_archive
            )
        console.show_reminders(reminders, full_info)
    except ValueError as error:
        console.print_error(error)
        sys.exit(-1)
    except NameError as error:
        console.print_error(error)
        sys.exit(-1)
    except IndexError as error:
        console.print_error(error)
        sys.exit(-1)


@cli.group(help='Команды для работы с планировщиками')
def scheduler():
    """Scheduler commands group."""
    pass


@scheduler.command(help='Добавить планировщик')
@click.option('--task-description', '-td', callback=entry_tools.validate_task_description,
              help='Задать описание создаваемой задачи(обязательная)')
@click.option('--start-date', '-sd', callback=entry_tools.validate_scheduler_start_date,
              help='Задать дату начала отсчета и создания первой задачи планировщика(по умолчанию завтрашний день)')
@click.option('--end-date', '-ed', callback=entry_tools.validate_scheduler_end_date,
              help='Указать дату конца работы планировщика(по умолчанию дата стартового дня)')
@click.option('--year-difference', '-y', is_flag=True, callback=entry_tools.validate_date_difference,
              help='Указать, что разница между задачами будет измеряться в годах')
@click.option('--month-difference', '-md', is_flag=True, callback=entry_tools.validate_date_difference,
              help='Указать, что разница между задачами будет измеряться в месяцах')
@click.option('--day-difference', '-dd', is_flag=True, callback=entry_tools.validate_date_difference,
              help='Указать, что разница между задачами будет измеряться в днях(по умолчанию)')
@click.option('--amount-difference', '-ad', type=int, default=1, help='Указать количество лет/месяцев/дней между '
                                                                      'создаваемыми задачами(по умолчанию 1)')
@click.pass_obj
def add(environment, task_description, start_date, end_date, year_difference, month_difference,
        day_difference, amount_difference):
    """Call add_scheduler function from extraeye_lib.manager.

    Args:
        environment (dict): Dict with application settings.
        task_description (str):
        start_date (:obj: `date`, optional): Define first date of creating tasks. Defaults to None.
        end_date (:obj: `date`, optional): Define last date of working scheduler. Defaults to None.
        year_difference (:obj: `bool`, optional): Indicate that date difference between created tasks
            will be counted in years. Defaults to False.
        month_difference (:obj: `bool`, optional): Indicate that date difference between created tasks
            will be counted in months. Defaults to False.
        day_difference (:obj: `bool`, optional): Indicate that date difference between created tasks
            will be counted in days. Defaults to False.
        amount_difference (:obj: `int`, optional): Indicate amount of delta years/months/days between
        created tasks. Defaults to 1.
    """
    try:
        extraeye_lib_tracker = environment['extraeye_lib_tracker']

        task_info = entry_tools.init_task_info(task_description)
        extraeye_lib_tracker.add_scheduler(
            environment['user_login'],
            task_info,
            start_date,
            end_date,
            year_difference,
            month_difference,
            day_difference,
            amount_difference
        )
    except NameError as error:
        console.print_error(error)
        sys.exit(-1)


@scheduler.command(help='Просмотреть планировщики')
@click.option('--full-info', '-f', is_flag=True, help='Подробный вывод(по умолчанию выключен)')
@click.option('--is-all', '-a', is_flag=True, help='Показывать, включая отработавшие планировщики')
@click.pass_obj
def show(environment, full_info, is_all):
    """Show user schedulers.

    Get schedulers with using get_schedulers function from extraeye_lib.manager.
    Displaying them by console functions.

    Args:
        environment (dict): Dict with application settings.
        full_info (:obj: `bool`, optional): Indicates displaying format. Defaults to False.
        is_all (:obj: `bool`, optional): Indicates that empty schedulers will be taken too.
    """
    try:
        extraeye_lib_tracker = environment['extraeye_lib_tracker']

        schedulers = extraeye_lib_tracker.get_schedulers(
                         environment['user_login'],
                         is_all=is_all
                     )
        console.show_schedulers(schedulers, full_info)
    except NameError as error:
        console.print_error(error)
        sys.exit(-1)


@scheduler.command(help='Удалить планировщик')
@click.option('--scheduler-id', '-i', callback=entry_tools.validate_entity_id, help='Указать id планировщика'
                                                                                    '(обязательная)')
@click.pass_obj
def delete(environment, scheduler_id):
    """Call delete_scheduler function from extraeye_lib.manager.

    Args:
        environment (dict): Dict with application settings.
        scheduler_id (str):
    """
    try:
        extraeye_lib_tracker = environment['extraeye_lib_tracker']

        extraeye_lib_tracker.delete_scheduler(
            environment['user_login'],
            scheduler_id
        )
    except NameError as error:
        console.print_error(error)
        sys.exit(-1)
    except IndexError as error:
        console.print_error(error)
        sys.exit(-1)


@cli.group(help='Команда для работы с группами')
def group():
    """Groups commands group"""
    pass


@group.command(help='Добавить группу к сущности')
@click.option('--name', '-n', callback=entry_tools.validate_group_name_in_add, help='Указать имя группы(обязательная)')
@click.option('--task-id', '-t', help='Указать id задачи, к которой добавляется группа(обязательная, '
                                      'если не указана -r)')
@click.option('--reminder-id', '-r', help='Указать id напоминания, к которому добавляется '
                                          'группа(обязательная, если не указана -t)')
@click.pass_obj
def add(environment, name, task_id, reminder_id):
    """Add group to entity with using add_task_to_entity functions from extraeye_lib.manager.

    Args:
        environment (dict): Dict with application settings.
        name (str):
        task_id (:obj: `str`, optional): Necessarily if don't define reminder_id. Defaults to None.
        reminder_id (:obj: `str`, optional): Necessarily if don't define task_id. Defaults to None.
    """
    try:
        extraeye_lib_tracker = environment['extraeye_lib_tracker']

        entry_tools.validate_entity_id_in_group_operations(group, task_id, reminder_id)
        if task_id:
            extraeye_lib_tracker.add_group_to_task(
                environment['user_login'],
                task_id,
                name
            )
        if reminder_id:
            extraeye_lib_tracker.add_group_to_reminder(
                environment['user_login'],
                reminder_id,
                name
            )
    except IndexError as error:
        console.print_error(error)
        sys.exit(-1)
    except NameError as error:
        console.print_error(error)
        sys.exit(-1)


@group.command(help='Удалить из группы')
@click.option('--name', '-n', callback=entry_tools.validate_group_name_in_delete,
              help='Указать имя группы, из которой удаляется сущность(обязательная)')
@click.option('--task-id', '-t', help='Указать id задачи, которую хотите удалить из группы(обязательная, '
                                      'если не указана -r)')
@click.option('--reminder-id', '-r', help='Указать id напоминания, которое хотите удалить из группы '
                                          'группа(обязательная, если не указана -t)')
@click.pass_obj
def delete(environment, name, task_id, reminder_id):
    """Call delete_group functions from extraeye_lib.manager.

    Args:
        environment (dict): Dict with application settings.
        name (str):
        task_id (str):
        reminder_id (str):
    """
    try:
        extraeye_lib_tracker = environment['extraeye_lib_tracker']

        entry_tools.validate_entity_id_in_group_operations(group, task_id, reminder_id)

        if task_id:
            extraeye_lib_tracker.delete_group_from_task(
                environment['user_login'],
                task_id,
                name,
            )
        if reminder_id:
            extraeye_lib_tracker.delete_group_from_reminder(
                environment['user_login'],
                reminder_id,
                name,
            )

    except ValueError as error:
        console.print_error(error)
        sys.exit(-1)
    except IndexError as error:
        console.print_error(error)
        sys.exit(-1)
    except NameError as error:
        console.print_error(error)
        sys.exit(-1)


@group.command(help='Показать группы')
@click.option('--name', '-n', callback=entry_tools.validate_group_show_parameters, help='Указать имя группы')
@click.option('--all-groups', '-a', is_flag=True, callback=entry_tools.validate_group_show_parameters,
              help='Показать все группы(по умолчанию)')
@click.pass_obj
def show(environment, name, all_groups):
    """Show groups.

    Get groups with help of extraeye_lib.manager functions and display them by console functions.

    Args:
        environment (dict): Dict with application settings.
        name (:obj: `str`, optional): Define group, that will be shown. Defaults to None.
        all_groups (:obj: `bool`, optional): Indicate that all groups will be received. Defaults to True.
    """
    try:
        extraeye_lib_tracker = environment['extraeye_lib_tracker']

        if name:
            group = extraeye_lib_tracker.get_group(
                environment['user_login'],
                name
            )
            console.show_group(group)
        if all_groups or not name:
            all_groups = extraeye_lib_tracker.get_all_groups(
                environment['user_login'],
            )
            console.show_groups(all_groups)
        if not all_groups and not name:
            console.print_error('Вы исключили все условия поиска')
            sys.exit(-1)
    except IndexError as error:
        console.print_error(error)
        sys.exit(-1)
    except NameError as error:
        console.print_error(error)
        sys.exit(-1)


@cli.group(help='Команда для работы с учетными записями')
def user():
    """User commands group"""
    pass


@user.command(help='Добавить нового пользователя')
@click.argument('user_login')
@click.pass_obj
def add(environment, user_login):
    """Call add_user from extraeye_lib.manager, having preliminary checked up the entrance data.

    Args:
        environment (dict): Dict with application settings.
        user_login (str): New user login.
    """
    try:
        extraeye_lib_tracker = environment['extraeye_lib_tracker']

        user_authorization.check_login_unique(user_login, environment['user_register_info_dir'])
        user_authorization.register_user(user_login, environment['user_register_info_dir'])
        extraeye_lib_tracker.create_user_storage(user_login)
    except NameError as error:
        console.print_error(error)
        sys.exit(-1)


@user.command(help='Сменить текущего пользователя')
@click.option('--user-login', '-l', callback=entry_tools.validate_user_login_in_change,
              help='Указать логин нового пользователя(обязательная)')
@click.pass_obj
def change(environment, user_login):
    """Change current user in application.

    Args:
        environment (dict): Dict with application settings.
        environment (dict): Dict with application settings.
        user_login (str):
    """
    try:
        if not user_authorization.is_user_registered(user_login, environment['user_register_info_dir']):
            console.print_error(user_authorization.USER_NOT_REGISTERED_MESSAGE)
            sys.exit(-1)
        user_authorization.change_current_user(user_login, environment['config_path'])
    except NameError as error:
        console.print_error(error)
        sys.exit(-1)
    except FileExistsError as error:
        console.print_error(error)
        sys.exit(-1)


@user.command(help='Показать логин текущего пользователя')
@click.pass_obj
def me(environment):
    """Show current user environment, if current user is registered in application.

    Args:
        environment (dict): Dict with application settings.
    """
    try:
        if not user_authorization.is_current_user_registered(
                   environment['config_path'],
                   environment['user_register_info_dir']
        ):
            console.print_error(user_authorization.USER_NOT_REGISTERED_MESSAGE)
            sys.exit(-1)
        current_user_login = user_authorization.get_current_user_login(environment['config_path'])
        console.show_current_user_login(current_user_login)
    except NameError as error:
        console.print_error(error)
        sys.exit(-1)
    except FileExistsError as error:
        console.print_error(error)
        sys.exit(-1)


def extraeye_entry():
    """Module entry point."""
    entry_tools.modify_usage_error(cli)
    cli()
