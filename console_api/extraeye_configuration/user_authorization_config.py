import os

CURRENT_USER_CONFIG_PATH = os.path.join(os.environ['HOME'], 'extraeye_current_user.ini')

USER_REGISTER_INFORMATION_DIR = os.path.join(os.environ['HOME'], 'Extraeye_Users')
