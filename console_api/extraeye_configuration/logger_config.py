import os
import logging

LOGGER_LEVEL = logging.DEBUG

LOG_FILE_DIR = '/var/tmp' if os.path.exists('/var/tmp') else os.environ['HOME']

LOG_FILE_NAME = 'extraeye_log.log'

LOGGER_FORMATTER = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'

ENABLED = True
