"""Module consists of supporting functions for extraeye_entry.py."""

import os
import sys
import click.exceptions

from collections import namedtuple
from extraeye_lib.storage.path import Path
from click._compat import get_text_stderr
from extraeye_lib import logger as lib_logger
from console_api.extraeye_configuration import user_authorization_config
from console_api.console import (
    print_error,
    show_help_menu,
    show_usage_error
)
from extraeye_lib.entities.task import (
    Priority,
    Status
)
from extraeye_lib.entities.reminder import (
    ReminderOwner,
    EntitiesType
)
from console_api.extraeye_configuration import (
    logger_config,
    storage_config
)
from datetime import (
    datetime,
    date,
    time
)

DEFAULT_TIME = time(15, 30)


def validate_deadline_datetime(ctx, param, value):
    """Validate deadline.

    Convert to datetime received str.

    Args:
       ctx (click.core.Context):
       param (click.core.Option):
       value (str):

    Returns:
        {
            'datetime': If received non empty str,
            'None': Otherwise.
        }

    Raises:
        ValueError: If got str in incorrect format or converted datetime is earlier then now.
    """
    if value:
        if value.count('_') == 0:
            value += '_{0}:{1}'.format(DEFAULT_TIME.hour, DEFAULT_TIME.minute)
        try:
            validating_datetime = datetime.strptime(value, '%d.%m.%Y_%H:%M')
            if validating_datetime < datetime.now():
                show_error_and_exit(ctx.command, 'Нельзя указать крайний срок позже текущего времени')
            return validating_datetime
        except ValueError:
            show_error_and_exit(ctx.command, 'Неправильно введена дата')
    else:
        return None


def validate_group(ctx, param, values):
    """Validate group.

    Get groups set and convert it to list. Exit if group name contain non alphabet symbols.

    Args:
       ctx (click.core.Context):
       param (click.core.Option):
       values (list):

    Returns:
        {
            'list': Groups list,
            'None': Otherwise.
        }
    """
    if values:
        for value in values:
            if not value.isalpha():
                show_error_and_exit(ctx.command, 'Недопустимое название группы')
        return list(values)
    return None


def validate_priority(ctx, param, value):
    """Create Priority obj from str instance.

    Returns:
        'Priority':

    Args:
       ctx (click.core.Context):
       param (click.core.Option):
       value (str):

    Raises:
        ValuerError: If value is in incorrect for priority form.
    """
    if value:
        try:
            return Priority(int(value))
        except ValueError:
            show_error_and_exit(ctx.command, 'Неправильно введен приоритет')
    else:
        return None


def validate_reminder_owner(ctx, param, owner):
    """Create ReminderOwner obj from str instances list.

    Returns:
        'ReminderOwner':

    Args:
       ctx (click.core.Context):
       param (click.core.Option):
       owner (list):

    Raises:
        ValueError: If owner can't be transformed to ReminderOwner instance.
    """
    if owner:
        try:
            owner = ReminderOwner(EntitiesType(owner[0]), owner[1], False)
        except ValueError:
            show_error_and_exit(ctx.command, 'Нет такого типа сущностей: {}'.format(owner[0]))
    return owner


def validate_group_entities(ctx, param, owner):
    """Transform owner to Type instance. Exit if owner.name is not equal any extraeye entities names

    Args:
       ctx (click.core.Context):
       param (click.core.Option):
       owner (list):

    Returns:
        {
            'Type': If owner can be transformed to Type,
            'None': Otherwise.
        }
    """
    if owner:
        Type = namedtuple('Type', 'name, id')
        owner = Type(owner[0], owner[1])
        if owner.name != EntitiesType.TASK.value and owner.name != EntitiesType.REMINDER.value:
            print_error('Нет такого типа сущностей: {}'.format(owner.name))
            sys.exit(-1)
    return owner


def priority_to_str(priority_obj):
    """Convert priority to str.

    Args:
       priority_obj (Priority):

    Returns:
        'str': str format of priority.
    """
    return Priority.to_str(priority_obj)


def check_parameters_for_reminder(reminder, description, owner):
    """Check create reminder parameters.

    Checking parameters for reminder creating. Exit if received parameters
    is not enough to create reminder.

    Args:
        reminder (Click.core.group): Misused command group.
        description (str):
        owner (ReminderOwner):
    """
    if not owner and not description:
        show_error_and_exit(reminder, 'Для самостоятельного напоминания описание обязательно')


def validate_notify_date_in_add_reminders(ctx, param, notify_date_str):
    """Validate notify_date in reminder.

    Convert received str to date instance. Exit if str is empty.

    Args:
       ctx (click.core.Context):
       param (click.core.Option):
       notify_date_str (str):

    Returns:
        'date':

    Raises:
        ValueError: If str is in incorrect format to transform to date.
    """
    try:
        if not notify_date_str:
            show_error_and_exit(ctx.command, 'Укажите дату напоминания')
        validating_datetime = datetime.strptime(notify_date_str, '%d.%m.%Y').date()
        if validating_datetime < date.today():
            show_error_and_exit(ctx.command, 'Нельзя указать дату напоминания позже текущего времени')
        return validating_datetime
    except ValueError:
        show_error_and_exit(ctx.command, 'Неправильно введена дата напоминания')


def validate_status(ctx, param, status):
    """Convert str instance to Status object, exit if str instance is int incorrect form.

    Args:
       ctx (click.core.Context):
       param (click.core.Option):
       status (str):

    Returns:
        {
            'Status': if Str is no empty,
            'None'L Otherwise.
        }
    """
    if status:
        try:
            status = Status(status)
            return status
        except ValueError:
            show_error_and_exit(ctx.command, 'Некорректное значение статуса')
    else:
        return None


def validate_share_obj_type(ctx, param, obj_type):
    """Validate sharing entity type. If it isn't task - exit.

    Args:
       ctx (click.core.Context):
       param (click.core.Option):
       obj_type (str):

    Returns:
        'str': obj type.
    """
    if obj_type != EntitiesType.TASK.value:
        show_error_and_exit(ctx.command, 'Неправильно указан тип сущности, которой хотите поделиться')
    return obj_type


def validate_entity_id(ctx, param, e_id):
    """Checks whether the id was passed. Exit if not.

    Args:
       ctx (click.core.Context):
       param (click.core.Option):
       e_id (str):

    Returns:
        'str': entity id
    """
    if not e_id:
        show_error_and_exit(ctx.command, 'Укажите id')
    return e_id


def validate_task_description(ctx, param, description):
    """Check where task description was passed. Exit if not.

    Args:
       ctx (click.core.Context):
       param (click.core.Option):
       description (str):

    Returns:
        'sts': task description.
    """
    if not description:
        show_error_and_exit(ctx.command, 'Укажите описание для задачи')
    return description


def validate_date(ctx, param, date_str):
    """Validate date.

    Convert received str to date instance.

    Args:
       ctx (click.core.Context):
       param (click.core.Option):
       date_str (str):

    Returns:
        {
            'date': If date_str is not empty,
            'None': Otherwise.
        }

    Raises:
        ValueError: If date_str is in incorrect format.
    """
    if date_str:
        try:
            date = datetime.strptime(date_str, '%d.%m.%Y').date()
            return date
        except ValueError:
            show_error_and_exit(ctx.command, 'Неправильно введена дата')
    return date_str


def validate_start_date_in_show(ctx, param, start_date_str):
    """Validate start date in show functions.

    Transform str to date instance. Exit if start date is later then end date.

    Args:
       ctx (click.core.Context):
       param (click.core.Option):
       start_date_str (str):

    Returns:
        {
            'date': If str is not empty,
            'None': Otherwise.
        }
    """
    start_date = validate_date(ctx, param, start_date_str)
    if start_date and 'end_date' in ctx.params:
        if start_date > ctx.params['end_date']:
            show_error_and_exit(ctx.command, 'Дата конца периода не может быть раньше даты начала')
    return start_date


def validate_end_date_in_show(ctx, param, end_date_str):
    """Validate end date in show functions.

    Transform str to date instance. Exit if end date is earlier then start date.

    Args:
       ctx (click.core.Context):
       param (click.core.Option):
       end_date_str (str):

    Returns:
        {
            'date': If str is not empty,
            'None': Otherwise.
        }
    """
    end_date = validate_date(ctx, param, end_date_str)

    if end_date and 'start_date' in ctx.params:
        if end_date < ctx.params['start_date']:
            show_error_and_exit(ctx.command, 'Дата конца периода не может быть раньше даты начала')

    return end_date


def validate_user_login_in_change(ctx, param, user_login):
    """Validate user login in change user login function. Exit if login is empty str.

    Args:
       ctx (click.core.Context):
       param (click.core.Option):
       user_login (str):

    Returns:
        'str': new current user login
    """
    if not user_login:
        show_error_and_exit(ctx.command, 'Укажите логин')
    return user_login


def setup_extraeye_logger():
    """Setup extraeye logger with using logger setup function."""
    lib_logger.setup_extraeye_logger(
        logger_level=logger_config.LOGGER_LEVEL,
        logger_formatter=logger_config.LOGGER_FORMATTER,
        logger_path=os.path.join(logger_config.LOG_FILE_DIR, logger_config.LOG_FILE_NAME),
        enabled=logger_config.ENABLED
    )


def setup_authorization_paths():
    """Setup path to dirs that performs register information of each user.

    Returns:
        'tuple': Path to config with current user information and path to register information of each user.
    """
    return user_authorization_config.CURRENT_USER_CONFIG_PATH, user_authorization_config.USER_REGISTER_INFORMATION_DIR


def validate_receiver_login(ctx, param, login):
    """Checks whether receiver login was passed. Exit if not.

    Args:
       ctx (click.core.Context):
       param (click.core.Option):
       login (list): receiver login

    Returns:
        'str': receiver login.
    """
    if not login:
        show_error_and_exit(ctx.command, 'Укажите логин получателя')
    return login


def setup_storage_paths():
    """Setup base application storage path from storage config.

    Returns:
        'str':
    """
    return Path(base_dir=storage_config.BASE_DIR)


def validate_group_name_in_add(ctx, param, name):
    """Checks whether group name was passed. Exit if not.

    Args:
       ctx (click.core.Context):
       param (click.core.Option):
       name (str):

    Returns:
        'str': Group name.
    """
    if not name:
        show_error_and_exit(ctx.command, 'Укажите имя группы')
    return name


def validate_entity_id_in_group_operations(group, task_id, reminder_id):
    """Checks whether task id or reminder id was passed. Exit if both don't.

    Args:
        group (click.core.Group): Misused command group.
        task_id (str):
        reminder_id (str):
    """
    if not task_id and not reminder_id:
        show_error_and_exit(group, 'Укажите id сущности')


def validate_group_show_parameters(ctx, param, value):
    """Checks group show parameters. Exit if both name and all_groups where identified.

    Returns:
        'str, bool': Group name or truth of is_all parameter.
    """
    if ('name' in ctx.params or 'all_groups' in ctx.params) and value:
        show_error_and_exit(ctx.command, 'Взаимоисключающие опции')
    return value


def validate_scheduler_start_date(ctx, param, start_date_str):
    """Validate scheduler start date.

    Convert received str to date instance with using function validate_date. Exit if
    start date is later then end date or start date is earlier then today date.

    Args:
       ctx (click.core.Context):
       param (click.core.Option):
       start_date_str (str):

    Returns:
        'date': start date.
    """
    start_date = validate_date(ctx, param, start_date_str)

    if start_date and 'end_date' in ctx.params:
        if start_date > ctx.params['end_date']:
            show_error_and_exit(ctx.command, 'Дата конца работы планировщика не может быть раньше даты начала')

    if start_date and start_date < date.today():
        show_error_and_exit(ctx.command, 'Дата начала планировщика не может быть раньше текущей даты')

    return start_date


def validate_group_name_in_delete(ctx, param, name):
    """Checks whether group name was passed. Exit if not.

    Args:
       ctx (click.core.Context):
       param (click.core.Option):
       name (str):

    Returns:
        'str': Group name.
    """
    if not name:
        show_error_and_exit(ctx.command, 'Укажите имя группы')
    return name


def validate_scheduler_end_date(ctx, param, end_date_str):
    """Validate scheduler end date.

    Args:
       ctx (click.core.Context):
       param (click.core.Option):
       end_date_str (str):

    Convert received str to date instance with using function validate_date. Exit if
    end date is earlier then start date or end date is earlier then today date.

    Returns:
        'date': end date.
    """
    end_date = validate_date(ctx, param, end_date_str)

    if end_date and 'start_date' in ctx.params:
        if end_date < ctx.params['start_date']:
            show_error_and_exit(ctx.command, 'Дата конца работы планировщика не может быть раньше даты начала')

    if end_date and end_date < date.today():
        show_error_and_exit(ctx.command, 'Дата конца планировщика не может быть раньше текущей даты')

    return end_date


def validate_date_difference(ctx, param, value):
    """Validate date difference.

    Validate date difference for scheduler. Exit if more then one date difference field is True.

    Args:
       ctx (click.core.Context):
       param (click.core.Option):
       value (bool):

    Returns:
     'bool': value of checking field.
    """
    true_date_difference_arguments_amount = 0
    if 'year_difference' in ctx.params and ctx.params['year_difference']:
        true_date_difference_arguments_amount += 1
    if 'month_difference' in ctx.params and ctx.params['month_difference']:
        true_date_difference_arguments_amount += 1
    if 'day_difference' in ctx.params and ctx.params['day_difference']:
        true_date_difference_arguments_amount += 1

    if true_date_difference_arguments_amount > 1:
        show_error_and_exit(ctx.command, 'Укажите только одну единицу даты, в которой будут измеряться промежутки')

    return value


def init_task_info(task_description):
    """Init task info dict.

    Args:
        task_description (str):

    Returns:
        'dict': Task info dict.
    """
    return {'description': task_description}


def modify_usage_error(commands_group):
    """A method to append the help menu when commands groups are used incorrectly.

    Args:
        commands_group: Misused commands group.
    """
    def show(self, file=None):
        if file is None:
            file = get_text_stderr()
        color = None
        show_usage_error(self.format_message(), file, color)
        sys.argv = [sys.argv[0]]
        commands_group()

    click.exceptions.UsageError.show = show


def show_error_and_exit(command, message):
    """Print error message and exit.

    Args:
        command (click.Core.Command): Misused command.
        message (str):
    """
    print_error(message)
    show_help_menu(command)
    sys.exit(-1)
