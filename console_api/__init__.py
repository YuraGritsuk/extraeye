"""This package implements specialized functions for interaction with lib by console.

Includes output information and parsing user commands. For execution user commands are
calling functions from extraeye lib.

* console.py              module for output information, errors.
* extraeye_entry.py       application entrance, consist of functions that parse user commands and calls
                          needed lib functions.
* extraeye_entry_tools.py supporting functions.
* user_authorization.py   functions for work with user registration and login to the system.
"""
