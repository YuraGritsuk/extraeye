"""Functions for work with users authorization.

They allow:

* register new user
* check user registration
* check login unique of new user
* check is someone registered in the system
* get current user login
* switch current user
"""

import os
import json
import configparser


USER_NOT_REGISTERED_MESSAGE = 'Пользователь не зарегистрирован'
DEFAULT_REGISTER_INFO_DIR = os.path.join(os.environ['HOME'], 'Extraeye_Users')
DEFAULT_CONFIG_PATH = os.path.join(os.environ['HOME'], 'extraeye_current_user.ini')


def register_user(user_login, user_register_information_dir=DEFAULT_REGISTER_INFO_DIR):
    """Registers the user in the application.
    
    user_login (str):
    user_register_information_dir (:obj: `str`, optional): Directory to users register files. Defaults to
        DEFAULT_REGISTER_INFO_DIR.
    """
    if not os.path.exists(user_register_information_dir):
        os.makedirs(user_register_information_dir)

    with open(os.path.join(user_register_information_dir, user_login), 'w',
              encoding='utf-8') as user_register_information_file:
        json.dump({'user_login': user_login}, user_register_information_file)


def change_current_user(user_login, config_path=DEFAULT_CONFIG_PATH):
    """Change current user login.

    Write in section 'Current_User', option 'login' of extraeye_current_user_config.ini user_login.

    Args:
        user_login (str):
        config_path (:obj: `str`, optional): Path to extraeye_current_user_config. Defaults to None.

    Raises:
        configparser.NoOptionError: If config doesn't have option 'login' in section 'Current_User'.
    """
    if not os.path.exists(config_path):
        _create_current_user_config(config_path)

    config = configparser.ConfigParser()
    config.read(config_path)
    config.set('Current_User', 'login', user_login)

    with open(config_path, 'w') as config_file:
        config.write(config_file)


def get_current_user_login(config_path=DEFAULT_CONFIG_PATH):
    """Get current user login.

    Read current user login from section 'Current_User', option 'login' in
    extraeye_current_user_config.ini. If config doesn't exist, create him.

    Args:
        config_path (:obj: `str`, optional): Path to extraeye_current_user_config. Defaults to None.

    Returns:
         current user login (str):
    """
    if not os.path.exists(config_path):
        _create_current_user_config(config_path)

    config = configparser.ConfigParser()
    config.read(config_path)

    return config.get('Current_User', 'login')


def check_login_unique(new_user_login, user_register_information_dir=DEFAULT_REGISTER_INFO_DIR):
    """Check new user login uniqueness.

    Iterates in registered users and compare them logins with new_user_login.

    Args:
        new_user_login (str):
        user_register_information_dir (:obj: `str`, optional): Directory to users register files. Defaults to
            DEFAULT_REGISTER_INFO_DIR.

    Raises:
        NameError: If someone already check login equal to new_user_login.
    """
    if not os.path.exists(user_register_information_dir):
        return

    for register_info_file in os.listdir(user_register_information_dir):
        with open(os.path.join(user_register_information_dir, register_info_file), 'r',
                  encoding='utf-8') as json_user_register_info:
            registered_user_login = json.load(json_user_register_info)['user_login']
            if registered_user_login == new_user_login:
                raise NameError('Логин уже занят')


def is_current_user_registered(config_path=DEFAULT_CONFIG_PATH,
                               user_register_information_dir=DEFAULT_REGISTER_INFO_DIR):
    """Check whether the application knows which user to work with.

    Check whether the application knows which user to work with. If application know, checking current
    user authorization. In case current user have been written in config file, but doesn't have account,
    recording is erasing.

    Args:
        config_path (:obj: `str`, optional): Path to extraeye_current_user_config. Defaults to None.
        user_register_information_dir (:obj: `str`, optional): Directory to users register files. Defaults to
            DEFAULT_REGISTER_INFO_DIR.

    Returns:
        {
            'True': If option login in extraeye current user config is not empty and user with this login is registered.
            'False': Otherwise.
        }
    """
    current_user_login = get_current_user_login(config_path)
    if not is_user_registered(current_user_login, user_register_information_dir):
        _set_default_current_user(config_path)
        return False
    return True


def is_user_registered(user_login, user_register_information_dir=DEFAULT_REGISTER_INFO_DIR):
    """Checking user authorization.

    Args:
        user_login (str):
        user_register_information_dir (:obj: `str`, optional): Directory to users register files. Defaults to
            DEFAULT_REGISTER_INFO_DIR.

    Returns:
        {
            'True': If user with this login is registered.
            'False': Otherwise.
        }
    """
    if user_login and _has_user_account(user_login, user_register_information_dir):
        return True
    return False


def _create_current_user_config(config_path=DEFAULT_CONFIG_PATH):
    if os.path.exists(config_path):
        raise FileExistsError('Конфигурационный файл уже создан')

    if not os.path.exists(os.path.dirname(config_path)):
        os.makedirs(os.path.dirname(config_path))
    _set_default_current_user(config_path)


def _has_user_account(user_login, user_register_information_dir=DEFAULT_REGISTER_INFO_DIR):
    if not os.path.exists(os.path.join(user_register_information_dir, user_login)):
        return False
    return True


def _set_default_current_user(config_path=DEFAULT_CONFIG_PATH):
    config = configparser.ConfigParser()
    config.add_section('Current_User')
    config.set('Current_User', 'login', '')

    with open(config_path, 'w') as config_file:
        config.write(config_file)
