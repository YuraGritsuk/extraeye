"""Module consists of functions that output extraeye application info to the console."""

import sys
import click

from click.utils import echo
from extraeye_lib.localization import rus_localization
from extraeye_lib.exceptions.id_exceptions import (
    TaskIdError,
    ReminderIdError,
    SchedulerIdError
)
from extraeye_lib.exceptions.name_exceptions import (
    GroupNameError,
    LoginNameError,
)
from extraeye_lib.exceptions.other_exceptions import SchedulerAlreadyCreatedAllTasksError


def show_tasks(user_tasks, full=False):
    """Display tasks.

    Displaying tasks in full or short format in depending on the value of full.

    Args:
        user_tasks (list):
        full (:obj: `bool`, optional): Default to False
    """
    if user_tasks:
        click.secho('==============================================', bold=True)
    for task in user_tasks:
        if full:
            _show_task_full(task)
        else:
            _show_task_short(task)
        click.secho('===============================================', bold=True)


def _show_task_full(task):
    """Display task in detail.

    Args:
        task (Task):
    """
    click.secho('id: {}'.format(task.t_id))
    click.secho('Описание: {}'.format(task.description))
    click.secho('Статус: {}'.format(rus_localization.convert_status_to_str(task.status)))
    click.secho('Приоритет: {}'.format(rus_localization.convert_priority_to_str(task.priority)))
    click.secho('Время создания: {}'.format(str(task.time.creation)))
    click.secho('Время изменения: {}'.format(str(task.time.edition)))
    click.secho('Владелец: {}'.format(task.user_login))

    if task.deadline:
        click.secho('Краний срок: {}'.format(str(task.deadline)))
    if task.parent:
        click.secho('Родительская задача: {}'.format(str(task.parent)))
    if task.subtasks:
        click.secho('Подазадачи:')
        for subtask_t_id in task.subtasks:
            click.secho('\t{}'.format(subtask_t_id))
    if task.other:
        click.secho('Есть у пользователей')
        for user_login in task.other:
            click.secho('\t{}'.format(user_login))
    if task.reminders:
        click.secho('Напоминания:')
        for reminder in task.reminders:
            click.secho('\t{}'.format(reminder))
    if task.groups:
        click.secho('Группы:')
        for group_name in task.groups:
            click.secho('\t{}'.format(group_name))


def _show_task_short(task):
    """Display task superficially.

    Args:
        task (Task):
    """
    click.secho('id: {}'.format(task.t_id))
    click.secho('Описание: {}'.format(task.description))
    click.secho('Статус: {}'.format(rus_localization.convert_status_to_str(task.status)))
    click.secho('Владелец: {}'.format(task.user_login))

    if task.deadline:
        click.secho('Краний срок: {}'.format(str(task.deadline)))


def show_reminders(user_reminders, full=False):
    """Display reminders.

    Displaying reminders in full or short format in depending on the value of full.

    Args:
        user_reminders (list):
        full (:obj: `bool`, optional): Default to False
    """
    if user_reminders:
        click.secho('==============================================', bold=True)
    for reminder in user_reminders:
        if full:
            _show_reminder_full(reminder)
        else:
            _show_reminder_short(reminder)
        click.secho('===============================================', bold=True)


def _show_reminder_full(reminder):
    """Display reminder in detail.

    Args:
        reminder (Reminder):
    """
    click.secho('id: {}'.format(reminder.r_id))
    click.secho('Описание: {}'.format(reminder.description))
    click.secho('Время напоминания: {}'.format(str(reminder.notify_date)))
    click.secho('Время создания: {}'.format(str(reminder.time.creation)))
    click.secho('Время изменения: {}'.format(str(reminder.time.edition)))
    click.secho('Владелец: {}'.format(reminder.user_login))

    if reminder.owner:
        click.secho('Принадлежит {0}: {1}'.format(reminder.owner.type.value, reminder.owner.r_id))
    if reminder.other:
        click.secho('Есть у пользователей')
        for user in reminder.other.values():
            click.secho('\t{}'.format(str(user)))
    if reminder.groups:
        click.secho('Группы:')
        for group_name in reminder.groups:
            click.secho('\t{}'.format(group_name))


def _show_reminder_short(reminder):
    """Display reminder superficially.

    Args:
        reminder (Reminder):
    """
    click.secho('id: {}'.format(reminder.r_id))
    click.secho('Описание: {}'.format(reminder.description))
    click.secho('Время напоминания: {}'.format(str(reminder.notify_date)))


def show_notify_reminders(reminders):
    """Display user missed reminders.

    Args:
        reminders (list):
    """
    click.secho('Пропущенные напоминания:')
    for reminder in reminders:
        click.secho('\t{0}: {1}'.format(str(reminder.notify_date), reminder.description))
    click.prompt('\nВведите что-либо чтобы продолжить')


def show_groups(groups):
    """Display groups.

    Args:
        groups (list):
    """
    if not groups:
        print_error('Нет групп, возможно потому что групповые задачи/напоминания перемещены в архив или удалены')
        sys.exit(-1)
    for group in groups:
        click.secho(group.name)


def show_group(group):
    """Display group.

    Args:
        group (Group):
    """
    if not group.tasks and not group.reminders:
        print_error('Группа не найдена, возможно потому что ее задачи/напоминания перемещены в архив или удалены')
        sys.exit(-1)

    click.secho('{}:'.format(group.name))

    if group.tasks:
        click.secho('  Задачи:')
        for task in group.tasks:
            click.secho('    {}'.format(task.description))
    if group.reminders:
        click.secho('  Напоминания:')
        for reminder in group.reminders:
            click.secho('    {}'.format(reminder.description))


def show_schedulers(user_schedulers, full=False):
    """Display schedulers.

    Function get user schedulers and depending on full value show them in full or show format.

    Args:
        user_schedulers (list):
        full (:obj: `bool`, optional):
    """
    if user_schedulers:
        click.secho('==============================================', bold=True)
    for scheduler in user_schedulers:
        if full:
            _show_scheduler_full(scheduler)
        else:
            _show_scheduler_short(scheduler)
        click.secho('==============================================', bold=True)


def _show_scheduler_full(scheduler):
    """Show scheduler in detail.

    Args:
        scheduler (Scheduler):
    """
    click.secho('id: {}'.format(scheduler.s_id))
    click.secho('Владелец: {}'.format(scheduler.user_login))
    click.secho('Время создания: {}'.format(str(scheduler.time.creation)))
    click.secho('Время изменения: {}'.format(str(scheduler.time.edition)))
    click.secho('Дата начала работы планировщика: {}'.format(str(scheduler.start_date)))
    click.secho('Дата конца работы планировщика: {}'.format(str(scheduler.end_date)))
    click.secho('Разница по времени между задачами: {}'.format(
        rus_localization.convert_date_delta_to_rus_str(scheduler.date_delta)))
    click.secho('Задач создано: {}'.format(str(scheduler.tasks_upload)))
    click.secho('Описание создаваемой задачи: {}'.format(scheduler.task_info['description']))


def _show_scheduler_short(scheduler):
    """Show scheduler superficially.

    Args:
        scheduler (Scheduler):
    """
    click.secho('id: {}'.format(scheduler.s_id))
    click.secho('Дата начала работы планировщика: {}'.format(str(scheduler.start_date)))
    click.secho('Дата конца работы планировщика: {}'.format(str(scheduler.end_date)))
    click.secho('Разница по времени между задачами: {}'.format(
        rus_localization.convert_date_delta_to_rus_str(scheduler.date_delta)))
    click.secho('Задач создано: {}'.format(str(scheduler.tasks_upload)))
    click.secho('Описание создаваемой задачи: {}'.format(scheduler.task_info['description']))


def show_planned_task(schedulers):
    """Show schedulers that will create during the period for which tasks are viewed.

    Args:
        schedulers (list):
    """
    if schedulers:
        click.secho('Планировщики, которые создадут в этот период задачу(и):\n')
    for scheduler in schedulers:
        _show_scheduler_short(scheduler)
        click.secho('======================================', bold=True)


def show_current_user_login(user_login):
    """Display user login.

    Args:
        user_login (str):
    """
    click.secho('Логин: {}'.format(user_login))


def suggest_show_reminders():
    """Suggest user show missed reminders.

    Suggest user show missed reminders, if user input incorrect symbols function calls again.

    Returns:
        {
            'True': If user tap 'y',
            'False': If user tap 'n',
            'None': If user tap 'l'

        }
    """
    choice = click.prompt('У вас есть напоминания, показать?(y-да, n-нет, l-позже)')

    if choice != 'y' and choice != 'n' and choice != 'l':
        print_error('Введите один из вышепредложенных вариантов')
        suggest_show_reminders()
    if choice == 'y':
        return True
    if choice == 'n':
        return False


def show_usage_error(message, file, color):
    echo('Error: %s\n' % message, file=file, color=color)


def show_help_menu(group):
    """Show group help menu.

    Args:
        group (click.core.Group): Misused commands group
    """
    with click.Context(group) as ctx:
        click.echo(group.get_help(ctx))


def print_error(error):
    """Print error message on stderr.

    Convert received error into str with localization functions help and
    print error str on stderr.

    Args:
        error (Exception or str):
    """
    if isinstance(error, TaskIdError):
        str_error = rus_localization.get_task_id_error_rus_str(error)
    elif isinstance(error, ReminderIdError):
        str_error = rus_localization.get_reminder_id_error_rus_str()
    elif isinstance(error, SchedulerIdError):
        str_error = rus_localization.get_scheduler_id_error_rus_str()
    elif isinstance(error, GroupNameError):
        str_error = rus_localization.get_group_name_error_rus_str(error)
    elif isinstance(error, LoginNameError):
        str_error = rus_localization.get_login_name_error_rus_str(error)
    elif isinstance(error, SchedulerAlreadyCreatedAllTasksError):
        str_error = rus_localization.get_scheduler_already_created_all_tasks_error_rus_str()
    else:
        str_error = str(error)

    click.secho(str_error, err=True, bg='red')
