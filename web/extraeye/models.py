from django.db import models
from django.utils import timezone

# Create your models here.


class Task(models.Model):
    user_login = models.CharField(max_length=30)
    description = models.CharField(max_length=200)
    creation_datetime = models.DateTimeField(default=timezone.now)


class User(models.Model):
    login = models.CharField(max_length=30)
    password = models.CharField(max_length=30)
