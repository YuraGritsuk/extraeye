from .forms import TaskForm
from .models import Task
from django.template.context_processors import csrf
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import (
    render,
    redirect,
    render_to_response,
    get_object_or_404
)
from django.contrib.auth import (
    login as auth_login,
    authenticate,
)


def home(request):
    # tasks = Task.objects.filter(creation_datetime__lte=timezone.now())
    return render(request, 'extraeye/home.html', {})


def task_info(request, pk):
    task = get_object_or_404(Task, pk=pk)
    return render(request, 'extraeye/task_info.html', {'task': task})


def create_task(request):
    if request.method == 'POST':
        task_form = TaskForm(request.POST)
        if task_form.is_valid():
            task = task_form.save(commit=False)
            task.save()
            return redirect('task_info', pk=task.pk)
    else:
        task_form = TaskForm()
    return render(request, 'extraeye/create_task.html', {'form': task_form})


def edit_task(request, pk):
    task = get_object_or_404(Task, pk=pk)
    if request.method == "POST":
        task_form = TaskForm(request.POST, instance=task)
        if task_form.is_valid():
            task = task_form.save()
            task.save()
            return redirect('task_info', pk=task.pk)
    else:
        task_form = TaskForm(instance=task)
    return render(request, 'extraeye/edit_task.html', {'form': task_form})


def test(request):
    return render(request, 'extraeye/test.html', {})


def login(request):
    args = {}
    args.update(csrf(request))
    if request.POST:
        login = request.POST.get('login', '')
        password = request.POST.get('password', '')

        user = authenticate(username=login, password=password)

        if user:
            auth_login(request, user)
            return redirect('../home/')
        else:
            args['login_error'] = 'Incorrect username or password'
            return render_to_response('authorization/login.html', args)

    return render_to_response('authorization/login.html', args)


def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            auth_login(request, user)
            return redirect('../home/')
    else:
        form = UserCreationForm()
    return render(request, 'authorization/signup.html', {'form': form})


def start(_):
    return redirect('login/')


def tasks(request):
    manager
    return render(request, 'extraeye/tasks.html', {})



