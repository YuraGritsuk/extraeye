from django import forms

from .models import (
    Task,
    User
)


class TaskForm(forms.ModelForm):

    class Meta:
        model = Task
        fields = ('description', )


class UserLoginForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ('login', 'password')
