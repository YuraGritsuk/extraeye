from django.conf.urls import url
from django.contrib.auth import views as system_views
from . import views

urlpatterns = [

    url(r'^signup/$', views.signup, name='signup'),
    url(r'^$', views.start, name='start'),
    url(r'^login/$', views.login, name='login'),
    url(r'^task/(?P<pk>\d+)/$', views.task_info, name='task_info'),
    url(r'^task/create/$', views.create_task, name='create_task'),
    url(r'^task/(?P<pk>\d+)/edit/$', views.edit_task, name='edit_task'),
    url(r'^home/$', views.home, name='home'),
    url(r'^test/$', views.test, name='test'),
    url(r'^tasks/$', views.tasks, name='tasks'),
]